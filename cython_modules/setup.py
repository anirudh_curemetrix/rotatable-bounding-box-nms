from distutils.core import setup
from Cython.Build import cythonize

from distutils.extension import Extension
from Cython.Distutils import build_ext


#ext_options = {"compiler_directives": {"profile": True}, "annotate": True}
ext_options = {}

#setup(name="cython_utils",
     # ext_modules=cythonize("cython_utils.pyx"),**ext_options)

#setup(name="cython_new_utils",
      #ext_modules=cythonize("cython_new_utils.pyx"),**ext_options)


# ext_modules=[
#     Extension("nms_rotated",
#               ["nms_rotated.pyx"],
#               libraries=["m"],
#               extra_compile_args = ["-O3", "-ffast-math", "-march=native", "-fopenmp" ],
#               extra_link_args=['-fopenmp']
#               ) 
# ]

#setup(name="nms_rotated",
      #ext_modules=cythonize("nms_rotated.pyx"),**ext_options)

setup(name="nms_rotated5",
      ext_modules=cythonize("nms_rotated5.pyx"),**ext_options)
# setup( 
#   name = "nms_rotated",
#   cmdclass = {"build_ext": build_ext},
#   ext_modules = ext_modules
# )
