import time 
import tensorflow as tf 
import numpy as np 

proposal_count = 1000

def nms(normalized_boxes, scores):
    boxes = normalized_boxes[:,0:4]
    avg = 0
    for _i in range(10000):
        start_time = time.time()
        indices = tf.image.non_max_suppression(boxes, scores, proposal_count, 0.7, name="rpn_non_max_suppression")
        end_time = time.time() - start_time
        avg = avg + end_time
    avg = avg/10000
    print("NMS time :{}".format(avg))
    
    proposals = tf.gather(normalized_boxes, indices)
       # Pad if needed
    #padding = proposal_count - tf.shape(input=proposals)[0]
    #proposals = tf.concat([proposals, tf.zeros([padding, 5],tf.float32)], 0)
    return proposals


if __name__ == '__main__':
    boxes = np.random.rand(2000,4)
#     boxes = np.array([[50, 50, 100, 100],
#                       [60, 60, 100, 100],
#                       [50, 50, 100, 100],
#                       [200, 200, 100, 100]])
    scores = np.random.rand(2000)
    print(boxes)
#     scores = np.array([0.99, 0.88, 0.66, 0.77])
    nms(boxes, scores)
    

    
    
    