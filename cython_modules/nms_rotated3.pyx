#cython: profile=True

cimport cython
import numpy as np
cimport numpy as np

DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

#from math import pi, cos, sin
from libc.math cimport sin, cos

cdef DTYPE_t Vector_Add_simple( DTYPE_t x1, DTYPE_t x2):
    return x1 + x2

cdef Vec_Line(DTYPE_t x1,DTYPE_t y1, DTYPE_t x2,DTYPE_t y2):
    cdef DTYPE_t a
    cdef DTYPE_t b 
    cdef DTYPE_t c
    a = y2 - y1
    b = x1 - x2
    c = x2*y1 - y2*x1
    return a,b,c

cdef DTYPE_t Line_point(DTYPE_t a,DTYPE_t b,DTYPE_t c, DTYPE_t x1, DTYPE_t y1):
    return a*x1 + b*y1 + c

cdef DTYPE_t Line_intersectionx(DTYPE_t a1,DTYPE_t b1, DTYPE_t c1,DTYPE_t a2, DTYPE_t b2,DTYPE_t c2):
    cdef DTYPE_t w
    cdef DTYPE_t x
    w = a1*b2 - b1*a2
    x = (b1*c2 - c1*b2)/w
    return x

cdef DTYPE_t Line_intersectiony(DTYPE_t a1,DTYPE_t b1, DTYPE_t c1,DTYPE_t a2, DTYPE_t b2,DTYPE_t c2):
    cdef DTYPE_t w
    cdef DTYPE_t y
    w = a1*b2 - b1*a2
    y = (c1*a2 - a1*c2)/w
    return y


cdef rectangle_vertices(cx, cy, w, h, angle):
    cdef DTYPE_t area = w*h
    #PI / 180. == 0.01745329251
    cdef DTYPE_t angle_rad = angle*0.01745329251
    cdef DTYPE_t dx = w/2
    cdef DTYPE_t dy = h/2
    cdef DTYPE_t dxcos = dx*cos(angle_rad)
    cdef DTYPE_t dxsin = dx*sin(angle_rad)
    cdef DTYPE_t dycos = dy*cos(angle_rad)
    cdef DTYPE_t dysin = dy*sin(angle_rad)
    
    cdef DTYPE_t x1,y1,x2,y2,x3,y3,x4,y4
    
    x1 = Vector_Add_simple(cx, -dxcos - -dysin)
    y1 = Vector_Add_simple(cy, -dxsin + -dycos)
    x2 = Vector_Add_simple(cx, dxcos - -dysin)
    y2 = Vector_Add_simple(cy, dxsin + -dycos)
    
    x3 = Vector_Add_simple(cx, dxcos -  dysin )
    y3 = Vector_Add_simple(cy, dxsin +  dycos)
    x4 = Vector_Add_simple(cx, -dxcos -  dysin )
    y4 = Vector_Add_simple(cy, -dxsin +  dycos)

    return [(x1,y1), (x2,y2), (x3,y3), (x4,y4)], area


cdef int neighbor(np.ndarray[DTYPE_t, ndim=1] r1, np.ndarray[DTYPE_t, ndim=1] r2):
    
    cdef DTYPE_t L1 = max(r1[2],r1[3])
    cdef DTYPE_t L2 = max(r2[2],r2[3])
    cdef DTYPE_t y11,x11,y12,x12
    cdef DTYPE_t y21,x21,y22,x22
    cdef int neighbor
    y11 = r1[0] - 0.5*L1
    x11 = r1[1] - 0.5*L1  
    y12 = r1[0] + 0.5*L1
    x12 = r1[1] + 0.5*L1
    
    y21 = r2[0] - 0.5*L2
    x21 = r2[1] - 0.5*L2  
    y22 = r2[0] + 0.5*L2
    x22 = r2[1] + 0.5*L2
    
    iw = min(y12, y22) - max(y11, y21)
    ih = min(x12, x22) - max(x11, x21) 

    if (iw>0 and ih>0):
        neighbor = 1
    else:
        neighbor = 0

    return neighbor

cdef DTYPE_t intersection_area(np.ndarray[DTYPE_t, ndim=1] r1, np.ndarray[DTYPE_t, ndim=1] r2):
    
    cdef DTYPE_t area1,area2,s_value,t_value,iou
    cdef DTYPE_t intersectionArea
    cdef tuple p,q,s,t,intersection_point
    cdef DTYPE_t a,b,c, a2, b2, c2
    cdef list rect1,rect2,intersection
    cdef list new_intersection,line_values
    
    if not neighbor(r1,r2):
        return 0
    
    rect1, area1 = rectangle_vertices(r1[0], r1[1], r1[2], r1[3], r1[4])
    rect2, area2 = rectangle_vertices(r2[0], r2[1], r2[2], r2[3], r2[4])
    
    intersection = rect1
    # Loop over the edges of the second rectangle
    for p, q in zip(rect2, rect2[1:] + rect2[:1]):
        if len(intersection) <= 2:
            break # No intersection

        a,b,c = Vec_Line(p[0],p[1],q[0],q[1])
        
        # Any point p with line(p) <= 0 is on the "inside" (or on the boundary),
        # any point p with line(p) > 0 is on the "outside".

        # Loop over the edges of the intersection polygon,
        # and determine which part is inside and which is outside.
        new_intersection = []
        line_values = [Line_point(a,b,c,t[0],t[1]) for t in intersection]
        for s, t, s_value, t_value in zip(
            intersection, intersection[1:] + intersection[:1],
            line_values, line_values[1:] + line_values[:1]):
            if s_value <= 0:
                new_intersection.append(s)
            if s_value * t_value < 0:
                # Points are on opposite sides.
                # Add the intersection of the lines to new_intersection.
                a2, b2, c2 = Vec_Line(s[0],s[1],t[0],t[1])
                x = Line_intersectionx(a, b, c, a2, b2,c2)
                y = Line_intersectiony(a, b, c, a2, b2,c2)
                intersection_point = (x,y)
                new_intersection.append(intersection_point)

        intersection = new_intersection

    # Calculate area
    if len(intersection) <= 2:
        return 0

    intersectionArea = 0.5 * sum(p[0]*q[1] - p[1]*q[0] for p, q in zip(intersection, intersection[1:] + intersection[:1]))
    iou = intersectionArea/(area1+area2-intersectionArea)
    
    return iou
                     

cpdef bbox_overlaps(
        np.ndarray[DTYPE_t, ndim=2] boxes,
        np.ndarray[DTYPE_t, ndim=2] query_boxes):
    """
    Parameters
    ----------
    boxes: (N, 5) ndarray of float
    query_boxes: (K, 5) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    cdef unsigned int N = boxes.shape[0]
    cdef unsigned int K = query_boxes.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=2] overlaps = np.zeros((N, K), dtype=DTYPE)
    cdef DTYPE_t iw, ih, box_area
    cdef DTYPE_t ua
    cdef unsigned int k, n
    for k in range(K):
        for n in range(N):
            overlaps[n, k] = intersection_area(query_boxes[k,:],boxes[n,:])	

    return overlaps

cdef inline np.float32_t max(np.float32_t a, np.float32_t b):
    return a if a >= b else b

cdef inline np.float32_t min(np.float32_t a, np.float32_t b):
    return a if a <= b else b

cpdef cpu_nms(np.ndarray[np.float32_t, ndim=2] dets,
        np.ndarray[np.float32_t, ndim=1] scores, np.float thresh, np.int nmax):
	
    cdef np.ndarray[np.int_t, ndim=1] order = scores.argsort()[::-1]
    cdef int ndets = dets.shape[0]
    cdef np.ndarray[np.int_t, ndim=1] suppressed = \
            np.zeros((ndets), dtype=np.int)
    cdef list keep
    cdef np.float32_t ovr
    
    # nominal indices
    cdef int _i, _j

    # sorted indices
    cdef int i, j   

    keep = []
    for _i in range(ndets):
        i = order[_i]
        if suppressed[i] == 1:
            continue
        keep.append(i)

        if len(keep)==nmax:
            return keep
        
        for _j in range(_i + 1, ndets):
            j = order[_j]
            if suppressed[j] == 1:
                continue
            ovr = intersection_area(dets[i,:],dets[j,:])

            if ovr >= thresh:
                suppressed[j] = 1

    return keep



