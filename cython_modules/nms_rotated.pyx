#cython: profile=True
## Compiler directives
# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False

from shapely.geometry import Polygon
cimport cython
import numpy as np
cimport numpy as np
from cython.parallel import prange

DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

from math import pi, cos, sin


cdef class Vector:
    cdef public DTYPE_t x,y
    
    def __init__(self, DTYPE_t x, DTYPE_t y):
        self.x = x
        self.y = y

    def __add__(self, Vector v):
#        if not isinstance(v, Vector):	
#            return NotImplemented
        cdef DTYPE_t vx = self.x + v.x
        cdef DTYPE_t vy = self.y + v.y		
        cdef Vector vsum = Vector(vx, vy)
        return vsum

    def __sub__(self, Vector v):
#        if not isinstance(v, Vector):
#            return NotImplemented
        cdef DTYPE_t vx = self.x - v.x
        cdef DTYPE_t vy = self.y - v.y		
        cdef Vector vsum = Vector(vx,vy)
        return vsum

    cdef cross(self, Vector v):
#        if not isinstance(v, Vector):
#            return NotImplemented
        cdef DTYPE_t result = self.x*v.y - self.y*v.x
        return result

cdef class Line:
    # ax + by + c = 0
    cdef public DTYPE_t a,b,c

    def __init__(self, Vector v1, Vector v2):
        self.a = v2.y - v1.y
        self.b = v1.x - v2.x
        self.c = v2.cross(v1)

    def __call__(self, Vector p):
        cdef DTYPE_t result= self.a*p.x + self.b*p.y + self.c
        return result

    cdef intersection(self, Line other):
        # See e.g.     https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Using_homogeneous_coordinates
#        if not isinstance(other, Line):
#            return NotImplemented
        cdef DTYPE_t w = self.a*other.b - self.b*other.a
        cdef DTYPE_t vx = (self.b*other.c - self.c*other.b)/w
        cdef DTYPE_t vy = (self.c*other.a - self.a*other.c)/w                               	
        cdef Vector v = Vector(vx,vy)
        return v
          

cdef rectangle_vertices(DTYPE_t cy, DTYPE_t cx, DTYPE_t h, DTYPE_t w, DTYPE_t angle):
    cdef DTYPE_t area = w*h
    #PI / 180. == 0.01745329251
    cdef DTYPE_t angle_rad = angle*0.01745329251
    cdef DTYPE_t dx = w/2
    cdef DTYPE_t dy = h/2
    cdef DTYPE_t dxcos = dx*cos(angle_rad)
    cdef DTYPE_t dxsin = dx*sin(angle_rad)
    cdef DTYPE_t dycos = dy*cos(angle_rad)
    cdef DTYPE_t dysin = dy*sin(angle_rad)
    
    cdef Vector v0 = Vector(cx, cy)
    
    cdef Vector v1 = v0+Vector(-dxcos + dysin, -dxsin - dycos)
    
    cdef Vector v2 = v0+Vector( dxcos + dysin,  dxsin - dycos)
    
    cdef Vector v3 = v0+Vector( dxcos - dysin,  dxsin + dycos)
       
    cdef Vector v4 = v0+Vector(-dxcos - dysin, -dxsin + dycos)

    return v1,v2,v3,v4,area


cdef int neighbor(np.ndarray[DTYPE_t, ndim=1] r1, np.ndarray[DTYPE_t, ndim=1] r2):
    
    cdef DTYPE_t L1 = max(r1[2],r1[3])
    cdef DTYPE_t L2 = max(r2[2],r2[3])
    cdef DTYPE_t y11,x11,y12,x12
    cdef DTYPE_t y21,x21,y22,x22
    cdef int neighbor
    y11 = r1[0] - 0.5*L1
    x11 = r1[1] - 0.5*L1  
    y12 = r1[0] + 0.5*L1
    x12 = r1[1] + 0.5*L1
    y21 = r2[0] - 0.5*L2
    x21 = r2[1] - 0.5*L2  
    y22 = r2[0] + 0.5*L2
    x22 = r2[1] + 0.5*L2
    iw = min(y12, y22) - max(y11, y21)
    ih = min(x12, x22) - max(x11, x21) 

    if (iw>0 and ih>0):
        neighbor = 1
    else:
        neighbor = 0

    return neighbor


cdef DTYPE_t intersection_area(np.ndarray[DTYPE_t, ndim=1] r1, np.ndarray[DTYPE_t, ndim=1] r2):

    cdef DTYPE_t area1,area2,s_value,t_value,iou
    cdef DTYPE_t intersectionArea
    cdef Vector v1,v2,v3,v4,p,q,s,t,intersection_point
    cdef Line line,newLine
    cdef list rect1,rect2,intersection
    cdef list new_intersection,line_values
    
    # r1 and r2 are in (center, width, height, rotation) representation
    # First convert these into a sequence of vertices

    if not neighbor(r1,r2):
        return 0
	
    v1,v2,v3,v4,area1 = rectangle_vertices(r1[0],r1[1],r1[2],r1[3],r1[4])
    rect1 = [v1,v2,v3,v4]
    v1,v2,v3,v4,area2 = rectangle_vertices(r2[0],r2[1],r2[2],r2[3],r2[4])
    rect2 = [v1,v2,v3,v4]
    
    # Use the vertices of the first rectangle as
    # starting vertices of the intersection polygon.
    intersection = rect1

    # Loop over the edges of the second rectangle
    for p, q in zip(rect2, rect2[1:] + rect2[:1]):
        if len(intersection) <= 2:
            break # No intersection

        line = Line(p, q)

        # Any point p with line(p) <= 0 is on the "inside" (or on the boundary),
        # any point p with line(p) > 0 is on the "outside".

        # Loop over the edges of the intersection polygon,
        # and determine which part is inside and which is outside.
        new_intersection = []
        line_values = [line(t) for t in intersection]
        for s, t, s_value, t_value in zip(
            intersection, intersection[1:] + intersection[:1],
            line_values, line_values[1:] + line_values[:1]):
            if s_value <= 0:
                new_intersection.append(s)
            if s_value * t_value < 0:
                # Points are on opposite sides.
                # Add the intersection of the lines to new_intersection
                newLine = Line(s, t)
                intersection_point = line.intersection(newLine)
                new_intersection.append(intersection_point)

        intersection = new_intersection

    # Calculate area
    if len(intersection) <= 2:
        return 0

    intersectionArea = 0.5 * sum(p.x*q.y - p.y*q.x for p, q in zip(intersection, intersection[1:] + intersection[:1]))
    iou = intersectionArea/(area1+area2-intersectionArea)

    return iou
                     

cpdef bbox_overlaps(
        np.ndarray[DTYPE_t, ndim=2] boxes,
        np.ndarray[DTYPE_t, ndim=2] query_boxes):
    """
    Parameters
    ----------
    boxes: (N, 5) ndarray of float
    query_boxes: (K, 5) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    cdef unsigned int N = boxes.shape[0]
    cdef unsigned int K = query_boxes.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=2] overlaps = np.zeros((N, K), dtype=DTYPE)
    cdef DTYPE_t iw, ih, box_area
    cdef DTYPE_t ua
    cdef unsigned int k, n
    for k in range(K):
        for n in range(N):
            overlaps[n, k] = intersection_area(query_boxes[k,:],boxes[n,:])	

    return overlaps

cdef inline np.float32_t max(np.float32_t a, np.float32_t b):
    return a if a >= b else b

cdef inline np.float32_t min(np.float32_t a, np.float32_t b):
    return a if a <= b else b

cpdef cpu_nms(np.ndarray[np.float32_t, ndim=2] dets,
        np.ndarray[np.float32_t, ndim=1] scores, np.float thresh, np.int nmax):
	
    cdef np.ndarray[np.int_t, ndim=1] order = scores.argsort()[::-1]
    cdef int ndets = dets.shape[0]
    cdef np.ndarray[np.int_t, ndim=1] suppressed = \
            np.zeros((ndets), dtype=np.int)
    cdef list keep
    cdef np.float32_t ovr
    
    # nominal indices
    cdef int _i, _j

    # sorted indices
    cdef int i, j   

    keep = []
    for _i in range(ndets):
        i = order[_i]
        if suppressed[i] == 1:
            continue
        keep.append(i)

        if len(keep)==nmax:
            return keep
        
        for _j in range(_i + 1, ndets):
            j = order[_j]
            if suppressed[j] == 1:
                continue
            ovr = intersection_area(dets[i,:],dets[j,:])

            if ovr >= thresh:
                suppressed[j] = 1

    return keep

