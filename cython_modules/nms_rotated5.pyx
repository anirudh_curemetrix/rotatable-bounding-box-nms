cimport cython
import numpy as np 
cimport numpy as np
import cv2
import time

DTYPE = np.float32
ctypedef np.float32_t DTYPE_t
ctypedef bint BOOL

@cython.cdivision(True)
@cython.boundscheck(False)
cdef DTYPE_t two_boxes_iou(np.ndarray[DTYPE_t, ndim=1] rectangle_1, np.ndarray[DTYPE_t, ndim=1] rectangle_2):

    """
	calu rectangle_1 and rectangle_2 iou
    :param rectangle_1: [x, y, w, h, theta]. shape: (5, )
    :param rectangle_2:
    :return:
    """
    cdef DTYPE_t area1 = rectangle_1[2] * rectangle_1[3]
    cdef DTYPE_t area2 = rectangle_2[2] * rectangle_2[3]
    cdef DTYPE_t epsilon = 0.00000001

    rect_1 = ((rectangle_1[0], rectangle_1[1]), (rectangle_1[2], rectangle_1[3]), rectangle_1[4])
    rect_2 = ((rectangle_2[0], rectangle_2[1]), (rectangle_2[2], rectangle_2[3]), rectangle_2[4])

    inter_points = cv2.rotatedRectangleIntersection(rect_1, rect_2)[1]

    cdef np.ndarray[DTYPE_t, ndim=3] order_points
    cdef float inter_area, iou
    if inter_points is not None:
        order_points = cv2.convexHull(inter_points, returnPoints=True)

        inter_area = cv2.contourArea(order_points)
        iou = inter_area *1.0 / (area1 + area2 - inter_area + epsilon)
        return <DTYPE_t> iou
    else:
        return <DTYPE_t> 0.0
    

def bbox_overlaps(
        np.ndarray[DTYPE_t, ndim=2] boxes,
        np.ndarray[DTYPE_t, ndim=2] query_boxes):
    """
    Parameters
    ----------
    boxes: (N, 5) ndarray of float
    query_boxes: (K, 5) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    cdef unsigned int N = boxes.shape[0]
    cdef unsigned int K = query_boxes.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=2] overlaps = np.zeros((N, K), dtype=np.float32)
    cdef unsigned int k, n
    for k in range(K):
        for n in range(N):
            overlaps[n, k] = two_boxes_iou(query_boxes[k,:],boxes[n,:])	

    return overlaps

@cython.cdivision(True)
@cython.boundscheck(False)
def cpu_nms(np.ndarray[np.float32_t, ndim=2] dets,
        np.ndarray[np.float32_t, ndim=1] scores, float thresh, int nmax):

    cdef np.ndarray[np.int_t, ndim=1] order = scores.argsort()[::-1]
    cdef unsigned int ndets = dets.shape[0]
    cdef np.ndarray[np.int_t, ndim=1] suppressed = \
            np.zeros((ndets), dtype=np.int)
    cdef list keep
    
    # nominal indices
    cdef unsigned int _i, _j

    # sorted indices
    cdef unsigned int i, j   
    
    cdef np.ndarray[DTYPE_t, ndim=3] order_points
    cdef float int_area
    cdef DTYPE_t area_r1
    cdef DTYPE_t area_r2
    cdef float iou
    
    keep = []
    for _i in range(ndets):
        
        if len(keep)==nmax:
            return keep
        
        i = order[_i]
        if suppressed[i] == 1:
            continue
        keep.append(i)

        r1 = ((dets[i, 0], dets[i, 1]), (dets[i, 2], dets[i, 3]), dets[i, 4])
        area_r1 = dets[i, 2] * dets[i, 3]
        
        for _j in range(_i + 1, ndets):
            j = order[_j]
            if suppressed[j] == 1:# Confirm if it is suppressed of j or i 
                continue
                
            r2 = ((dets[j, 0], dets[j, 1]), (dets[j, 2], dets[j, 3]), dets[j, 4])
            area_r2 = dets[j, 2] * dets[j, 3]
            iou = 0.0
            
            int_pts = cv2.rotatedRectangleIntersection(r1, r2)[1]
            if int_pts is not None:
                order_pts = cv2.convexHull(int_pts, returnPoints=True)

                int_area = cv2.contourArea(order_pts)

                iou = int_area *1.0/(area_r1 + area_r2 - int_area)

            if iou >= thresh:
                suppressed[j] = 1

    return np.array(keep, np.int64)