#cython: language_level=3
cimport cython
import numpy as np
cimport numpy as np

# DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

#from math import pi, cos, sin
from libc.math cimport sin, cos

cdef DTYPE_t Vector_Add_simple(DTYPE_t x1, DTYPE_t x2):
    return x1 + x2

@cython.cdivision(True)
cdef Vec_Line(DTYPE_t x1, DTYPE_t y1, DTYPE_t x2,DTYPE_t y2):
    cdef DTYPE_t p[3]
    p[0] = y2 - y1
    p[1] = x1 - x2
    p[2] = x2*y1 - y2*x1
    return p

cdef DTYPE_t Line_point(DTYPE_t a,DTYPE_t b,DTYPE_t c, DTYPE_t x1, DTYPE_t y1):
    return a*x1 + b*y1 + c

@cython.cdivision(True)
cdef DTYPE_t Line_intersectionx(DTYPE_t a1,DTYPE_t b1, DTYPE_t c1, DTYPE_t a2, DTYPE_t b2, DTYPE_t c2):
    cdef DTYPE_t w
    cdef DTYPE_t x 
    w = a1*b2 - b1*a2
    x = (b1*c2 - c1*b2)/w
    return x

@cython.cdivision(True)
cdef DTYPE_t Line_intersectiony(DTYPE_t a1, DTYPE_t b1, DTYPE_t c1, DTYPE_t a2, DTYPE_t b2, DTYPE_t c2):
    cdef DTYPE_t w
    cdef DTYPE_t y
    w = a1*b2 - b1*a2
    y = (c1*a2 - a1*c2)/w
    return y

@cython.cdivision(True)
@cython.boundscheck(False)
cdef rectangle_vertices(DTYPE_t cy, DTYPE_t cx, DTYPE_t h, DTYPE_t w, DTYPE_t angle):
    cdef DTYPE_t area = w*h
    #PI / 180. == 0.01745329251
    cdef DTYPE_t angle_rad = angle*0.01745329251
    cdef DTYPE_t dx = w/2
    cdef DTYPE_t dy = h/2
    cdef DTYPE_t dxcos = dx*cos(angle_rad)
    cdef DTYPE_t dxsin = dx*sin(angle_rad)
    cdef DTYPE_t dycos = dy*cos(angle_rad)
    cdef DTYPE_t dysin = dy*sin(angle_rad)
    
    cdef DTYPE_t x1,y1,x2,y2,x3,y3,x4,y4
    cdef list result
    #cdef int p[1000] ---- Keep this in mind, we can may be try this 
    # x1 - p[0], x2 = p[1], x3 = p[2] etc
    # This will basically remove the python list conversion 
    #cdef np.ndarray[DTYPE_t, ndim=2] result = np.zeros((4, 2), dtype=np.float32)
    
    
    x1  = Vector_Add_simple(cx, -dxcos - -dysin)
    y1  = Vector_Add_simple(cy, -dxsin + -dycos)
    x2 = Vector_Add_simple(cx, dxcos - -dysin)
    y2 = Vector_Add_simple(cy, dxsin + -dycos)
    
    x3 = Vector_Add_simple(cx, dxcos -  dysin )
    y3 = Vector_Add_simple(cy, dxsin +  dycos)
    x4 = Vector_Add_simple(cx, -dxcos -  dysin )
    y4 = Vector_Add_simple(cy, -dxsin +  dycos)
    
    result = [(x1,y1), (x2,y2), (x3,y3), (x4,y4)]
    
    return result

cdef inline DTYPE_t max_dtype(DTYPE_t a, DTYPE_t b):
    return a if a >= b else b

cdef inline DTYPE_t min_dtype(DTYPE_t a, DTYPE_t b):
    return a if a <= b else b

@cython.cdivision(True)
@cython.boundscheck(False)
cdef int neighbor(np.ndarray[DTYPE_t, ndim=1] r1, np.ndarray[DTYPE_t, ndim=1] r2):
     
    #cdef DTYPE_t L1 = r1[2] if (r1[2] >= r1[3]) else r1[3] #max(r1[2],r1[3])
    #cdef DTYPE_t L2 = r2[2] if (r2[2] >= r2[3]) else r2[3] #max(r2[2],r2[3])
    cdef DTYPE_t L1 
    L1 = max_dtype(r1[2],r1[3])
    cdef DTYPE_t L2  
    L2 = max_dtype(r2[2],r2[3])
    
    cdef DTYPE_t y11,x11,y12,x12
    cdef DTYPE_t y21,x21,y22,x22
    #cdef int neighbor
    cdef DTYPE_t iw, iw1, iw2, ih, ih1, ih2 
    #cdef int iw, ih
    
    y11 = r1[0] - 0.5*L1
    x11 = r1[1] - 0.5*L1  
    y12 = r1[0] + 0.5*L1
    x12 = r1[1] + 0.5*L1
    
    y21 = r2[0] - 0.5*L2
    x21 = r2[1] - 0.5*L2  
    y22 = r2[0] + 0.5*L2
    x22 = r2[1] + 0.5*L2
    
    #iw1 = y12 if (y12 <= y22) else y22 #min(y12, y22)
    #iw2 = y11 if (y11 >= y21) else y21 #max(y11, y21)
    
    #ih1 = x12 if (x12 <= x22) else x22 #min(x12, x22)
    #ih2 = x11 if (x11 >= x21)else x21 #max(x11, x21) 
    
    iw = min_dtype(y12, y22) - max_dtype(y11, y21)
    ih = min_dtype(x12, x22) - max_dtype(x11, x21) 
    #iw = iw1 - iw2 
    #ih = ih1 - ih2

#     if (iw>0 && ih>0):
#         neighbor = 1
#     else:
#         neighbor = 0
    if iw > 0:
        if ih > 0:
            neighbor = 1
        else:
            neighbor = 0
    else:
        neighbor = 0

    #return 1 if (iw>0 and ih>0) else 0 #neighbor
    return neighbor 

@cython.cdivision(True)
@cython.boundscheck(False)
cdef DTYPE_t intersection_area(np.ndarray[DTYPE_t, ndim=1] r1, np.ndarray[DTYPE_t, ndim=1] r2):
    
    cdef DTYPE_t area1,area2,s_value,t_value,iou
    cdef DTYPE_t intersectionArea
    cdef tuple p,q,s,t,u, v
    cdef DTYPE_t a,b,c, a2, b2, c2
    cdef list rect1,rect2,intersection
    cdef list new_intersection,line_values
    cdef DTYPE_t x, y
    cdef int t1
    cdef DTYPE_t vec1[3], vec2[3]
    
    if not neighbor(r1,r2):
        return 0
    
    rect1 = rectangle_vertices(r1[0], r1[1], r1[2], r1[3], r1[4])
    rect2 = rectangle_vertices(r2[0], r2[1], r2[2], r2[3], r2[4])
    
    area1 = r1[2]*r1[3]
    area2 = r2[2]*r2[3]
    
    intersection = rect1
    # Loop over the edges of the second rectangle
    #for _p in range(4):#_p declare p 
        #if intersection.shape[0] < 2:
    cdef int i, j, k, N
    cdef list rect2_copy = rect2[1:] + rect2[:1]
    
    cdef list intersection_copy 
    cdef list line_values_copy  
    
    for i in range(4):
        N = len(intersection)
        p = rect2[i]
        q = rect2_copy[i]
        
        if N <=2:
            break
            
        vec1 = Vec_Line(p[0],p[1],q[0],q[1])
        a = vec1[0]
        b = vec1[1]
        c = vec1[2]
        
        new_intersection = []
        line_values = []
        for t1 in range(N):
            line_values.append(Line_point(a,b,c,intersection[t1][0],intersection[t1][1]))
        
        line_values_copy  = line_values[1:] + line_values[:1]
        intersection_copy = intersection[1:] + intersection[:1]
        
        for j in range(N):
            #print(intersection_copy[j])
            s = intersection[j]
            t = intersection_copy[j]
            s_value = line_values[j]
            t_value = line_values_copy[j]
            
            if s_value <= 0:
                new_intersection.append(s)
            if s_value * t_value < 0:
                # Points are on opposite sides.
                # Add the intersection of the lines to new_intersection.
                vec2 = Vec_Line(s[0],s[1],t[0],t[1])
                a2 = vec2[0]
                b2 = vec2[1]
                c2 = vec2[2]
                x = Line_intersectionx(a, b, c, a2, b2,c2)
                y = Line_intersectiony(a, b, c, a2, b2,c2)
                new_intersection.append((x,y))
                #print(new_intersection)

        intersection = new_intersection
    
    intersection_copy = intersection[1:] + intersection[:1]
    # Calculate area
    N = len(intersection)
    if N <= 2:
        return 0
    
    for k in range(N):
        u = intersection[k]
        v = intersection_copy[k]
        intersectionArea += 0.5*(u[0]*v[1] - u[1]*v[0])
    iou = intersectionArea/(area1+area2-intersectionArea)
    
    return iou
            
#     for p, q in zip(rect2, rect2[1:] + rect2[:1]):
#         if len(intersection) <= 2:
#             break # No intersection

#         vec1 = Vec_Line(p[0],p[1],q[0],q[1])
#         a = vec1[0]
#         b = vec1[1]
#         c = vec1[2]
#         # Any point p with line(p) <= 0 is on the "inside" (or on the boundary),
#         # any point p with line(p) > 0 is on the "outside".

#         # Loop over the edges of the intersection polygon,
#         # and determine which part is inside and which is outside.
#         new_intersection = []
#         line_values = []
#         for t1 in range(4):
#             line_values.append(Line_point(a,b,c,intersection[t][0],intersection[t][1]))
#         #line_values = [Line_point(a,b,c,t[0],t[1]) for t in intersection]
#         for s, t, s_value, t_value in zip(
#             intersection, intersection[1:] + intersection[:1],
#             line_values, line_values[1:] + line_values[:1]):
#             if s_value <= 0:
#                 new_intersection.append(s)
#             if s_value * t_value < 0:
#                 # Points are on opposite sides.
#                 # Add the intersection of the lines to new_intersection.
#                 vec2 = Vec_Line(s[0],s[1],t[0],t[1])
#                 a2 = vec2[0]
#                 b2 = vec2[1]
#                 c2 = vec2[2]
#                 x = Line_intersectionx(a, b, c, a2, b2,c2)
#                 y = Line_intersectiony(a, b, c, a2, b2,c2)
#                 new_intersection.append((x,y))

#         intersection = new_intersection

#     # Calculate area
#     if len(intersection) <= 2:
#         return 0

#     intersectionArea = 0.5 * sum(p[0]*q[1] - p[1]*q[0] for p, q in zip(intersection, intersection[1:] + intersection[:1]))
#     iou = intersectionArea/(area1+area2-intersectionArea)
    
#     return iou
                     

def bbox_overlaps(
        np.ndarray[DTYPE_t, ndim=2] boxes,
        np.ndarray[DTYPE_t, ndim=2] query_boxes):
    """
    Parameters
    ----------
    boxes: (N, 5) ndarray of float
    query_boxes: (K, 5) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    cdef unsigned int N = boxes.shape[0]
    cdef unsigned int K = query_boxes.shape[0]
    cdef np.ndarray[DTYPE_t, ndim=2] overlaps = np.zeros((N, K), dtype=np.float32)
    cdef DTYPE_t iw, ih, box_area
    cdef DTYPE_t ua
    cdef unsigned int k, n
    for k in range(K):
        for n in range(N):
            overlaps[n, k] = intersection_area(query_boxes[k,:],boxes[n,:])	

    return overlaps



def cpu_nms(np.ndarray[np.float32_t, ndim=2] dets,
        np.ndarray[np.float32_t, ndim=1] scores, float thresh, int nmax):

    cdef np.ndarray[np.int_t, ndim=1] order = scores.argsort()[::-1]
    cdef int ndets = dets.shape[0]
    cdef np.ndarray[np.int_t, ndim=1] suppressed = \
            np.zeros((ndets), dtype=np.int)
    cdef list keep
    cdef np.float32_t ovr
    
    # nominal indices
    cdef int _i, _j

    # sorted indices
    cdef int i, j   

    keep = []
    for _i in range(ndets):
        i = order[_i]
        if suppressed[i] == 1:
            continue
        keep.append(i)

        if len(keep)==nmax:
            return keep
        
        for _j in range(_i + 1, ndets):
            j = order[_j]
            if suppressed[j] == 1:
                continue
            ovr = intersection_area(dets[i,:],dets[j,:])

            if ovr >= thresh:
                suppressed[j] = 1

    return keep




