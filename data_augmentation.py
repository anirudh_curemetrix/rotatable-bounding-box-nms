from __future__ import division

import os
import cv2
import glob
import sys
import numpy as np
import yaml
from scipy.interpolate import RectBivariateSpline

from skimage.transform import resize as imresize
from skimage.transform import rotate as imrotate
import logging
import tensorflow as tf


class DataAugmentor:

    def __init__(self,  img_height, img_width, config_yaml='data_augmentation.yaml'):
        with open(config_yaml) as fp:
            self.params = yaml.load(fp)
        logging.info(self.params)
        self.img_width  = img_width
        self.img_height = img_height
        self.x = np.linspace(-0.5,0.5, img_width) 
        self.y = np.linspace(-0.5,0.5, img_height)
        self.X,self.Y = np.meshgrid(self.x,self.y)
        R = np.sqrt(self.X**2 + self.Y**2)
        self.theta0 = np.exp(-3.0*R )
        self.transformations = self.params.keys()
        self.probs = np.zeros(len(self.transformations))
        for (n,trans) in enumerate(self.transformations):
            if 'prob' in self.params[trans]:
                self.probs[n] = self.params[trans]['prob']
            else:
                self.probs[n] = 1.0/float(len(self.transformations))

        self.probs = self.probs / self.probs.sum()
        self.cumprobs = np.cumsum(self.probs)  # the last element should be 1
        self.count = 0 

    def pyinterp2lin(self, img, P, Q, fill_value=0):
        # expects a 2D img
        f = RectBivariateSpline(self.y, self.x, img, kx=1, ky=1)
        p = P.ravel()
        q = Q.ravel()
        z = f.ev(q, p)
        out_of_bounds_x = (p < self.x[0]) | (p > self.x[-1])
        out_of_bounds_y = (q < self.y[0]) | (q > self.y[-1])
        any_out_of_bounds_x = np.any(out_of_bounds_x)
        any_out_of_bounds_y = np.any(out_of_bounds_y)
        if any_out_of_bounds_x:
            z[out_of_bounds_x] = fill_value
        if any_out_of_bounds_y:
            z[out_of_bounds_y] = fill_value

        return z.reshape((self.img_height,self.img_width))


    def _rotate_boxes(self, bb, theta0, input_shape, output_shape, doDiffRot=False):
        """
        Rotate each of the bounding boxes around the origin by theta degrees
        theta0 is either the rotation angle for the angle multipler for diffrot
        Each corner is rotated and then a new box is draw to enclose all four points
        """

        # the origin of rotation in the middle of the image
        x0 = float(input_shape[1])*0.5
        y0 = float(input_shape[0])*0.5

        s_x = output_shape[1]/float(input_shape[1])
        s_y = output_shape[0]/float(input_shape[0])

        numGT = bb.shape[0]
        # x coords of each corner (relative to center)
        xi = np.stack([bb[:,1], bb[:,1], bb[:,3], bb[:,3]]) - x0
        # x coords of each corner (relative to center) shape (4, numGT)
        yi = np.stack([bb[:,0], bb[:,2], bb[:,0], bb[:,2]]) - y0

        # shape (4*numGT)
        xi = np.reshape(xi, -1)
        yi = np.reshape(yi, -1)

        if doDiffRot:  # for diffrot the theta depends on position
            r = np.sqrt((0.5*xi/x0)**2 + (0.5*yi/y0)**2)
            theta_rad = -theta0*np.exp(-3.0*r)  #still confused about this negative 
        else:  # for rotation theta is a constant scalar
            theta_rad = theta0 * np.ones(4*numGT)

        # rotate the 4 corners of all boxes
        xc_rot =  xi*np.cos(theta_rad) + yi*np.sin(theta_rad) + x0
        yc_rot = -xi*np.sin(theta_rad) + yi*np.cos(theta_rad) + y0

        # reshape back to (4, numGT) and scale
        xc_rot = s_x*np.reshape(xc_rot, (4, numGT))
        yc_rot = s_y*np.reshape(yc_rot, (4, numGT))

        xmin = np.min(xc_rot, axis=0)
        xmax = np.max(xc_rot, axis=0)
        ymin = np.min(yc_rot, axis=0)
        ymax = np.max(yc_rot, axis=0)

        bboxes_da = np.zeros_like(bb)
        # clip in case it goes out of bounds
        bboxes_da[:,0] = np.clip(ymin, 0, output_shape[0]-1)
        bboxes_da[:,1] = np.clip(xmin, 0, output_shape[1]-1)
        bboxes_da[:,2] = np.clip(ymax, 0, output_shape[0]-1)
        bboxes_da[:,3] = np.clip(xmax, 0, output_shape[1]-1)

        # find any boxes with non positive width or height
        l_remove = (bboxes_da[:,3]<= bboxes_da[:,1]) | (bboxes_da[:,2]<=bboxes_da[:,0])
        # set bad boxes to all zeros which is a placeholder 
        bboxes_da[l_remove,:] = 0 

        return bboxes_da


    def diffrot(self, img, bboxes, params):
        
        p = params[0] * (2*np.random.rand()-1)
        print('diffrot: %f' %p )

        theta = p*self.theta0
        sinTheta = np.sin(theta)
        cosTheta = np.cos(theta)
        P =  self.X*cosTheta + self.Y*sinTheta
        Q = -self.X*sinTheta + self.Y*cosTheta
        out = np.zeros_like(img)
        out[:,:,0] = self.pyinterp2lin(img[:,:,0],P,Q,fill_value=0)
        out[:,:,1] = out[:,:,0]
        out[:,:,2] = out[:,:,0]

        # count the number of nonzero rows of bboxes, these are the real GTs
        # they should come first 
        numGT = sum([np.any(bboxes[i,:]) for i in range(bboxes.shape[0])])
        bboxes_da = bboxes.copy()

        if numGT>0:
            bboxes_da[:numGT,:] = self._rotate_boxes( bboxes_da[:numGT,:], p, img.shape, img.shape, doDiffRot=True)

        return out, bboxes_da

    def identity(self, img, bboxes, params):
        print('identity')
        return img, bboxes

    def rotate(self, img, bboxes, params):

        theta = (2*np.random.rand()-1) * params[0]  # rotation angle in degress
        theta_rad = theta *np.pi/180.  # in radians
        print('rotate: %f' % theta)

        # precalucated required padding for rotating 2048x1600 image up to 20 degrees
        # insert the calculation here to generalize
        pad_x = 350
        pad_y = 212
        img_pad = np.pad(img, ((pad_y, pad_y),(pad_x,pad_x), (0,0)), 'constant')

        # rotate theta degrees CCW
        img_rot = imrotate(img_pad, theta, resize=False)
        # resize the image back to the original size
        img_out = imresize(img_rot, img.shape, mode='constant', preserve_range=True, clip=True)

        # count the number of nonzero rows of bboxes, these are the real GTs
        # they should come first 
        numGT = sum([np.any(bboxes[i,:]) for i in range(bboxes.shape[0])])
        bboxes_pad = bboxes.copy()

        if numGT>0:
            # bboxes in the padded image
            bboxes_pad[:numGT,0]+= pad_y
            bboxes_pad[:numGT,1]+= pad_x
            bboxes_pad[:numGT,2]+= pad_y
            bboxes_pad[:numGT,3]+= pad_x
            bboxes_pad[:numGT, :] = self._rotate_boxes( bboxes_pad[:numGT,:], theta_rad, img_rot.shape, img_out.shape)

        return img_out, bboxes_pad

    def imadjust(self, img, bboxes, params):

        gamma = 1.0 + (2*np.random.rand()-1) * params[0]
        print('imadjust: %f' % gamma)
        img = 255*((img/255)**gamma)  # img type is already float32 but has max of 255
        return img, bboxes


    def blur(self, img, bboxes, params):
        p = params[0] + np.random.rand()*(params[1]-params[0])
        print('blur: %f' % p)        
        img = cv2.GaussianBlur(img, ksize=(0,0), sigmaX=p, sigmaY=p)
        return img, bboxes


    def jitter(self, img, bboxes, params):
        # random magnitude between p0 and p1
        mag = params[0] + np.random.rand()*(params[1]-params[0])
        print('jitter: %f' % mag)
        noise = 2*mag*np.random.random((img.shape[0], img.shape[1])) - mag
        img = img + noise[...,np.newaxis]  # add the same noise onto each channel
        return img, bboxes


    def log_transform(self, img, bboxes, params):
        gamma = params[0] + np.random.rand()*(params[1]-params[0])
        print('log_transform: %f'% gamma)
        img = img/ img.max()
        img = np.log(1.+img)**gamma
        img = (255.*np.log(2.)**-gamma) * img
        return img, bboxes


    def imoffset(self, img, bboxes, params):
        # select random offset between +p0 and +p1 or between -p1 and -p0
        r = 2*np.random.rand()-1  # ~U(-1, 1)
        offset = params[0] + np.abs(r)*(params[1] - params[0]) # ~U(p0, p1)
        offset *= np.sign(r)
        print('imoffset: %f' % offset)
        img = img + offset
        img = np.clip(img, 0, 255)
        return img, bboxes



    # img should have intensities between 0 and 255
    def doAugmentation(self, img, bboxes):
        # convert to numpy
        bboxes = bboxes.numpy()
        img = img.numpy()

        p = np.random.rand()  # p ~ U(0,1)
        for (n,trans) in enumerate(self.transformations):
            # select just one transformation with the specified probability
            if p <= self.cumprobs[n]:
                augmentation_fn = getattr(self, trans)
                img, bboxes = augmentation_fn(img, bboxes, self.params[trans]['params'])
                break

       # self.save_image(img, bboxes, trans)
        return img, bboxes



    def save_image(self, img, bboxes, trans):
        img = np.squeeze(img)
        img = np.clip(img, 0, 255)
        img = img.astype(np.uint8)
        pngName = '%05d_%s.png'%(self.count, trans)
        self.count += 1
        for i in range(bboxes.shape[0]):
            bbi = bboxes.astype(np.int)[i,:]
            cv2.rectangle(img, (bbi[1], bbi[0]), (bbi[3], bbi[2]), (0,0,255), 1)
        
        cv2.imwrite(pngName, img)
        print(pngName)


if __name__=='__main__':
    img = cv2.imread('test.png')
    print(img.shape)
    img = img.astype(np.float)
    print(img.max())
    #img = img[:,:,0:1]
    np.random.seed(42)

    da = DataAugmentor(img.shape[0], img.shape[1])
    dx = 5
    bboxes = np.array([[247, 930, 367, 1107], [1024-dx, 800-dx, 1024+dx,800+dx], [973,1380,1037,1470]])
    #bboxes = np.array([[247, 930, 367, 1107]])

    for n in range(10):
        img_da, bboxes_da = da.doAugmentation(img.astype(np.float), bboxes)
        print(img_da.shape)
        print(img_da.min())
        print(img_da.max())

        img_da[:,:,1] = 0.
        img_da[:,:,2] = 0.
        ibboxes = np.rint(bboxes_da).astype(np.int)
        for i in range(ibboxes.shape[0]):
            iy1 = np.clip(ibboxes[i,0], 0, img.shape[0]-1)
            ix1 = np.clip(ibboxes[i,1], 0, img.shape[1]-1)
            iy2 = np.clip(ibboxes[i,2], 0, img.shape[0]-1)
            ix2 = np.clip(ibboxes[i,3], 0, img.shape[1]-1)
            img_da[iy1:iy2, ix1:ix2, 1] = 128.0

        cv2.imwrite('test_da_%d.png'%n, img_da)
