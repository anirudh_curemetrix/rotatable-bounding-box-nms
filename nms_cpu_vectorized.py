from math import pi, cos, sin
import numpy as np

def Vector_Add(x1, y1, x2, y2):
    x = x1 + x2 
    y = y1 + y2
    return x,y

def Vector_Sub(x1,y1,x2,y2):
    x = x1 - x2
    y = y1 - y2
    return x,y

def Vector_cross(x1,y1,x2,y2):
    x = x1*y2 - y1*x2
    return x 

def Vec_Line(x1,y1,x2,y2):
    a = y2 - y1
    b = x1 - x2
    c = x2*y1 - y2*x1
    return a,b,c

def Line_point(a,b,c, x1,y1):
    return a*x1 + b*y1 + c

def Line_intersection(a1,b1,c1,a2,b2,c2):
    w = a1*b2 - b1*a2
    x = (b1*c2 - c1*b2)/w
    y = (c1*a2 - a1*c2)/w
    return x,y

def rectangle_vertices(cx, cy, w, h, angle): #Change angle into radians 
    area = w*h
    #PI / 180. == 0.01745329251
    angle = angle*0.01745329251
    dx = w/2
    dy = h/2
    dxcos = dx*cos(angle)
    dxsin = dx*sin(angle)
    dycos = dy*cos(angle)
    dysin = dy*sin(angle)
    
    x1,y1 = Vector_Add(cx, cy, -dxcos - -dysin, -dxsin + -dycos)
    x2,y2 = Vector_Add(cx, cy,dxcos - -dysin,  dxsin + -dycos)
    
    x3,y3 = Vector_Add(cx, cy, dxcos -  dysin,  dxsin +  dycos)
    x4,y4 = Vector_Add(cx, cy, -dxcos -  dysin, -dxsin +  dycos)

    return [(x1,y1), (x2,y2), (x3,y3), (x4,y4)], area

def intersection_area(r1, r2):
    
    rect1, area1 = rectangle_vertices1(*r1)
    rect2, area2 = rectangle_vertices1(*r2)
    
    intersection = rect1
    # Loop over the edges of the second rectangle
    for p, q in zip(rect2, rect2[1:] + rect2[:1]):
        if len(intersection) <= 2:
            break # No intersection

        a,b,c = Vec_Line(p[0],p[1],q[0],q[1])
        
        # Any point p with line(p) <= 0 is on the "inside" (or on the boundary),
        # any point p with line(p) > 0 is on the "outside".

        # Loop over the edges of the intersection polygon,
        # and determine which part is inside and which is outside.
        new_intersection = []
        line_values = [Line_point(a,b,c,t[0],t[1]) for t in intersection]
        for s, t, s_value, t_value in zip(
            intersection, intersection[1:] + intersection[:1],
            line_values, line_values[1:] + line_values[:1]):
            if s_value <= 0:
                new_intersection.append(s)
            if s_value * t_value < 0:
                # Points are on opposite sides.
                # Add the intersection of the lines to new_intersection.
                a2, b2, c2 = Vec_Line(s[0],s[1],t[0],t[1])
                intersection_point = Line_intersection(a, b, c, a2, b2,c2)
                new_intersection.append(intersection_point)

        intersection = new_intersection

    # Calculate area
    if len(intersection) <= 2:
        return 0

    intersectionArea = 0.5 * sum(p[0]*q[1] - p[1]*q[0] for p, q in zip(intersection, intersection[1:] + intersection[:1]))
    iou = intersectionArea/(area1+area2-intersectionArea)
    
    return iou

def bbox_overlaps(boxes, query_boxes):
    """
    Parameters
    ----------
    boxes: (N, 5) ndarray of float
    query_boxes: (K, 5) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    N = boxes.shape[0]
    K = query_boxes.shape[0]
    overlaps = np.zeros((N, K), dtype=DTYPE)
    for k in range(K):
        for n in range(N):
            overlaps[n, k] = intersection_area(query_boxes[k,:],boxes[n,:])	

    return overlaps

def cpu_nms(dets, scores, thresh, nmax):
    
    order = scores.argsort()[::-1]
    ndets = dets.shape[0]
    suppressed = np.zeros((ndets), dtype=np.int)
    keep = []
    
    for _i in range(ndets):
        i = order[_i]
        if suppressed[i] == 1:
            continue
        keep.append(i)

        if len(keep)==nmax:
            return keep
        
        for _j in range(_i + 1, ndets):
            j = order[_j]
            if suppressed[j] == 1:
                continue
            ovr = intersection_area(dets[i,:],dets[j,:])

            if ovr >= thresh:
                suppressed[j] = 1

    return keep


# if __name__ == '__main__':
#     r1 = (10, 15, 15, 10, 30)
#     r2 = (15, 15, 20, 10, 0)
#     print(intersection_area(r1, r2))