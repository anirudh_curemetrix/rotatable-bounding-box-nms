from __future__ import division

import numpy as np
import tensorflow as tf
import tensorflow.keras.regularizers as regularizers
import utils
#import cython_modules.cython_utils as cyutils
import cython_modules.cython_new_utils as cyutils2
 
import cv2

from math import ceil
from backbones import resnet
import time 

#from iou import intersection_area

import pdb


############ Chnages made here #############
#import pstats, cProfile, sys
#import numpy as np

#import pyximport
#pyximport.install()

#import cython_new_utils
#import cython_modules.nms_rotated as nms_r
#import cython_modules.nms_rotated2 as nms_r2
#import nms_cpu_python 
#import nms_cpu_vectorized
# import cython_modules.nms_rotated5 as nms_r5
from cython_modules.rotate_polygon_nms import rotate_gpu_nms
from cython_modules.rbbox_overlaps import rbbx_overlaps
from cython_modules.iou_cpu import get_iou_matrix
#from nms_rotated import nms_rotate_cpu
#tf.debugging.enable_check_numerics()

############ Changes made here ##############

class rpn(tf.keras.Model):
    def __init__(self,config):
        super(rpn,self).__init__()
        
        """Define RPN graph
    
        Inputs:
        feature map: [batch_size, W, H, C]
        Returns:
        cls_logits: [batch_size, num_anchors, 2]
        bbox_deltas [batch_size, num_anchors, (dy, dx, log(dh), log(dw))]
        """

        anchor_per_loc = len(config.RPN_ANCHOR_SCALES)*len(config.RPN_ANCHOR_RATIOS)

        initializer = tf.random_normal_initializer(mean=0.0, stddev=0.01)

        self.base  = tf.keras.Sequential([
            tf.keras.layers.Conv2D(filters=512,kernel_size=[3,3],padding="same",
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                   name="rpn_base1"),

            tf.keras.layers.Conv2D(filters=512,kernel_size=[3,3],padding="same",
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                   name="rpn_base2"),

            tf.keras.layers.Conv2D(filters=512,kernel_size=[3,3],padding="same",
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                   name="rpn_base3")
        ])

        self.cls_logits = tf.keras.layers.Conv2D(filters=2*anchor_per_loc,strides=config.RPN_ANCHOR_STRIDE,
                                                 kernel_size=[1,1],padding="valid",kernel_initializer=initializer,
                                                 kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                                 activation=None,name="rpn_logits",dtype="float32")
        
        self.bbox = tf.keras.layers.Conv2D(filters=5*anchor_per_loc,strides=config.RPN_ANCHOR_STRIDE,
                                           kernel_size=[1,1],padding="valid",
                                           kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                           kernel_initializer=initializer,activation=None,name="rpn_bbox",dtype="float32")
#    @tf.function    
    def __call__(self,inputs):            
        base = self.base(inputs)
        cls_logits=self.cls_logits(base)
        shape = cls_logits.get_shape()
        cls_logits = tf.reshape(cls_logits, [shape[0], -1, 2])

        bbox = self.bbox(base)
        #pdb.set_trace()
        bbox = tf.reshape(bbox, [shape[0], -1, 5])

        return cls_logits, bbox

############################################################
#  RPN anchors generation
############################################################

def generate_anchors(scales, ratios, shape, feature_stride, anchor_stride):
    """
    scales: 1D array of anchor sizes in pixels. Example: [32, 64, 128]
    ratios: 1D array of anchor ratios of width/height. Example: [0.5, 1, 2]
    shape: [height, width] spatial shape of the feature map over which
            to generate anchors.
    feature_stride: Stride of the feature map relative to the image in pixels.
    anchor_stride: Stride of anchors on the feature map. For example, if the
        value is 2 then generate anchors for every other feature map pixel.
    """
    # Get all combinations of scales and ratios
    scales, ratios = np.meshgrid(np.array(scales), np.array(ratios))
    scales = scales.flatten()
    ratios = ratios.flatten()
    
    # Enumerate heights and widths from scales and ratios
    heights = scales / np.sqrt(ratios)
    widths = scales * np.sqrt(ratios)

    # Enumerate shifts in feature space
    shifts_y = np.arange(0, shape[0], anchor_stride) * feature_stride
    shifts_x = np.arange(0, shape[1], anchor_stride) * feature_stride
    shifts_x, shifts_y = np.meshgrid(shifts_x, shifts_y)

    # Enumerate combinations of shifts, widths, and heights
    box_widths, box_centers_x = np.meshgrid(widths, shifts_x)
    box_heights, box_centers_y = np.meshgrid(heights, shifts_y)

    # Reshape to get a list of (y, x) and a list of (h, w)
    box_centers = np.stack(
        [box_centers_y, box_centers_x], axis=2).reshape([-1, 2])
    box_sizes = np.stack([box_heights, box_widths], axis=2).reshape([-1, 2])

    # Convert to corner coordinates (y1, x1, y2, x2)
#    boxes = np.concatenate([box_centers - 0.5 * box_sizes,
#                            box_centers + 0.5 * box_sizes], axis=1)

    # Save new boxes in (y,x,h,w) 
    boxes = np.concatenate([box_centers,box_sizes],axis=1)
    #print("Boxes before y,x,h,w, convention:{}".format(boxes))
    #--------------------------------------------
    # Add rotatable boxes
    rotatedBoxes = []
    nAngles = 1   # Number of discrete angles
    angles = np.arange(0,nAngles)*(np.pi/(2.0*float(nAngles)))

    for box in boxes:
        if box[2]==box[3]:
            # Don't rotate square boxes
            rotatedBoxes.append(np.append(box,0.0))
        else:
            for angle in angles:
                rotatedBoxes.append(np.append(box,angle))
                
    boxes = np.asarray(rotatedBoxes)
    #----------------------------------------------
    # Return boxes (y,x,h,w,angle)
    boxes = boxes.astype(np.float32)

    return boxes

def apply_box_deltas_graph(boxes, deltas):
    """Applies the given deltas to the given boxes.
    boxes: [N, 5] where each row is y1, x1, y2, x2, angle
    deltas: [N, 5] where each row is [dy, dx, log(dh), log(dw), dangle]
    """
    # Boxes stored in (y, x, h, w, angle)
    center_y = boxes[:, 0]
    center_x = boxes[:, 1]
    height = boxes[:, 2]
    width = boxes[:, 3]
    
    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= tf.exp(deltas[:, 2])
    width *= tf.exp(deltas[:, 3])
    
    # New - need to fix 
    angle = boxes[:,4] + deltas[:,4]
    
    result = tf.stack([center_y,center_x,height,width,angle], axis=1, name="apply_box_deltas_out")

    return result

#TODO: Modify this function for rotatable bounding boxes
def clip_boxes_graph(boxes, window):
    """
    boxes: [N, 5] each row is (y, x, h, w, angle)
    window: [5] in the form y1, x1, y2, x2
    """
    # Split corners
    wy1, wx1, wy2, wx2 = tf.split(window, 4)
    y1, x1, y2, x2, angle = tf.split(boxes, 5, axis=1)


    # Return boxes (y,x,h,w,angle)
    # Convert to (y1, x1, y2, x2)
#    R = tf.math.sqrt(boxes[:,2]**2+boxes[:,3]**2)
#    phi = tf.math.atan(boxes[:,2]/boxes[:,3])

#    y1 = boxes[:,0] - 0.5*boxes[:,2]
#    x1 = boxes[:,1] - 0.5*boxes[:,3]

#    y1 = boxes[:,0] - 0.5*boxes[:,2]
#    x1 = boxes[:,1] - 0.5*boxes[:,3]

    # Clip
    y1 = tf.maximum(tf.minimum(y1, wy2), wy1)
    x1 = tf.maximum(tf.minimum(x1, wx2), wx1)
    y2 = tf.maximum(tf.minimum(y2, wy2), wy1)
    x2 = tf.maximum(tf.minimum(x2, wx2), wx1)
    
    clipped = tf.concat([y1, x1, y2, x2, angle], axis=1, name="clipped_boxes")

    return clipped


def nms_rotate_gpu(boxes_list, scores, iou_threshold, proposal_count, use_angle_condition=False, angle_gap_threshold=0, device_id=0):

    if use_angle_condition:
        x_c, y_c, w, h, theta = tf.unstack(boxes_list, axis=1)
        boxes_list = tf.transpose(tf.stack([x_c, y_c, w, h, theta]))
        det_tensor = tf.concat([boxes_list, tf.expand_dims(scores, axis=1)], axis=1)
        keep = tf.numpy_function(rotate_gpu_nms,
                        inp=[det_tensor, iou_threshold, device_id],
                        Tout=tf.int64)

    else:
        x_c, y_c, w, h, theta = tf.unstack(boxes_list, axis=1)
        boxes_list = tf.transpose(tf.stack([x_c, y_c, w, h, theta]))
        det_tensor = tf.concat([boxes_list, tf.expand_dims(scores, axis=1)], axis=1)
        keep = tf.numpy_function(rotate_gpu_nms,
                        inp=[det_tensor, iou_threshold, device_id],
                        Tout=tf.int64)
        keep = tf.reshape(keep, [-1])

    keep = tf.cond(tf.greater(tf.shape(keep)[0], proposal_count),true_fn=lambda: tf.slice(keep, [0], [proposal_count]),
    false_fn=lambda: keep)

    return keep.numpy().tolist()
    
def get_proposals(cls_logits, bbox_deltas, anchors, config, training):
    """Receives anchor scores and selects a subset to pass as proposals
    to the second stage. Filtering is done based on anchor scores and
    non-max suppression to remove overlaps. It also applies bounding
    box refinment details to anchors.

    Inputs:
        cls_logits: [batch, anchors, (bg prob, fg prob)]
        bbox_deltas: [batch, anchors, (dy, dx, log(dh), log(dw), dangle)]

    Returns:
        Proposals in normalized coordinates [batch, rois, (y, x, h, w, angle)]
    """

    scores = cls_logits[:,:,1] # foreground score

    deltas = bbox_deltas * np.reshape(config.RPN_BBOX_STD_DEV, [1, 1, 5])

    if training:
        pre_nms_count = config.RPN_TRAIN_PRE_NMS_COUNT
    else:
        pre_nms_count = config.RPN_TEST_PRE_NMS_COUNT
    pre_nms_limit = min(pre_nms_count, anchors.shape[0])

    ix = tf.nn.top_k(scores, pre_nms_limit,sorted=True,name="top_anchors").indices

    scores = utils.batch_slice([scores, ix], lambda x, y: tf.gather(x, y),
                               config.BATCH_SIZE)
    
    deltas = utils.batch_slice([deltas, ix], lambda x, y: tf.gather(x, y),
                               config.BATCH_SIZE)

    anchors = utils.batch_slice(ix, lambda x: tf.gather(anchors, x),config.BATCH_SIZE,                                
                                    names=["pre_nms_anchors"])

    # Apply deltas to anchors to get refined anchors.
    # [batch, N, (y1, x1, y2, x2, angle)]    
    boxes = utils.batch_slice([anchors, deltas],lambda x, y: apply_box_deltas_graph(x, y),                              
                                   config.BATCH_SIZE,names=["refined_anchors"])
    
    
    # Clip to image boundaries. [batch, N, (y1, x1, y2, x2)]
    # Bill - NEED to FIX
    height, width = config.IMAGE_SHAPE[:2]
    window = np.array([0, 0, height, width],dtype=np.float32)

    #boxes = utils.batch_slice(boxes,lambda x: clip_boxes_graph(x, window),
                              #config.BATCH_SIZE,names=["refined_anchors_clipped"])

    # Normalize dimensions to range of 0 to 1.
    normalized_boxes = boxes / np.array([[height, width, height, width, 1.0]])

    if training:
        proposal_count = config.RPN_TRAIN_PROPOSAL_COUNT
    else:
        proposal_count =config.RPN_TEST_PROPOSAL_COUNT

#    # Non-max suppression - OLD -DOES NOT WORK WITH ROTATED ANGLE
#    def nms(normalized_boxes, scores):
#        boxes = normalized_boxes[:,0:4]
#        indices = tf.image.non_max_suppression(boxes, scores, proposal_count,
#            config.RPN_NMS_THRESHOLD, name="rpn_non_max_suppression")
#        proposals = tf.gather(normalized_boxes, indices)
#        # Pad if needed
#        padding = proposal_count - tf.shape(input=proposals)[0]
#        proposals = tf.concat([proposals, tf.zeros([padding, 5],tf.float32)], 0)
#        return proposals
#    proposals = utils.batch_slice([normalized_boxes, scores], nms, config.BATCH_SIZE)

    # Non-max suppression - NEW
    # Currently NMS only works with GPU and not with CPU
    def nms(normalized_boxes, scores):
        # Profiling the code 
        #pr = cProfile.Profile()
        #pr.enable()
        #start_time = time.time()
        indices = nms_rotate_gpu(normalized_boxes, scores, config.RPN_NMS_THRESHOLD, proposal_count)
        #end_time = time.time() - start_time
        #print("nms took: {} seconds".format(end_time))
        #pr.disable()
        #ps = pstats.Stats(pr, stream=sys.stdout)
        #ps.print_stats()
        #indices = cpu_nms_python.cpu_nms(normalized_boxes.numpy(), scores.numpy(), config.RPN_NMS_THRESHOLD, proposal_count)
        proposals = tf.gather(normalized_boxes, indices)
        # Pad if needed
        padding = proposal_count - tf.shape(input=proposals)[0]
        proposals = tf.concat([proposals, tf.zeros([padding, 5],tf.float32)], 0)
        return proposals

    proposals = utils.batch_slice([normalized_boxes, scores], nms, config.BATCH_SIZE)
    
    return proposals

def iou_rotate_calculate(boxes1, boxes2, use_gpu=False, gpu_id=0):
    '''
    :param boxes_list1:[N, 8] tensor
    :param boxes_list2: [M, 8] tensor
    :return:
    '''
    boxes1 = tf.cast(boxes1, tf.float32)
    boxes2 = tf.cast(boxes2, tf.float32)
    
    if use_gpu:
        iou_matrix = tf.numpy_function(rbbx_overlaps,
                                inp=[boxes1, boxes2, gpu_id],
                                Tout=tf.float32)
    else:
        iou_matrix = tf.numpy_function(get_iou_matrix, inp=[boxes1, boxes2],
                                Tout=tf.float32)

    iou_matrix = tf.reshape(iou_matrix, [tf.shape(boxes1)[0], tf.shape(boxes2)[0]])
    #pdb.set_trace()
    return iou_matrix


def _build_rpn_targets(gt_class_ids, gt_boxes, anchors, config):
    """Given the anchors and GT boxes, compute overlaps and identify positive
    anchors and deltas to refine them to match their corresponding GT boxes.
    Only RPN_TRAIN_SIZE ROIs are labeled. Rest are set to background

    anchors: [num_anchors, (y1, x1, y2, x2)]
    gt_class_ids: [num_gt_boxes] Integer class IDs.
    gt_boxes: [num_gt_boxes, (y1, x1, y2, x2)]

    Returns:
    rpn_match: [N] (int32) matches between anchors and GT boxes.
               1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_bbox: [N, (dy, dx, log(dh), log(dw))] Anchor bbox deltas.
    """
    # remove zero boxes in gt
    non_zero = np.any(gt_boxes, axis=1)
    gt_boxes = gt_boxes[non_zero]
    gt_class_ids = gt_class_ids[non_zero]
    
    
    # JCQ bypass if there are no GTs
    if len(gt_class_ids) == 0:  
        rpn_match = np.zeros ( (anchors.shape[0]  ), dtype=np.int32)
        rpn_bbox  = np.zeros( (anchors.shape[0], 5), dtype=np.float32)
        # randomly select config.RPN_TRAIN_ANCHORS_PER_IMAGE anchors to be negative and the rest neutral
        rpn_match[:config.RPN_TRAIN_ANCHORS_PER_IMAGE] = -1.0
        rpn_match = np.random.permutation(rpn_match)
        return rpn_match, rpn_bbox

    # RPN Match: 1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_match = np.zeros([anchors.shape[0]], dtype=np.int32)
    # RPN bounding boxes: [max anchors per image, (dy, dx, log(dh), log(dw), dangle)]
    rpn_bbox = np.zeros((config.RPN_TRAIN_ANCHORS_PER_IMAGE, 5))

    # # Compute overlaps [num_anchors, num_gt_boxes]
#    overlaps = cyutils.bbox_overlaps(anchors, gt_boxes.numpy() )
    
    # Anirudh - changed to puthon version
    overlaps = cyutils2.bbox_overlaps(anchors, gt_boxes ) ## Original Line 
    #print("overlaps_1 {}".format(overlaps_1))
    #overlaps = nms_r5.bbox_overlaps(anchors, gt_boxes.numpy() )
    
    #Converting anchors and gt_boxes in to normal co-ordinates 
    #start_time = time.time()
    #overlaps = iou_rotate_calculate(anchors, gt_boxes)
    #overlaps = overlaps.numpy()
    #end_time = time.time() - start_time
    #print("box_overlap time : {}".format(end_time))
    print("overlaps {}".format(overlaps))
    #pdb.set_trace()
    # Match anchors to GT Boxes
    # If an anchor overlaps a GT box with IoU >= 0.7 then it's positive.
    # If an anchor overlaps a GT box with IoU < 0.3 then it's negative.
    # However, don't keep any GT box unmatched (rare, but happens). Instead,
    # match it to the closest anchor (even if its max IoU is < 0.3).
    # 1. Set negative anchors first. It gets overwritten if a gt box is matched to them.
    anchor_iou_argmax = np.argmax(overlaps, axis=1)
    anchor_iou_max = overlaps[np.arange(overlaps.shape[0]), anchor_iou_argmax]
    rpn_match[anchor_iou_max <= config.RPN_NEG_THRESHOLD] = -1.0
    # 2. Set an anchor for each GT box (regardless of IoU value).
    # TODO: If multiple anchors have the same IoU match all of them
    gt_iou_argmax = np.argmax(overlaps, axis=0)
    rpn_match[gt_iou_argmax] = 1.0

    # 3. Set anchors with high overlap as positive.
    rpn_match[anchor_iou_max >= config.RPN_POS_THRESHOLD] = 1.0

    margin = 0
    # set cross-boundary anchors to background
    # BILL NEED TO FIX
    inds_out = np.where(
            ~((anchors[:,0] > margin) &
              (anchors[:,1] > margin) &
              (anchors[:,2] < config.IMG_HEIGHT - margin) &
              (anchors[:,3] < config.IMG_WIDTH - margin))
             )[0]
    rpn_match[inds_out] = 0.0

    # Subsample to balance positive and negative anchors
    # Don't let positives be more than half the anchors
    ids = np.where(rpn_match == 1)[0]
    extra = len(ids) - (config.RPN_TRAIN_ANCHORS_PER_IMAGE // 2)
    if extra > 0:
        # Reset the extra ones to neutral
        ids = np.random.choice(ids, extra, replace=False)
        rpn_match[ids] = 0.0
    # Same for negative proposals
    ids = np.where(rpn_match == -1)[0]
    extra = len(ids) - (config.RPN_TRAIN_ANCHORS_PER_IMAGE - np.sum(rpn_match == 1))
    if extra > 0:
        # Rest the extra ones to neutral
        ids = np.random.choice(ids, extra, replace=False)
        rpn_match[ids] = 0.0

    # For positive anchors, compute shift and scale needed to transform them
    # to match the corresponding GT boxes.
    ids = np.where(rpn_match == 1)[0]
    #pdb.set_trace()
    ix = 0  # index into rpn_bbox
    # TODO: use box_refinment() rather than duplicating the code here
    for i, a in zip(ids, anchors[ids]):
        # Closest gt box (it might have IoU < 0.7)
        gt = gt_boxes[anchor_iou_argmax[i]]

        # GT Box uysinng center plus width/height
        gt_h = gt[2]
        gt_w = gt[3]
        gt_center_y = gt[0]
        gt_center_x = gt[1]
        gt_angle = gt[4]
        
        # Anchor
        a_h = a[2]
        a_w = a[3]
        a_center_y = a[0]
        a_center_x = a[1]
        a_angle = a[4]
        
        # Inorder to avoid NaNs in division and log
        gt_w += 1e-8
        gt_h += 1e-8
        a_w += 1e-8
        a_h += 1e-8
        # Compute the bbox refinement that the RPN should predict.
        rpn_bbox[ix] = [
            (gt_center_y - a_center_y) / a_h,
            (gt_center_x - a_center_x) / a_w,
            np.log(gt_h / a_h),
            np.log(gt_w / a_w),
            np.tan(gt_angle - a_angle)   # NEW - placeholder
            #gt_angle - a_angle
        ]
        # Normalize
        rpn_bbox[ix] /= config.RPN_BBOX_STD_DEV
        ix += 1
    return rpn_match.astype(np.float32), rpn_bbox.astype(np.float32)

def rpn_targets_graph(anchors, gt_cls, gt_bboxes, config):
    """Wrapper for _build_rpn_targets to convert it to tf op

    Inputs:
        - anchors: [num_anchors, 5]
        - gt_cls: [batch_size, num_boxes]
        - gt_bboxes: [batch_size, num_boxes, 5]

    Outputs:
        - rpn_cls_gt: [batch_size, num_anchors]
        - rpn_bbox_gt: [batch_size, num_anchors]
    """
    
    def rpn_targets(gt_cls, gt_bboxes):

        return tf.numpy_function(lambda x,y : _build_rpn_targets(x,y,                                                              
                                            anchors=anchors,
                                            config=config),
                                            [gt_cls, gt_bboxes],
                                            [tf.float32, tf.float32],
                                            name='rpn_targets_python')
    
    rpn_cls_gt, rpn_bbox_gt = utils.batch_slice([gt_cls, gt_bboxes],
                                    rpn_targets,
                                    batch_size=config.BATCH_SIZE,)
    
    return rpn_cls_gt, rpn_bbox_gt

def log2(x):
    return tf.math.log(x)/tf.math.log(2.0)

def forward_convert(coordinate):
    """
    :param coordinate: format [y_c, x_c, h, w, theta] theta in radians 
    :return: format [x1, y1, x2, y2, x3, y3, x4, y4]
    """
    boxes = []
    for rect in coordinate:
        box = cv2.boxPoints(((rect[1], rect[0]), (rect[3], rect[2]), 57.29577951308232*rect[4]))
        box = np.reshape(box, [-1, ])
        boxes.append([box[0], box[1], box[2], box[3], box[4], box[5], box[6], box[7]])
    
    return np.array(boxes, dtype=np.float32)

def get_horizen_minAreaRectangle(boxs):

    rpn_proposals_boxes_convert = tf.numpy_function(forward_convert,
                                            inp=[boxs],
                                            Tout=tf.float32)
    
    rpn_proposals_boxes_convert = tf.reshape(rpn_proposals_boxes_convert, [-1, 8])

    boxes_shape = tf.shape(rpn_proposals_boxes_convert)
    x_list = tf.strided_slice(rpn_proposals_boxes_convert, begin=[0, 0], end=[boxes_shape[0], boxes_shape[1]],
                                strides=[1, 2])
    y_list = tf.strided_slice(rpn_proposals_boxes_convert, begin=[0, 1], end=[boxes_shape[0], boxes_shape[1]],
                                strides=[1, 2])

    y_max = tf.reduce_max(y_list, axis=1)
    y_min = tf.reduce_min(y_list, axis=1)
    x_max = tf.reduce_max(x_list, axis=1)
    x_min = tf.reduce_min(x_list, axis=1)

    return tf.transpose(tf.stack([x_min, y_min, x_max, y_max], axis=0))

def roi_pooling(feature_maps, rois, img_shape):
    
    img_h, img_w = tf.cast(img_shape[0], tf.float32), tf.cast(img_shape[1], tf.float32)
    N = tf.shape(rois)[0]
    x1, y1, x2, y2 = tf.unstack(rois, axis=1)

    normalized_x1 = x1 / img_w
    normalized_x2 = x2 / img_w
    normalized_y1 = y1 / img_h
    normalized_y2 = y2 / img_h

    normalized_rois = tf.transpose( tf.stack([normalized_y1, normalized_x1, normalized_y2, normalized_x2]), name='get_normalized_rois')

    normalized_rois = tf.stop_gradient(normalized_rois)
    cropped_roi_features = tf.image.crop_and_resize(feature_maps, normalized_rois,
                                                        box_indices=tf.zeros(shape=[N, ],
                                                                            dtype=tf.int32),
                                                        crop_size=[14,14], #Hardcoded for now 
                                                        name='CROP_AND_RESIZE')

    maxpool = tf.keras.layers.MaxPool2D([2, 2], padding='same')
    pooled = maxpool(cropped_roi_features)



    return pooled

class ROIAlign(tf.keras.Model):
    def __init__(self,config):
        super(ROIAlign,self).__init__()
        """Implements ROI Pooling
        
        Inputs:
        - boxes: [batch, num_boxes, (y1, x1, y2, x2)] in normalized
        coordinates. Possibly padded with zeros if not enough
        boxes to fill the array.
        - Feature map: feature map from backbone
                    [batch, height, width, channels]

        Output:
        Pooled regions in the shape: [batch, num_boxes, height, width, channels].
        The width and height are those specific in the pool_shape in the config
        """
        self.config = config
        
        #if self.config.USE_ROI_MAXPOOL:
        #   self.maxpool = tf.keras.layers.MaxPool2D([2, 2], padding='same')
        self.maxpool = tf.keras.layers.MaxPool2D([2, 2], padding='same')
    
    # def RROI_Align_Perspective(feature_map, boxes):
    #     return pooled 
#    @tf.function        
    def __call__(self, boxes, feature_map):
            
        # Crop boxes [batch, num_boxes, (y1, x1, y2, x2, theta)] in normalized coords

        # Feature Maps. List of feature maps from different level of the
        # feature pyramid. Each is [batch, height, width, channels]

        # Crop and Resize
        # Result: [batch * num_boxes, pool_height, pool_width, channels]
        box_indices = tf.where(tf.ones_like(boxes[:,:,0], dtype=tf.bool))
        box_indices = tf.cast(box_indices[:,0], tf.int32)

        boxes_shape = tf.shape(input=boxes)
        #boxes = boxes * np.array([[self.config.IMG_HEIGHT, self.config.IMG_WIDTH, self.config.IMG_HEIGHT, self.config.IMG_WIDTH, 1]])
        boxes = tf.reshape(boxes, (-1, 5))
        
        boxes = tf.stop_gradient(boxes)
        box_indices = tf.stop_gradient(box_indices)

        fheight = feature_map.shape[1]
        fwidth = feature_map.shape[2]
        #pdb.set_trace()
        if self.config.USE_ROI_MAXPOOL:

            feat_shape = (self.config.POOL_SIZE, self.config.POOL_SIZE)
            img_shape  = (self.config.IMG_HEIGHT, self.config.IMG_WIDTH,3)
            rois = get_horizen_minAreaRectangle(boxes)
            pooled_features = roi_pooling(feature_maps=feature_map, rois=rois, img_shape=img_shape)
            ch = pooled_features.get_shape()[-1]
            pooled_features = tf.reshape(pooled_features, (boxes_shape[0], boxes_shape[1], feat_shape[0], feat_shape[1], ch))

            return pooled_features
        else:
            feat_shape = (2*self.config.POOL_SIZE, 2*self.config.POOL_SIZE)
            # NEW - need to fix - ROI pooling with angle

            # Return boxes (y,x,h,w,angle)
            depth = feature_map.shape[-1]
            pooled = np.zeros([len(box_indices),feat_shape[0],feat_shape[1],depth])
                
            for i,box in enumerate(boxes):
                height = np.int0(fheight*box[2])
                width = np.int0(fwidth*box[3])
                rect = cv2.boxPoints(((fwidth*box[1],fheight*box[0]),(width,height),180.0*box[4]/np.pi))
                rect = np.int0(rect)
                src_pts = rect.astype("float32")
                #print("Height {0}, Width {1}, fwidth {2}, fheight {3}, box[2] {4}".format(height, width, fwidth, fheight, box[2]))
                # dst_pts = np.array([[0, height-1],[0, 0],[width-1, 0],
                #             [width-1, height-1]], dtype="float32")
                dst_pts = np.array([[0,0], 
                                    [width, 0], 
                                    [0, height],
                                    [width, height]], dtype="float32")
                #print("src_pts {}".format(src_pts))
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                chunk = 512
                for j in range(0,depth,chunk):
                    pooled[i,:,:,j:j+chunk-1]=cv2.warpPerspective(feature_map.numpy()[0,:,:,j:j+chunk-1], M, feat_shape)
                #for j in range(depth):
                    #pooled[i,:,:,j] = cv2.warpPerspective(feature_map.numpy()[0,:,:,j], M, feat_shape)

            
            pooled =tf.convert_to_tensor(pooled)
            #pooled = self.maxpool(pooled)
#            pooled = tf.image.crop_and_resize(feature_map, boxes[:,0:4], box_indices, feat_shape,method="bilinear")
            
        ch = pooled.get_shape()[-1]
        feat_shape = (2*self.config.POOL_SIZE, 2*self.config.POOL_SIZE)
        #pdb.set_trace()
        # JCQ changed this to use the original shape of box 
        # it shouldn't have worked before but apparently TF
        # optimized out this operation. 
        pooled = tf.reshape(pooled, (boxes_shape[0], boxes_shape[1], feat_shape[0], feat_shape[1], ch))
        
        # returned shape = [batch, num_rois, w, h, c]
        #pdb.set_trace()
        return pooled


class rcnn_head(tf.keras.Model):
    def __init__(self,config):
        super(rcnn_head,self).__init__()
             
        """Implements second stage classifier head
        Inputs:

           - pooled_rois: (batch_size, num_rois, w, h, c)
        
        Returns:

            - cls_logits: (batch_size*num_rois, num_classes)
            - bbox: (batch_size*num_rois, 5*num_classes)
        """
        self.config = config
        
        initializer = tf.random_normal_initializer(mean=0.0, stddev=0.01)
        initializer_bbox  = tf.random_normal_initializer(mean=0.0, stddev=0.001)

        self.block4 = resnet.block4(depth=101)
        
        self.cls_logits = tf.keras.layers.Dense(units=self.config.NUM_CLASSES,
                                                kernel_initializer=initializer,
                                                kernel_regularizer=regularizers.l2(self.config.WEIGHT_DECAY),
                                                activation=None,name="rcnn_logits",dtype="float32")

        self.bbox = tf.keras.layers.Dense(units=5*self.config.NUM_CLASSES,
                                          kernel_initializer=initializer_bbox,
                                          kernel_regularizer=regularizers.l2(self.config.WEIGHT_DECAY),
                                          activation=None,name="rcnn_bbox",dtype="float32")        

#    @tf.function    
    def __call__(self, pooled_rois, training=True):
        
        shape = pooled_rois.get_shape().as_list()
        pooled_rois = tf.reshape(pooled_rois, [-1] +  shape[2:])
        net = self.block4(pooled_rois, training=training)
       
        # average pooling done by reduce_mean
        net = tf.reduce_mean(input_tensor=net, axis=[1, 2])
    
        cls_logits = self.cls_logits(net)

        bbox = self.bbox(net)

        cls_logits = tf.reshape(cls_logits, [-1, self.config.NUM_CLASSES])
        bbox = tf.reshape(bbox, [-1, self.config.NUM_CLASSES, 5])

        return cls_logits, bbox

def _box_refinement_graph_rotated(box, gt_box):
    """
    Compute the refinement needed to transform box to gt_box 
    box and gt_box are [N,(y,x,h,w,angle)]
    """
    height = box[:, 2]
    width = box[:, 3] 
    center_y = box[:, 0] 
    center_x = box[:, 1]

    gt_height = gt_box[:, 2] 
    gt_width = gt_box[:, 3] 
    gt_center_y = gt_box[:, 0] 
    gt_center_x = gt_box[:, 1]

    gt_width += 1e-8
    gt_height += 1e-8
    height+= 1e-8
    width+= 1e-8

    dy = (gt_center_y - center_y) / height
    dx = (gt_center_x - center_x) / width
    dh = tf.math.log(gt_height / height)
    dw = tf.math.log(gt_width / width)

    # NEW - need to fix
    dAngle = tf.math.tan(box[:,4] - gt_box[:,4])

    result = tf.stack([dy, dx, dh, dw, dAngle], axis=1)
    return result 

def _box_refinement_graph(box, gt_box):
    """Compute refinement needed to transform box to gt_box.
    box and gt_box are [N, (y1, x1, y2, x2)]
    """
#    box = tf.cast(box, TF_DTYPE )
#    gt_box = tf.cast(gt_box, TF_DTYPE)

    height = box[:, 2] - box[:, 0]
    width = box[:, 3] - box[:, 1]
    center_y = box[:, 0] + 0.5 * height
    center_x = box[:, 1] + 0.5 * width

    gt_height = gt_box[:, 2] - gt_box[:, 0]
    gt_width = gt_box[:, 3] - gt_box[:, 1]
    gt_center_y = gt_box[:, 0] + 0.5 * gt_height
    gt_center_x = gt_box[:, 1] + 0.5 * gt_width

    dy = (gt_center_y - center_y) / height
    dx = (gt_center_x - center_x) / width
    dh = tf.math.log(gt_height / height)
    dw = tf.math.log(gt_width / width)

    # NEW - need to fix
    #dAngle = tf.math.tan(box[:,4] - gt_box[:,4])
    dAngle = box[:,4] - gt_box[:,4]
    
    result = tf.stack([dy, dx, dh, dw, dAngle], axis=1)
    return result


def _overlaps_graph(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)].
    """
    # 1. Tile boxes2 and repeate boxes1. This allows us to compare
    # every boxes1 against every boxes2 without loops.
    # TF doesn't have an equivalent to np.repeate() so simulate it
    # using tf.tile() and tf.reshape.
    b1 = tf.reshape(tf.tile(tf.expand_dims(boxes1, 1),
                            [1, 1, tf.shape(input=boxes2)[0]]), [-1, 5])
    b2 = tf.tile(boxes2, [tf.shape(input=boxes1)[0], 1])
    # 2. Compute intersections
    b1_y1, b1_x1, b1_y2, b1_x2, b1_angle = tf.split(b1, 5, axis=1)
    b2_y1, b2_x1, b2_y2, b2_x2, b2_angle = tf.split(b2, 5, axis=1)
    y1 = tf.maximum(b1_y1, b2_y1)
    x1 = tf.maximum(b1_x1, b2_x1)
    y2 = tf.minimum(b1_y2, b2_y2)
    x2 = tf.minimum(b1_x2, b2_x2)
    intersection = tf.maximum(x2 - x1, 0) * tf.maximum(y2 - y1, 0)
    # 3. Compute unions
    b1_area = (b1_y2 - b1_y1) * (b1_x2 - b1_x1)
    b2_area = (b2_y2 - b2_y1) * (b2_x2 - b2_x1)
    union = b1_area + b2_area - intersection
    # 4. Compute IoU and reshape to [boxes1, boxes2]
    iou = intersection / union
    overlaps = tf.reshape(iou, [tf.shape(input=boxes1)[0], tf.shape(input=boxes2)[0]])
    return overlaps


def _rcnn_targets(proposals, gt_class_ids, gt_boxes, config):
    """Generates detection targets for one image. Subsamples proposals and
    generates target class IDs, bounding box deltas, and masks for each.

    Inputs:
    proposals: [N, (y, x, h, w, angle)] in normalized coordinates. Might
               be zero padded if there are not enough proposals.
    gt_class_ids: [MAX_GT_INSTANCES] int class IDs
    gt_boxes: [MAX_GT_INSTANCES, (y, x, h, w, angle)] in normalized coordinates.

    Returns: Target ROIs and corresponding class IDs, bounding box shifts,
    rois: [NUM_RCNN_TRAIN_ROIS, (y, x, h, w, angle)] in normalized coordinates
    class_ids: [NUM_RCNN_TRAIN_ROIS]. Integer class IDs. Zero padded.
    deltas: [NUM_RCNN_TRAIN_ROIS, NUM_CLASSES, (dy, dx, log(dh), log(dw), dangle)]
            Class-specific bbox refinments.

    Note: Returned arrays might be zero padded if not enough target ROIs.
    """

    # Compute overlaps matrix [rpn_rois, gt_boxes]
    #Anirudh 
    #overlaps = _overlaps_graph(proposals, gt_boxes)
    #overlaps_1 = iou_rotate_calculate(proposals, gt_boxes, use_gpu=False, gpu_id=0)
    overlaps = cyutils2.bbox_overlaps(proposals.numpy(), gt_boxes.numpy() )
    #overlaps = overlaps.numpy()
    #pdb.set_trace()
    # Determine postive and negative ROIs
    roi_iou_max = tf.reduce_max(input_tensor=overlaps, axis=1)
    # 1. Positive ROIs are those with >= 0.5 IoU with a GT box
    positive_roi_bool = (roi_iou_max >= config.RCNN_POS_THRESHOLD)
    positive_indices = tf.where(positive_roi_bool)[:, 0]
    # 2. Negative ROIs are those with < 0.5 with every GT box
    negative_indices = tf.where(roi_iou_max < config.RCNN_NEG_THRESHOLD)[:, 0]

    num_pos_req = int(config.NUM_RCNN_TRAIN_ROIS*config.RCNN_TRAIN_FG_RATIO)
    positive_indices = tf.random.shuffle(positive_indices)[:num_pos_req]
    pos_count = tf.shape(input=positive_indices)[0]

    num_neg_req = tf.cast(config.NUM_RCNN_TRAIN_ROIS - pos_count, tf.int32)
    negative_indices = tf.random.shuffle(negative_indices)[:num_neg_req]

    positive_rois = tf.gather(proposals, positive_indices)
    negative_rois = tf.gather(proposals, negative_indices)

    # JCQ: in the unlikley event that there are not enough negatives
    # pad the batch with placeholder negative rois. 
    # It can happen that there are not enough negatives because roi_iou_max
    # can have nans when both the ROI and GT and placeholders (boxes of size zero)

    N_extra_negatives_req = num_neg_req - tf.shape(input=negative_indices)[0]
    #negative_rois = tf.Print(negative_rois, [tf.shape(positive_rois)], message='positive rois ')
    #negative_rois = tf.Print(negative_rois, [tf.shape(negative_rois)], message='negative rois before ')
    negative_rois = tf.pad(tensor=negative_rois, paddings=[(0, N_extra_negatives_req), (0, 0)])
    #negative_rois = tf.Print(negative_rois, [tf.shape(negative_rois)], message='negative rois after ')

    # Assign positive ROIs to GT boxes.
    positive_overlaps = tf.gather(overlaps, positive_indices)
    roi_gt_box_assignment = tf.argmax(input=positive_overlaps, axis=1)
    roi_gt_boxes = tf.gather(gt_boxes, roi_gt_box_assignment)
    roi_gt_class_ids = tf.gather(gt_class_ids, roi_gt_box_assignment)

    # Compute bbox refinement for positive ROIs

    #deltas = _box_refinement_graph(positive_rois, roi_gt_boxes)
    deltas = _box_refinement_graph_rotated(positive_rois, roi_gt_boxes)
    deltas /= config.BBOX_STD_DEV

    # Append negative ROIs and pad bbox deltas that
    # are not used for negative ROIs with zeros.
    rois = tf.concat([positive_rois, negative_rois], axis=0)
    N = tf.shape(input=negative_rois)[0]
    roi_gt_boxes = tf.pad(tensor=roi_gt_boxes, paddings=[(0, N), (0, 0)])
    roi_gt_class_ids = tf.pad(tensor=roi_gt_class_ids, paddings=[(0, N)])
    deltas = tf.pad(tensor=deltas, paddings=[(0, N), (0, 0)])

    return rois, roi_gt_class_ids, deltas #, masks

def rcnn_targets_graph(rois, gt_cls, gt_bboxes, config):
    """Wrapper around _rcnn_targets to do batch processing"""

    rois, roi_gt_cls, roi_gt_bbox = utils.batch_slice(
                                [rois, gt_cls, gt_bboxes],
                                lambda x,y,z: _rcnn_targets(x, y, z, config),
                                batch_size=config.BATCH_SIZE)

    return rois, roi_gt_cls, roi_gt_bbox


def clip_to_window(window, boxes):
    """
    window: (y1, x1, y2, x2). The window in the image we want to clip to.
    boxes: [N, (y1, x1, y2, x2)]
    """
    boxes[:, 0] = np.maximum(np.minimum(boxes[:, 0], window[2]), window[0])
    boxes[:, 1] = np.maximum(np.minimum(boxes[:, 1], window[3]), window[1])
    boxes[:, 2] = np.maximum(np.minimum(boxes[:, 2], window[2]), window[0])
    boxes[:, 3] = np.maximum(np.minimum(boxes[:, 3], window[3]), window[1])
    return boxes

def apply_box_deltas(boxes, deltas):
    """Applies the given deltas to the given boxes.
    boxes: [N, (y1, x1, y2, x2,angle)]. Note that (y2, x2) is outside the box.
    deltas: [N, (dy, dx, log(dh), log(dw),dAngle)]

    """
    # Convert to y, x, h, w
    center_y = boxes[:, 0]
    center_x = boxes[:, 1]
    height = boxes[:, 2]
    width = boxes[:, 3]
    
    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= np.exp(deltas[:, 2])
    width *= np.exp(deltas[:, 3])

    # Bill Need to fix 
    angle = boxes[:,4] + deltas[:,4]    
    
    return np.stack([center_y,center_x,height,width,angle], axis=1)


def get_rcnn_detections(rois, pred_probs, deltas, config):
    """Apply bbox refinement s at stage 2
    Inputs:
        - rois: [num_rois, (y1, x1, y2, x2)]
        - pred_probs: [num_rois, num_classes]
        - deltas: [num_rois, num_classes, (dy, dx, log(dh), log(dw))]
        - config: configuration object
    Outputs:
        - rois: [n_detections, (y1, x1, y2, x2)]
        - probs: [n_detections, 1]
    """
    # we are interested only in the last class
    class_ids = (config.NUM_CLASSES - 1)*np.ones(pred_probs.shape[0]).astype(np.int)
    # Class probability of the last class of each ROI

#    class_scores = pred_probs[np.arange(class_ids.shape[0]), class_ids]
    class_scores = pred_probs[:,1].numpy()
    
    # Class-specific bounding box deltas
#    deltas_specific = deltas[np.arange(deltas.shape[0]), class_ids]
    deltas_specific = deltas[:,1,:]
    
    # Apply bounding box deltas
    # Shape: [boxes, (y1, x1, y2, x2)] in normalized coordinates

    refined_rois = apply_box_deltas(rois, deltas_specific * config.BBOX_STD_DEV)

    # Convert coordiates to image domain
    height, width = config.IMAGE_SHAPE[:2]
    window = np.array([0, 0, height, width])
    refined_rois *= np.array([height, width, height, width, 1.0])
    
    # Clip boxes to image window
    # Bill - should we still clip?
#    refined_rois = clip_to_window(window, refined_rois)    

#    keep = cyutils.cpu_nms(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD)
#    keep = np.array(keep)

    # Bill - New - need to fix?
    #Anirudh 
    #keep = cyutils2.cpu_nms(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD,config.RPN_TEST_PROPOSAL_COUNT) 
    #keep = nms_r5.cpu_nms(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD,config.RPN_TEST_PROPOSAL_COUNT)
    keep = nms_rotate_gpu(refined_rois.numpy(), class_scores.numpy(), config.DETECTION_NMS_THRESHOLD, config.RPN_TEST_PROPOSAL_COUNT)
    #keep = cv2.dnn.NMSBoxes(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD,config.RPN_TEST_PROPOSAL_COUNT)
    keep = np.array(keep)
        
    # Round and cast to int since we're deadling with pixels now
    # Bill - do we need this
#    refined_rois = np.rint(refined_rois).astype(np.int32)

    # Keep top detections
    roi_count = config.RCNN_MAX_DETECTIONS
    top_ids = np.argsort(class_scores[keep])[::-1][:roi_count]
    keep = keep[top_ids]

    # Arrange output as [N, (y1, x1, y2, x2, class_id, score)]
    # Coordinates are in image domain.
    # result = np.hstack((refined_rois[keep],
                        # class_ids[keep][..., np.newaxis],
                        # class_scores[keep][..., np.newaxis]))

    refined_rois = refined_rois[keep]
    scores = class_scores[keep]

    return refined_rois, scores, keep.astype(np.int32)


#def get_feature_maps(features, idx, config):
#
#    feature_maps = utils.batch_slice([features, idx], lambda x,y: tf.gather(x, y), 
#        batch_size=config.BATCH_SIZE, names=['final_feature_maps'] )
#    return feature_maps



# Convert coordinates from (y,x,h,w,angle)

#def boxCoordinates(box):
#
        # Convert to corner coordinates (y1, x1, y2, x2)
#    boxes = np.concatenate([box_centers - 0.5 * box_sizes,
#                            box_centers + 0.5 * box_sizes], axis=1)

#    # Save new boxes in 
#    boxes = np.concatenate([box_centers,box_sizes],axis=1)
