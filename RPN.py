from __future__ import division

import numpy as np
import tensorflow as tf
import tensorflow.keras.regularizers as regularizers
import utils
#import cython_modules.cython_utils as cyutils
import cython_modules.cython_new_utils as cyutils2
 
import cv2

from math import ceil
from backbones import resnet
import time 

#from iou import intersection_area

import pdb


############ Chnages made here #############
#import pstats, cProfile, sys
#import numpy as np

#import pyximport
#pyximport.install()

#import cython_new_utils
#import cython_modules.nms_rotated as nms_r
#import cython_modules.nms_rotated2 as nms_r2
#import nms_cpu_python 
#import nms_cpu_vectorized
# import cython_modules.nms_rotated5 as nms_r5
from cython_modules.rotate_polygon_nms import rotate_gpu_nms
from cython_modules.rbbox_overlaps import rbbx_overlaps
from cython_modules.iou_cpu import get_iou_matrix
#from nms_rotated import nms_rotate_cpu
#tf.debugging.enable_check_numerics()

############ Changes made here ##############

class rpn(tf.keras.Model):
    def __init__(self,config):
        super(rpn,self).__init__()
        
        """Define RPN graph
        Inputs:
        feature map: [batch_size, W, H, C]
        Returns:
        cls_logits: [batch_size, num_anchors, 2]
        bbox_deltas [batch_size, num_anchors, (dy, dx, log(dh), log(dw), dangel)]
        """

        anchor_per_loc = len(config.RPN_ANCHOR_SCALES)*len(config.RPN_ANCHOR_RATIOS)

        initializer = tf.random_normal_initializer(mean=0.0, stddev=0.01)

        self.base  = tf.keras.Sequential([
            tf.keras.layers.Conv2D(filters=512,kernel_size=[3,3],padding="same",
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                   name="rpn_base1"),

            tf.keras.layers.Conv2D(filters=512,kernel_size=[3,3],padding="same",
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                   name="rpn_base2"),

            tf.keras.layers.Conv2D(filters=512,kernel_size=[3,3],padding="same",
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                   name="rpn_base3")
        ])

        self.cls_logits = tf.keras.layers.Conv2D(filters=2*anchor_per_loc,strides=config.RPN_ANCHOR_STRIDE,
                                                 kernel_size=[1,1],padding="valid",kernel_initializer=initializer,
                                                 kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                                 activation=None,name="rpn_logits",dtype="float32")
        
        self.bbox = tf.keras.layers.Conv2D(filters=5*anchor_per_loc,strides=config.RPN_ANCHOR_STRIDE,
                                           kernel_size=[1,1],padding="valid",
                                           kernel_regularizer=regularizers.l2(config.WEIGHT_DECAY),
                                           kernel_initializer=initializer,activation=None,name="rpn_bbox",dtype="float32")
#    @tf.function    
    def __call__(self,inputs):            
        base = self.base(inputs)
        cls_logits=self.cls_logits(base)
        shape = cls_logits.get_shape()
        cls_logits = tf.reshape(cls_logits, [shape[0], -1, 2])

        bbox = self.bbox(base)
        #pdb.set_trace()
        bbox = tf.reshape(bbox, [shape[0], -1, 5])

        return cls_logits, bbox

############################################################
#  RPN anchors generation
############################################################

def generate_anchors(scales, ratios, shape, feature_stride, anchor_stride):
    """
    scales: 1D array of anchor sizes in pixels. Example: [32, 64, 128]
    ratios: 1D array of anchor ratios of width/height. Example: [0.5, 1, 2]
    shape: [height, width] spatial shape of the feature map over which
            to generate anchors.
    feature_stride: Stride of the feature map relative to the image in pixels.
    anchor_stride: Stride of anchors on the feature map. For example, if the
        value is 2 then generate anchors for every other feature map pixel.
    """
    # Get all combinations of scales and ratios
    scales, ratios = np.meshgrid(np.array(scales), np.array(ratios))
    scales = scales.flatten()
    ratios = ratios.flatten()
    
    # Enumerate heights and widths from scales and ratios
    heights = scales / np.sqrt(ratios)
    widths = scales * np.sqrt(ratios)

    # Enumerate shifts in feature space
    shifts_y = np.arange(0, shape[0], anchor_stride) * feature_stride
    shifts_x = np.arange(0, shape[1], anchor_stride) * feature_stride
    shifts_x, shifts_y = np.meshgrid(shifts_x, shifts_y)

    # Enumerate combinations of shifts, widths, and heights
    box_widths, box_centers_x = np.meshgrid(widths, shifts_x)
    box_heights, box_centers_y = np.meshgrid(heights, shifts_y)

    # Reshape to get a list of (y, x) and a list of (h, w)
    box_centers = np.stack(
        [box_centers_y, box_centers_x], axis=2).reshape([-1, 2])
    box_sizes = np.stack([box_heights, box_widths], axis=2).reshape([-1, 2])

    # Convert to corner coordinates (y1, x1, y2, x2)
#    boxes = np.concatenate([box_centers - 0.5 * box_sizes,
#                            box_centers + 0.5 * box_sizes], axis=1)

    # Save new boxes in (y,x,h,w) 
    boxes = np.concatenate([box_centers,box_sizes],axis=1)
    #print("Boxes before y,x,h,w, convention:{}".format(boxes))
    #--------------------------------------------
    # Add rotatable boxes
    rotatedBoxes = []
    nAngles = 1   # Number of discrete angles
    angles = np.arange(0,nAngles)*(np.pi/(2.0*float(nAngles)))

    for box in boxes:
        if box[2]==box[3]:
            # Don't rotate square boxes
            rotatedBoxes.append(np.append(box,0.0))
        else:
            for angle in angles:
                rotatedBoxes.append(np.append(box,angle))
                
    boxes = np.asarray(rotatedBoxes)
    #----------------------------------------------
    # Return boxes (y,x,h,w,angle)
    boxes = boxes.astype(np.float32)

    return boxes

def apply_box_deltas_graph(boxes, deltas):
    """Applies the given deltas to the given boxes.
    boxes: [N, 5] where each row is y1, x1, y2, x2, angle
    deltas: [N, 5] where each row is [dy, dx, log(dh), log(dw), dangle]
    """
    # Boxes stored in (y, x, h, w, angle)
    center_y = boxes[:, 0]
    center_x = boxes[:, 1]
    height = boxes[:, 2]
    width = boxes[:, 3]
    
    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= tf.exp(deltas[:, 2])
    width *= tf.exp(deltas[:, 3])
    
    # New - need to fix 
    angle = 180.0*boxes[:,4]/np.pi + deltas[:,4]
    result = tf.stack([center_y,center_x,height,width,angle], axis=1, name="apply_box_deltas_out")

    return result


# def clip_boxes_graph_rotated(boxes, window):

#     boxes_shape = tf.shape(input=boxes)
    
#     boxes = tf.reshape(boxes, (-1, 5))
    
#     boxes = []
#     for rect in boxes:
#         print(rect)
#         box = cv2.boxPoints(((rect[1], rect[0]), (rect[3], rect[2]), 57.29577951308232*rect[4]))
#         box = np.reshape(box, [-1, ])
#         #if any(v < 0 for v in box ):
#             #continue
#         if (box[0] > window[2]) or (box[2] > window[2]) or (box[4] > window[2]) or (box[6] > window[2]):
#             continue
#         if (box[0] < window[0]) or (box[2] < window[0]) or (box[4] > window[0]) or (box[6] < window[0]):
#             continue
#         if (box[1] > window[3]) or (box[3] > window[3]) or (box[5] > window[3]) or (box[7] > window[3]):
#             continue
#         boxes.append(rect)

#     boxes = np.array(boxes, dtype=np.float32)
#     #boxes = boxes.reshape(1,-1)
#     boxes = tf.convert_to_tensor(boxes, dtype=np.float32)
#     pdb.set_trace() 

#     return boxes

def clip_boxes_graph(boxes, window):
    """
    boxes: [N, 5] each row is (y, x, h, w, angle)
    window: [5] in the form y1, x1, y2, x2
    """
    # Split corners
    wy1, wx1, wy2, wx2 = tf.split(window, 4)
    y1, x1, y2, x2, angle = tf.split(boxes, 5, axis=1)


    # Return boxes (y,x,h,w,angle)
    # Convert to (y1, x1, y2, x2)
#    R = tf.math.sqrt(boxes[:,2]**2+boxes[:,3]**2)
#    phi = tf.math.atan(boxes[:,2]/boxes[:,3])

#    y1 = boxes[:,0] - 0.5*boxes[:,2]
#    x1 = boxes[:,1] - 0.5*boxes[:,3]

#    y1 = boxes[:,0] - 0.5*boxes[:,2]
#    x1 = boxes[:,1] - 0.5*boxes[:,3]

    # Clip
    y1 = tf.maximum(tf.minimum(y1, wy2), wy1)
    x1 = tf.maximum(tf.minimum(x1, wx2), wx1)
    y2 = tf.maximum(tf.minimum(y2, wy2), wy1)
    x2 = tf.maximum(tf.minimum(x2, wx2), wx1)
    
    clipped = tf.concat([y1, x1, y2, x2, angle], axis=1, name="clipped_boxes")

    return clipped


def nms_rotate_gpu(boxes_list, scores, iou_threshold, proposal_count, use_angle_condition=False, angle_gap_threshold=0, device_id=0):

    if use_angle_condition:
        y_c, x_c, h, w, theta = tf.unstack(boxes_list, axis=1)
        boxes_list = tf.transpose(tf.stack([x_c, y_c, w, h, theta]))
        det_tensor = tf.concat([boxes_list, tf.expand_dims(scores, axis=1)], axis=1)
        keep = tf.numpy_function(rotate_gpu_nms,
                        inp=[det_tensor, iou_threshold, device_id],
                        Tout=tf.int64)

    else:
        y_c, x_c, h, w, theta = tf.unstack(boxes_list, axis=1)
        boxes_list = tf.transpose(tf.stack([x_c, y_c, w, h, theta]))
        det_tensor = tf.concat([boxes_list, tf.expand_dims(scores, axis=1)], axis=1)
        keep = tf.numpy_function(rotate_gpu_nms,
                        inp=[det_tensor, iou_threshold, device_id],
                        Tout=tf.int64)
        keep = tf.reshape(keep, [-1])

    keep = tf.cond(tf.greater(tf.shape(keep)[0], proposal_count),true_fn=lambda: tf.slice(keep, [0], [proposal_count]),
    false_fn=lambda: keep)

    return keep.numpy().tolist()
    
def get_proposals(cls_logits, bbox_deltas, anchors, config, training):
    """Receives anchor scores and selects a subset to pass as proposals
    to the second stage. Filtering is done based on anchor scores and
    non-max suppression to remove overlaps. It also applies bounding
    box refinment details to anchors.

    Inputs:
        cls_logits: [batch, anchors, (bg prob, fg prob)]
        bbox_deltas: [batch, anchors, (dy, dx, log(dh), log(dw), dangle)]

    Returns:
        Proposals in normalized coordinates [batch, rois, (y, x, h, w, angle)]
    """

    scores = cls_logits[:,:,1] # foreground score

    deltas = bbox_deltas * np.reshape(config.RPN_BBOX_STD_DEV, [1, 1, 5])

    if training:
        pre_nms_count = config.RPN_TRAIN_PRE_NMS_COUNT
    else:
        pre_nms_count = config.RPN_TEST_PRE_NMS_COUNT
    pre_nms_limit = min(pre_nms_count, anchors.shape[0])

    ix = tf.nn.top_k(scores, pre_nms_limit,sorted=True,name="top_anchors").indices

    scores = utils.batch_slice([scores, ix], lambda x, y: tf.gather(x, y),
                               config.BATCH_SIZE)
    
    deltas = utils.batch_slice([deltas, ix], lambda x, y: tf.gather(x, y),
                               config.BATCH_SIZE)

    anchors = utils.batch_slice(ix, lambda x: tf.gather(anchors, x),config.BATCH_SIZE,                                
                                    names=["pre_nms_anchors"])

    # Apply deltas to anchors to get refined anchors.
    # [batch, N, (y1, x1, y2, x2, angle)]    
    boxes = utils.batch_slice([anchors, deltas],lambda x, y: apply_box_deltas_graph(x, y),                              
                                   config.BATCH_SIZE,names=["refined_anchors"])
    
    
    # Clip to image boundaries. [batch, N, (y1, x1, y2, x2)]
    # Bill - NEED to FIX
    height, width = config.IMAGE_SHAPE[:2]
    window = np.array([0, 0, height, width],dtype=np.float32)
    
    #pdb.set_trace()
    # boxes = utils.batch_slice(boxes,lambda x: clip_boxes_graph_rotated(x, window),
    #                           config.BATCH_SIZE,names=["refined_anchors_clipped"])
    
    # Normalize dimensions to range of 0 to 1.
    normalized_boxes = boxes #/ np.array([[height, width, height, width, 1.0]])

    if training:
        proposal_count = config.RPN_TRAIN_PROPOSAL_COUNT
    else:
        proposal_count =config.RPN_TEST_PROPOSAL_COUNT

#    # Non-max suppression - OLD -DOES NOT WORK WITH ROTATED ANGLE
#    def nms(normalized_boxes, scores):
#        boxes = normalized_boxes[:,0:4]
#        indices = tf.image.non_max_suppression(boxes, scores, proposal_count,
#            config.RPN_NMS_THRESHOLD, name="rpn_non_max_suppression")
#        proposals = tf.gather(normalized_boxes, indices)
#        # Pad if needed
#        padding = proposal_count - tf.shape(input=proposals)[0]
#        proposals = tf.concat([proposals, tf.zeros([padding, 5],tf.float32)], 0)
#        return proposals
#    proposals = utils.batch_slice([normalized_boxes, scores], nms, config.BATCH_SIZE)

    # Non-max suppression - NEW
    # Currently NMS only works with GPU and not with CPU
    def nms(normalized_boxes, scores):
        # Profiling the code 
        #pr = cProfile.Profile()
        #pr.enable()
        #start_time = time.time()
        indices = nms_rotate_gpu(normalized_boxes, scores, config.RPN_NMS_THRESHOLD, proposal_count)
        #end_time = time.time() - start_time
        #print("nms took: {} seconds".format(end_time))
        #pr.disable()
        #ps = pstats.Stats(pr, stream=sys.stdout)
        #ps.print_stats()
        #indices = cpu_nms_python.cpu_nms(normalized_boxes.numpy(), scores.numpy(), config.RPN_NMS_THRESHOLD, proposal_count)
        proposals = tf.gather(normalized_boxes, indices)
        # Pad if needed
        padding = proposal_count - tf.shape(input=proposals)[0]
        proposals = tf.concat([proposals, tf.zeros([padding, 5],tf.float32)], 0)
        return proposals

    proposals = utils.batch_slice([normalized_boxes, scores], nms, config.BATCH_SIZE)
    
    return proposals

def iou_rotate_calculate(boxes1, boxes2, use_gpu=False, gpu_id=0):
    '''
    :param boxes_list1:[N, 8] tensor
    :param boxes_list2: [M, 8] tensor
    :return:
    '''
    boxes1 = tf.cast(boxes1, tf.float32)
    boxes2 = tf.cast(boxes2, tf.float32)
    
    if use_gpu:
        iou_matrix = tf.numpy_function(rbbx_overlaps,
                                inp=[boxes1, boxes2, gpu_id],
                                Tout=tf.float32)
    else:
        iou_matrix = tf.numpy_function(get_iou_matrix, inp=[boxes1, boxes2],
                                Tout=tf.float32)

    iou_matrix = tf.reshape(iou_matrix, [tf.shape(boxes1)[0], tf.shape(boxes2)[0]])
    #pdb.set_trace()
    return iou_matrix


def _build_rpn_targets(gt_class_ids, gt_boxes, anchors, config):
    """Given the anchors and GT boxes, compute overlaps and identify positive
    anchors and deltas to refine them to match their corresponding GT boxes.
    Only RPN_TRAIN_SIZE ROIs are labeled. Rest are set to background

    anchors: [num_anchors, (y1, x1, y2, x2)]
    gt_class_ids: [num_gt_boxes] Integer class IDs.
    gt_boxes: [num_gt_boxes, (y1, x1, y2, x2)]

    Returns:
    rpn_match: [N] (int32) matches between anchors and GT boxes.
               1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_bbox: [N, (dy, dx, log(dh), log(dw))] Anchor bbox deltas.
    """
    # remove zero boxes in gt
    non_zero = np.any(gt_boxes, axis=1)
    gt_boxes = gt_boxes[non_zero]
    gt_class_ids = gt_class_ids[non_zero]
    
    
    # JCQ bypass if there are no GTs
    if len(gt_class_ids) == 0:  
        rpn_match = np.zeros ( (anchors.shape[0]  ), dtype=np.int32)
        rpn_bbox  = np.zeros( (anchors.shape[0], 5), dtype=np.float32)
        # randomly select config.RPN_TRAIN_ANCHORS_PER_IMAGE anchors to be negative and the rest neutral
        rpn_match[:config.RPN_TRAIN_ANCHORS_PER_IMAGE] = -1.0
        rpn_match = np.random.permutation(rpn_match)
        return rpn_match, rpn_bbox

    # RPN Match: 1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_match = np.zeros([anchors.shape[0]], dtype=np.int32)
    # RPN bounding boxes: [max anchors per image, (dy, dx, log(dh), log(dw), dangle)]
    rpn_bbox = np.zeros((config.RPN_TRAIN_ANCHORS_PER_IMAGE, 5))

    # # Compute overlaps [num_anchors, num_gt_boxes]
#    overlaps = cyutils.bbox_overlaps(anchors, gt_boxes.numpy() )
    
    # Anirudh - changed to python version
    #overlaps = cyutils2.bbox_overlaps(anchors, gt_boxes ) ## Original Line 
    #print("overlaps_1 {}".format(overlaps_1))
    #overlaps = nms_r5.bbox_overlaps(anchors, gt_boxes.numpy() )
    
    #Converting anchors and gt_boxes in to normal co-ordinates 
    #start_time = time.time()
    overlaps = iou_rotate_calculate(anchors, gt_boxes)
    overlaps = overlaps.numpy()
    #end_time = time.time() - start_time
    #print("box_overlap time : {}".format(end_time))
    print("overlaps {}".format(overlaps))
    #pdb.set_trace()
    # Match anchors to GT Boxes
    # If an anchor overlaps a GT box with IoU >= 0.7 then it's positive.
    # If an anchor overlaps a GT box with IoU < 0.3 then it's negative.
    # However, don't keep any GT box unmatched (rare, but happens). Instead,
    # match it to the closest anchor (even if its max IoU is < 0.3).
    # 1. Set negative anchors first. It gets overwritten if a gt box is matched to them.
    anchor_iou_argmax = np.argmax(overlaps, axis=1)
    anchor_iou_max = overlaps[np.arange(overlaps.shape[0]), anchor_iou_argmax]
    rpn_match[anchor_iou_max <= config.RPN_NEG_THRESHOLD] = -1.0
    # 2. Set an anchor for each GT box (regardless of IoU value).
    # TODO: If multiple anchors have the same IoU match all of them
    gt_iou_argmax = np.argmax(overlaps, axis=0)
    rpn_match[gt_iou_argmax] = 1.0

    # 3. Set anchors with high overlap as positive.
    rpn_match[anchor_iou_max >= config.RPN_POS_THRESHOLD] = 1.0

    margin = 0
    # set cross-boundary anchors to background
    # BILL NEED TO FIX
    inds_out = np.where(
            ~((anchors[:,0] > margin) &
              (anchors[:,1] > margin) &
              (anchors[:,2] < config.IMG_HEIGHT - margin) &
              (anchors[:,3] < config.IMG_WIDTH - margin))
             )[0]
    rpn_match[inds_out] = 0.0

    # Subsample to balance positive and negative anchors
    # Don't let positives be more than half the anchors
    ids = np.where(rpn_match == 1)[0]
    extra = len(ids) - (config.RPN_TRAIN_ANCHORS_PER_IMAGE // 2)
    if extra > 0:
        # Reset the extra ones to neutral
        ids = np.random.choice(ids, extra, replace=False)
        rpn_match[ids] = 0.0
    # Same for negative proposals
    ids = np.where(rpn_match == -1)[0]
    extra = len(ids) - (config.RPN_TRAIN_ANCHORS_PER_IMAGE - np.sum(rpn_match == 1))
    if extra > 0:
        # Rest the extra ones to neutral
        ids = np.random.choice(ids, extra, replace=False)
        rpn_match[ids] = 0.0

    # For positive anchors, compute shift and scale needed to transform them
    # to match the corresponding GT boxes.
    ids = np.where(rpn_match == 1)[0]
    #pdb.set_trace()
    ix = 0  # index into rpn_bbox
    # TODO: use box_refinment() rather than duplicating the code here
    for i, a in zip(ids, anchors[ids]):
        # Closest gt box (it might have IoU < 0.7)
        gt = gt_boxes[anchor_iou_argmax[i]]

        # GT Box uysinng center plus width/height
        gt_h = gt[2]
        gt_w = gt[3]
        gt_center_y = gt[0]
        gt_center_x = gt[1]
        gt_angle = gt[4]
        
        # Anchor
        a_h = a[2]
        a_w = a[3]
        a_center_y = a[0]
        a_center_x = a[1]
        a_angle = a[4]
        
        # Inorder to avoid NaNs in division and log
        gt_w += 1e-8
        gt_h += 1e-8
        a_w += 1e-8
        a_h += 1e-8
        # Compute the bbox refinement that the RPN should predict.
        rpn_bbox[ix] = [
            (gt_center_y - a_center_y) / a_h,
            (gt_center_x - a_center_x) / a_w,
            np.log(gt_h / a_h),
            np.log(gt_w / a_w),
            np.tan(gt_angle - a_angle)   # NEW - placeholder
            #gt_angle - a_angle
        ]
        # Normalize
        rpn_bbox[ix] /= config.RPN_BBOX_STD_DEV
        ix += 1
    return rpn_match.astype(np.float32), rpn_bbox.astype(np.float32)

def rpn_targets_graph(anchors, gt_cls, gt_bboxes, config):
    """Wrapper for _build_rpn_targets to convert it to tf op

    Inputs:
        - anchors: [num_anchors, 5]
        - gt_cls: [batch_size, num_boxes]
        - gt_bboxes: [batch_size, num_boxes, 5]

    Outputs:
        - rpn_cls_gt: [batch_size, num_anchors]
        - rpn_bbox_gt: [batch_size, num_anchors]
    """
    
    def rpn_targets(gt_cls, gt_bboxes):

        return tf.numpy_function(lambda x,y : _build_rpn_targets(x,y,                                                              
                                            anchors=anchors,
                                            config=config),
                                            [gt_cls, gt_bboxes],
                                            [tf.float32, tf.float32],
                                            name='rpn_targets_python')
    
    rpn_cls_gt, rpn_bbox_gt = utils.batch_slice([gt_cls, gt_bboxes],
                                    rpn_targets,
                                    batch_size=config.BATCH_SIZE,)
    
    return rpn_cls_gt, rpn_bbox_gt