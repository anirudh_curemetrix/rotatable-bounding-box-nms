import numpy as np
import PIL.Image as Image
import PIL.ImageColor as ImageColor
import PIL.ImageDraw as ImageDraw
import PIL.ImageFont as ImageFont
import tensorflow as tf
import math

def batch_slice(inputs, graph_fn, batch_size, names=None):
    """Splits inputs into slices and feeds each slice to a copy of the given
    computation graph and then combines the results. It allows you to run a
    graph on a batch of inputs even if the graph is written to support one
    instance only.

    inputs: list of tensors. All must have the same first dimension length
    graph_fn: A function that returns a TF tensor that's part of a graph.
    batch_size: number of slices to divide the data into.
    names: If provided, assigns names to the resulting tensors.
    """
    if not isinstance(inputs, list):
        inputs = [inputs]

    outputs = []
    for i in range(batch_size):
        inputs_slice = [x[i] for x in inputs]
        output_slice = graph_fn(*inputs_slice)
        if not isinstance(output_slice, (tuple, list)):
            output_slice = [output_slice]
        outputs.append(output_slice)
    # Change outputs from a list of slices where each is
    # a list of outputs to a list of outputs and each has
    # a list of slices
    outputs = list(zip(*outputs))

    if names is None:
        names = [None] * len(outputs)

    result = [tf.stack(o, axis=0, name=n)
              for o, n in zip(outputs, names)]
    if len(result) == 1:
        result = result[0]

    return result


STANDARD_COLORS = [
    'AliceBlue', 'Chartreuse', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque',
    'BlanchedAlmond', 'BlueViolet', 'BurlyWood', 'CadetBlue', 'AntiqueWhite',
    'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson', 'Cyan',
    'DarkCyan', 'DarkGoldenRod', 'DarkGrey', 'DarkKhaki', 'DarkOrange',
    'DarkOrchid', 'DarkSalmon', 'DarkSeaGreen', 'DarkTurquoise', 'DarkViolet',
    'DeepPink', 'DeepSkyBlue', 'DodgerBlue', 'FireBrick', 'FloralWhite',
    'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'Gold', 'GoldenRod',
    'Salmon', 'Tan', 'HoneyDew', 'HotPink', 'IndianRed', 'Ivory', 'Khaki',
    'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue',
    'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey',
    'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue',
    'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime',
    'LimeGreen', 'Linen', 'Magenta', 'MediumAquaMarine', 'MediumOrchid',
    'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen',
    'MediumTurquoise', 'MediumVioletRed', 'MintCream', 'MistyRose', 'Moccasin',
    'NavajoWhite', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed',
    'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed',
    'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple',
    'Red', 'RosyBrown', 'RoyalBlue', 'SaddleBrown', 'Green', 'SandyBrown',
    'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
    'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'GreenYellow',
    'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White',
    'WhiteSmoke', 'Yellow', 'YellowGreen'
]

NUM_COLORS = len(STANDARD_COLORS)

try:
  FONT = ImageFont.truetype('arial.ttf', 24)
except IOError:
  FONT = ImageFont.load_default()



def _draw_single_box(image, ymin, xmin, ymax, xmax, display_str,
                    font, color='black', thickness=6):
  draw = ImageDraw.Draw(image)
  (left, right, top, bottom) = (xmin, xmax, ymin, ymax)
  draw.line([(left, top), (left, bottom), (right, bottom),
             (right, top), (left, top)], width=thickness, fill=color)
  text_bottom = bottom
  # Reverse list and print from bottom to top.
  text_width, text_height = font.getsize(display_str)
  margin = np.ceil(0.05 * text_height)
  draw.rectangle(
      [(left, text_bottom - text_height - 2 * margin), (left + text_width,
                                                        text_bottom)],
      fill=color)
  draw.text(
      (left + margin, text_bottom - text_height - margin),
      display_str,
      fill='black',
      font=font)

  return image


def draw_bounding_boxes_with_score(image, gt_boxes, scores, box_name, config):
  num_boxes = gt_boxes.shape[0]
  gt_boxes_new = gt_boxes.copy()
  gt_boxes_new = np.round(gt_boxes_new)
  disp_image = Image.fromarray(np.uint8(image + config.MEAN_PIXEL))
  

  for i in range(num_boxes):
    text = box_name + ' ' + str(scores[i])
    disp_image = _draw_single_box(disp_image,
                                gt_boxes_new[i, 0],
                                gt_boxes_new[i, 1],
                                gt_boxes_new[i, 2],
                                gt_boxes_new[i, 3],
                                text,
                                FONT,
                                color=STANDARD_COLORS[26])

  image = np.array(disp_image).astype(np.float32)
  return image

def image_with_bbox_summ_with_score(imgs, bboxes, scores, summ_name, box_name, config):
    def summ_py_func(imgs, bboxes, scores):
        return tf.py_function(lambda x,y,z: draw_bounding_boxes_with_score(x,y,z,
                            box_name, config),
                            [imgs, bboxes, scores],
                            tf.float32)
    disp_imgs = batch_slice([imgs, bboxes, scores],
                                summ_py_func,
                                batch_size=config.BATCH_SIZE)

    return tf.summary.image(summ_name, disp_imgs)

def draw_bounding_boxes(image, gt_boxes, box_name, config):
  num_boxes = gt_boxes.shape[0]
  gt_boxes_new = gt_boxes.copy()
  gt_boxes_new = np.round(gt_boxes_new)
  disp_image = Image.fromarray(np.uint8(image + config.MEAN_PIXEL))
  print("GT Boxes: {}".format(gt_boxes))

  for i in range(num_boxes):
    disp_image = _draw_single_box(disp_image,
                                gt_boxes_new[i, 0],
                                gt_boxes_new[i, 1],
                                gt_boxes_new[i, 2],
                                gt_boxes_new[i, 3],
                                box_name,
                                FONT,
                                color=STANDARD_COLORS[26])

  image = np.array(disp_image)#.astype(np.float32)*255
  return image

def draw_bounding_boxes_rotated(image, gt_boxes, box_name, config):
    """
    Assuming gt_boxes have the following features (num_boxes, y_c, x_c, h, w, angle)
    """
    num_boxes = gt_boxes.shape[0]
    gt_boxes_new = gt_boxes.copy()
    gt_boxes_new = np.round(gt_boxes_new)
    disp_image = Image.fromarray(np.uint8(image + config.MEAN_PIXEL))

    for i in range(num_boxes):
        y_c = gt_boxes_new[i,0]
        x_c = gt_boxes_new[i,1]
        h = gt_boxes_new[i,2]
        w = gt_boxes_new[i,3]
        angle = gt_boxes_new[i,4]
        dx = w/2
        dy = h/2
        #print("--------Angle-----:{} ".format(angle))
        dxcos = dx*math.cos(angle*np.pi/180)
        dxsin = dx*math.sin(angle*np.pi/180)
        dycos = dy*math.cos(angle*np.pi/180)
        dysin = dy*math.sin(angle*np.pi/180)

        x1  = x_c +  -dxcos - -dysin
        y1  = y_c + -dxsin + -dycos
        #x2 = cx + dxcos - -dysin
        #y2 = cy + dxsin + -dycos
        x3 = x_c + dxcos -  dysin 
        y3 = y_c +  dxsin +  dycos
        #x4 = cx + -dxcos -  dysin 
        #y4 = cy +  -dxsin +  dycos

        disp_image = _draw_single_box(disp_image, y1, x1, y3, x3, box_name, FONT, color=STANDARD_COLORS[26])

    image = np.array(disp_image)#.astype(np.float32)*255
    return image

def image_with_bbox_summ(imgs, bboxes, summ_name, box_name, config, writer, iteration):
    def summ_py_func(imgs, bboxes):
        return tf.numpy_function(lambda x,y: draw_bounding_boxes_rotated(x,y, box_name, config),
                            [imgs, bboxes],
                            tf.float32)
    disp_imgs = batch_slice([imgs, bboxes],
                                summ_py_func,
                                batch_size=config.BATCH_SIZE)
    with writer.as_default():
        tf.summary.image(summ_name, disp_imgs, step=iteration)
    #return tf.summary.image(summ_name, disp_imgs, step=iteration)
