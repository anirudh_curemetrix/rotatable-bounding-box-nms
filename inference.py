"""
Apply the Shashank FRCNN model to a directory of PNG images
The PNGs should already be the proper size (usually 2048x1600x3)
The output is a JSON file which contains the ROI bounding boxes and FRCNN scores
The coordinate system of the ROIs is the same as the coordinate system of the input PNGs
which is not the same as our usual _MassCalc.json files. """


import tensorflow as tf
import numpy as np
from config import Config
import skimage.io as io

import argparse
import glob
import os
import json

from frcnn_model import FRCNN
from cmdataset import CMDataset
from utils import draw_bounding_boxes_with_score
from tensorflow.keras.mixed_precision import experimental as mixed_precision

#import matplotlib
#import matplotlib.pyplot as plt
import nms
import pdb

# Define Dataset
#config = Config()
#config = Config(exp_dir="/nfs/experiments/CMA-3D-cmNN/Focal-Loss/Exp25-focal20-RGB-V2native/")
config = Config(exp_dir="/nfs/experiments/CMA-3D-cmNN/Port/Test1/")
#/nfs/experiments/CMA-3D-cmNN/Focal-Loss/Exp25-focal20-RGB-V2native-F/")

# NMS IoU threshold for ROI pruning 
NMS_OVERLAP = 0.3

SHOW_DETS = False
MIN_FRCNN_DISPLAY_SCORE = 0.1

def load_image(png_path):
    # image in RGB
    image = io.imread(png_path)

    if image.ndim != 3:
        image = np.repeat(image[:,:,np.newaxis], 3, axis=2)

    return image

def output_json(png_name, output_path, rois, scores):
    # rois are the bounding boxes in Shashank order: (y1, x1, y2, x2)
    all_rois = []
    for i in range(rois.shape[0]):
        new_roi = {}
        new_roi['min'] = {'x':float(rois[i,1]), 'y':float(rois[i,0])}
        new_roi['max'] = {'x':float(rois[i,3]), 'y':float(rois[i,2])}
        new_roi['frcnnScore'] = float(scores[i])
        all_rois.append(new_roi)

    json_path = os.path.join(output_path, png_name.replace('.png', '.json'))
    with open(json_path, 'wt') as fp:
        json.dump(all_rois, fp, indent=1, separators=(',', ': '))
        print('ROIs saved in %s ' % json_path)

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='FRCNN inference code')
    
    # path to PNGs of proper size  example: /nfs/images/PNGs_2048x1600_maxwell/Mass/ 
    parser.add_argument('-i',  '--png_path', dest='png_path', help='Path to PNGs to apply FRCNN to', required=True) 

    # the output ROIs will be in the same coordinate system as the input PNGs 
    # warning:  this is not the usual coordinate system (DICOM) of the JSON files so it cannot be used directly in ROC code. 
    parser.add_argument('-o',  '--output_path', dest='output_path', help='Path to output JSONs', required=False) 

    parser.add_argument('-g',  '--gpu_num', dest='gpu_num', help='GPU number', required=True)
    
    # change the number of classes here. Only the ROIs from the last class are outputted!  
    parser.add_argument('-n',  '--num_classes', dest='num_classes', help='Number of classes', required=False, default=3)
    # path to TF model snapshot file (only include the part of the name common to all three checkpoint files)
    parser.add_argument('-c',  '--ckpt', dest='ckpt', help='Path to TF model weights', required=True)

    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_num)

    # Directory for JSON results
    outputBase = args.ckpt+"/Validation/"
    if not os.path.exists(outputBase):
        os.mkdir(outputBase)

    if args.output_path==None:
        outputBase = (args.ckpt).split("/")[0]
        fileName =(args.ckpt).split("/")[-1]
        itnum = fileName.replace("Weights","").replace(".h5","")        
        outputDir = outputBase+"/JSON-"+itnum
    else:
        outputDir = args.output_path
    
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    config.NUM_CLASSES = int(args.num_classes)
    
    png_list = glob.glob(args.png_path + '*.png')

    tfconfig = tf.compat.v1.ConfigProto(allow_soft_placement=True)
    tfconfig.gpu_options.allow_growth = True

    # Mixed precision
    policy = mixed_precision.Policy('mixed_float16')
    mixed_precision.set_policy(policy)
    print('Compute dtype: %s' % policy.compute_dtype)
    print('Variable dtype: %s' % policy.variable_dtype)

    # Train dataset
    dataset = CMDataset(config.CLASSES)
    dataset.load_data(config.DATASET, config)
    dataset.build_tf_dataset(config)
    
    # Build model and load weights
    model = FRCNN(config=config,training=True)

    img = load_image(png_list[0]) -  config.MEAN_PIXEL
    img = img[np.newaxis, :,:,:]
    img = tf.convert_to_tensor(img)
    model(dataset,training=True)
    
    print ("loading weights")
    model.load_weights(args.ckpt)
    print ("finished")
    
    for png_name in png_list:
        
        # process single image batches only
        img = load_image(png_name) -  config.MEAN_PIXEL
        img = img[np.newaxis, :,:,:]   
        img = tf.convert_to_tensor(img)
        
        # warning shashank coordinate order is (y1, x1, y2, x2)
        # our normal convention is (x1, y1, x2, y2)

        rois, scores  =  model(img,training=False)            
        
        if NMS_OVERLAP > 0:
            BBs = np.hstack([rois, scores[:,np.newaxis]])
            idx_keep1 = nms.nms_FZ(BBs, NMS_OVERLAP)
            idx_keep2 = nms.secondaryPrune(BBs[idx_keep1, :])
            # composite the two prunes
            idx_keep = idx_keep1[idx_keep2]
            scores = scores[idx_keep]
            rois = rois[idx_keep, :]

        output_json(os.path.basename(png_name), outputDir, rois, scores)

