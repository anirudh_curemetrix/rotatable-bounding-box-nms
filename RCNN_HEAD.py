from __future__ import division

import numpy as np
import tensorflow as tf
import tensorflow.keras.regularizers as regularizers
import utils
#import cython_modules.cython_utils as cyutils
import cython_modules.cython_new_utils as cyutils2
 
import cv2

from math import ceil
from backbones import resnet
import time 

#from iou import intersection_area

import pdb


############ Chnages made here #############
#import pstats, cProfile, sys
#import numpy as np

#import pyximport
#pyximport.install()

#import cython_new_utils
#import cython_modules.nms_rotated as nms_r
#import cython_modules.nms_rotated2 as nms_r2
#import nms_cpu_python 
#import nms_cpu_vectorized
# import cython_modules.nms_rotated5 as nms_r5
from cython_modules.rotate_polygon_nms import rotate_gpu_nms
from cython_modules.rbbox_overlaps import rbbx_overlaps
from cython_modules.iou_cpu import get_iou_matrix
#from nms_rotated import nms_rotate_cpu
#tf.debugging.enable_check_numerics()

############ Changes made here ##############

def iou_rotate_calculate(boxes1, boxes2, use_gpu=False, gpu_id=0):
    '''
    :param boxes_list1:[N, 8] tensor
    :param boxes_list2: [M, 8] tensor
    :return:
    '''
    boxes1 = tf.cast(boxes1, tf.float32)
    boxes2 = tf.cast(boxes2, tf.float32)
    
    if use_gpu:
        iou_matrix = tf.numpy_function(rbbx_overlaps,
                                inp=[boxes1, boxes2, gpu_id],
                                Tout=tf.float32)
    else:
        iou_matrix = tf.numpy_function(get_iou_matrix, inp=[boxes1, boxes2],
                                Tout=tf.float32)

    iou_matrix = tf.reshape(iou_matrix, [tf.shape(boxes1)[0], tf.shape(boxes2)[0]])
    #pdb.set_trace()
    return iou_matrix


class rcnn_head(tf.keras.Model):
    def __init__(self,config):
        super(rcnn_head,self).__init__()
             
        """Implements second stage classifier head
        Inputs:

           - pooled_rois: (batch_size, num_rois, w, h, c)
        
        Returns:

            - cls_logits: (batch_size*num_rois, num_classes)
            - bbox: (batch_size*num_rois, 5*num_classes)
        """
        self.config = config
        
        initializer = tf.random_normal_initializer(mean=0.0, stddev=0.01)
        initializer_bbox  = tf.random_normal_initializer(mean=0.0, stddev=0.001)

        self.block4 = resnet.block4(depth=101)
        
        self.cls_logits = tf.keras.layers.Dense(units=self.config.NUM_CLASSES,
                                                kernel_initializer=initializer,
                                                kernel_regularizer=regularizers.l2(self.config.WEIGHT_DECAY),
                                                activation=None,name="rcnn_logits",dtype="float32")

        self.bbox = tf.keras.layers.Dense(units=5*self.config.NUM_CLASSES,
                                          kernel_initializer=initializer_bbox,
                                          kernel_regularizer=regularizers.l2(self.config.WEIGHT_DECAY),
                                          activation=None,name="rcnn_bbox",dtype="float32")        

#    @tf.function    
    def __call__(self, pooled_rois, training=True):
        
        shape = pooled_rois.get_shape().as_list()
        pooled_rois = tf.reshape(pooled_rois, [-1] +  shape[2:])
        net = self.block4(pooled_rois, training=training)
       
        # average pooling done by reduce_mean
        net = tf.reduce_mean(input_tensor=net, axis=[1, 2])
    
        cls_logits = self.cls_logits(net)

        bbox = self.bbox(net)

        cls_logits = tf.reshape(cls_logits, [-1, self.config.NUM_CLASSES])
        bbox = tf.reshape(bbox, [-1, self.config.NUM_CLASSES, 5])

        return cls_logits, bbox

def _box_refinement_graph_rotated(box, gt_box):
    """
    Compute the refinement needed to transform box to gt_box 
    box and gt_box are [N,(y,x,h,w,angle)]
    """
    height = box[:, 2]
    width = box[:, 3] 
    center_y = box[:, 0] 
    center_x = box[:, 1]

    gt_height = gt_box[:, 2] 
    gt_width = gt_box[:, 3] 
    gt_center_y = gt_box[:, 0] 
    gt_center_x = gt_box[:, 1]

    gt_width += 1e-8
    gt_height += 1e-8
    height+= 1e-8
    width+= 1e-8

    dy = (gt_center_y - center_y) / height
    dx = (gt_center_x - center_x) / width
    dh = tf.math.log(gt_height / height)
    dw = tf.math.log(gt_width / width)

    # NEW - need to fix
    dAngle = tf.math.tan(box[:,4] - gt_box[:,4])

    result = tf.stack([dy, dx, dh, dw, dAngle], axis=1)
    return result 

def _box_refinement_graph(box, gt_box):
    """Compute refinement needed to transform box to gt_box.
    box and gt_box are [N, (y1, x1, y2, x2)]
    """
#    box = tf.cast(box, TF_DTYPE )
#    gt_box = tf.cast(gt_box, TF_DTYPE)

    height = box[:, 2] - box[:, 0]
    width = box[:, 3] - box[:, 1]
    center_y = box[:, 0] + 0.5 * height
    center_x = box[:, 1] + 0.5 * width

    gt_height = gt_box[:, 2] - gt_box[:, 0]
    gt_width = gt_box[:, 3] - gt_box[:, 1]
    gt_center_y = gt_box[:, 0] + 0.5 * gt_height
    gt_center_x = gt_box[:, 1] + 0.5 * gt_width

    dy = (gt_center_y - center_y) / height
    dx = (gt_center_x - center_x) / width
    dh = tf.math.log(gt_height / height)
    dw = tf.math.log(gt_width / width)

    # NEW - need to fix
    #dAngle = tf.math.tan(box[:,4] - gt_box[:,4])
    dAngle = box[:,4] - gt_box[:,4]
    
    result = tf.stack([dy, dx, dh, dw, dAngle], axis=1)
    return result


def _overlaps_graph(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)].
    """
    # 1. Tile boxes2 and repeate boxes1. This allows us to compare
    # every boxes1 against every boxes2 without loops.
    # TF doesn't have an equivalent to np.repeate() so simulate it
    # using tf.tile() and tf.reshape.
    b1 = tf.reshape(tf.tile(tf.expand_dims(boxes1, 1),
                            [1, 1, tf.shape(input=boxes2)[0]]), [-1, 5])
    b2 = tf.tile(boxes2, [tf.shape(input=boxes1)[0], 1])
    # 2. Compute intersections
    b1_y1, b1_x1, b1_y2, b1_x2, b1_angle = tf.split(b1, 5, axis=1)
    b2_y1, b2_x1, b2_y2, b2_x2, b2_angle = tf.split(b2, 5, axis=1)
    y1 = tf.maximum(b1_y1, b2_y1)
    x1 = tf.maximum(b1_x1, b2_x1)
    y2 = tf.minimum(b1_y2, b2_y2)
    x2 = tf.minimum(b1_x2, b2_x2)
    intersection = tf.maximum(x2 - x1, 0) * tf.maximum(y2 - y1, 0)
    # 3. Compute unions
    b1_area = (b1_y2 - b1_y1) * (b1_x2 - b1_x1)
    b2_area = (b2_y2 - b2_y1) * (b2_x2 - b2_x1)
    union = b1_area + b2_area - intersection
    # 4. Compute IoU and reshape to [boxes1, boxes2]
    iou = intersection / union
    overlaps = tf.reshape(iou, [tf.shape(input=boxes1)[0], tf.shape(input=boxes2)[0]])
    return overlaps


def _rcnn_targets(proposals, gt_class_ids, gt_boxes, config):
    """Generates detection targets for one image. Subsamples proposals and
    generates target class IDs, bounding box deltas, and masks for each.

    Inputs:
    proposals: [N, (y, x, h, w, angle)] in normalized coordinates. Might
               be zero padded if there are not enough proposals.
    gt_class_ids: [MAX_GT_INSTANCES] int class IDs
    gt_boxes: [MAX_GT_INSTANCES, (y, x, h, w, angle)] in normalized coordinates.

    Returns: Target ROIs and corresponding class IDs, bounding box shifts,
    rois: [NUM_RCNN_TRAIN_ROIS, (y, x, h, w, angle)] in normalized coordinates
    class_ids: [NUM_RCNN_TRAIN_ROIS]. Integer class IDs. Zero padded.
    deltas: [NUM_RCNN_TRAIN_ROIS, NUM_CLASSES, (dy, dx, log(dh), log(dw), dangle)]
            Class-specific bbox refinments.

    Note: Returned arrays might be zero padded if not enough target ROIs.
    """

    # Compute overlaps matrix [rpn_rois, gt_boxes]
    #Anirudh 
    #overlaps = _overlaps_graph(proposals, gt_boxes)
    overlaps = iou_rotate_calculate(proposals, gt_boxes, use_gpu=False, gpu_id=0)
    #overlaps = cyutils2.bbox_overlaps(proposals.numpy(), gt_boxes.numpy())
    #overlaps = overlaps.numpy()
    #pdb.set_trace()
    print("overlaps in rcnn :{}".format(overlaps))
    # Determine postive and negative ROIs
    roi_iou_max = tf.reduce_max(input_tensor=overlaps, axis=1)
    # 1. Positive ROIs are those with >= 0.5 IoU with a GT box
    positive_roi_bool = (roi_iou_max >= config.RCNN_POS_THRESHOLD)
    positive_indices = tf.where(positive_roi_bool)[:, 0]
    # 2. Negative ROIs are those with < 0.5 with every GT box
    negative_indices = tf.where(roi_iou_max < config.RCNN_NEG_THRESHOLD)[:, 0]

    num_pos_req = int(config.NUM_RCNN_TRAIN_ROIS*config.RCNN_TRAIN_FG_RATIO)
    positive_indices = tf.random.shuffle(positive_indices)[:num_pos_req]
    pos_count = tf.shape(input=positive_indices)[0]

    num_neg_req = tf.cast(config.NUM_RCNN_TRAIN_ROIS - pos_count, tf.int32)
    negative_indices = tf.random.shuffle(negative_indices)[:num_neg_req]

    positive_rois = tf.gather(proposals, positive_indices)
    negative_rois = tf.gather(proposals, negative_indices)

    # JCQ: in the unlikley event that there are not enough negatives
    # pad the batch with placeholder negative rois. 
    # It can happen that there are not enough negatives because roi_iou_max
    # can have nans when both the ROI and GT and placeholders (boxes of size zero)

    N_extra_negatives_req = num_neg_req - tf.shape(input=negative_indices)[0]
    #negative_rois = tf.Print(negative_rois, [tf.shape(positive_rois)], message='positive rois ')
    #negative_rois = tf.Print(negative_rois, [tf.shape(negative_rois)], message='negative rois before ')
    negative_rois = tf.pad(tensor=negative_rois, paddings=[(0, N_extra_negatives_req), (0, 0)])
    #negative_rois = tf.Print(negative_rois, [tf.shape(negative_rois)], message='negative rois after ')

    # Assign positive ROIs to GT boxes.
    positive_overlaps = tf.gather(overlaps, positive_indices)
    roi_gt_box_assignment = tf.argmax(input=positive_overlaps, axis=1)
    roi_gt_boxes = tf.gather(gt_boxes, roi_gt_box_assignment)
    roi_gt_class_ids = tf.gather(gt_class_ids, roi_gt_box_assignment)

    # Compute bbox refinement for positive ROIs

    #deltas = _box_refinement_graph(positive_rois, roi_gt_boxes)
    deltas = _box_refinement_graph_rotated(positive_rois, roi_gt_boxes)
    deltas /= config.BBOX_STD_DEV

    # Append negative ROIs and pad bbox deltas that
    # are not used for negative ROIs with zeros.
    rois = tf.concat([positive_rois, negative_rois], axis=0)
    N = tf.shape(input=negative_rois)[0]
    roi_gt_boxes = tf.pad(tensor=roi_gt_boxes, paddings=[(0, N), (0, 0)])
    roi_gt_class_ids = tf.pad(tensor=roi_gt_class_ids, paddings=[(0, N)])
    deltas = tf.pad(tensor=deltas, paddings=[(0, N), (0, 0)])

    return rois, roi_gt_class_ids, deltas #, masks

def rcnn_targets_graph(rois, gt_cls, gt_bboxes, config):
    """Wrapper around _rcnn_targets to do batch processing"""

    rois, roi_gt_cls, roi_gt_bbox = utils.batch_slice(
                                [rois, gt_cls, gt_bboxes],
                                lambda x,y,z: _rcnn_targets(x, y, z, config),
                                batch_size=config.BATCH_SIZE)

    return rois, roi_gt_cls, roi_gt_bbox


def clip_to_window(window, boxes):
    """
    window: (y1, x1, y2, x2). The window in the image we want to clip to.
    boxes: [N, (y1, x1, y2, x2)]
    """
    boxes[:, 0] = np.maximum(np.minimum(boxes[:, 0], window[2]), window[0])
    boxes[:, 1] = np.maximum(np.minimum(boxes[:, 1], window[3]), window[1])
    boxes[:, 2] = np.maximum(np.minimum(boxes[:, 2], window[2]), window[0])
    boxes[:, 3] = np.maximum(np.minimum(boxes[:, 3], window[3]), window[1])
    return boxes

def apply_box_deltas(boxes, deltas):
    """Applies the given deltas to the given boxes.
    boxes: [N, (y1, x1, y2, x2,angle)]. Note that (y2, x2) is outside the box.
    deltas: [N, (dy, dx, log(dh), log(dw),dAngle)]

    """
    # Convert to y, x, h, w
    center_y = boxes[:, 0]
    center_x = boxes[:, 1]
    height = boxes[:, 2]
    width = boxes[:, 3]
    
    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= np.exp(deltas[:, 2])
    width *= np.exp(deltas[:, 3])

    # Bill Need to fix 
    angle = boxes[:,4] + deltas[:,4]    
    
    return np.stack([center_y,center_x,height,width,angle], axis=1)

def nms_rotate_gpu(boxes_list, scores, iou_threshold, proposal_count, use_angle_condition=False, angle_gap_threshold=0, device_id=0):
    if use_angle_condition:
        y_c, x_c, h, w, theta = tf.unstack(boxes_list, axis=1)
        boxes_list = tf.transpose(tf.stack([x_c, y_c, w, h, theta]))
        det_tensor = tf.concat([boxes_list, tf.expand_dims(scores, axis=1)], axis=1)
        keep = tf.numpy_function(rotate_gpu_nms,
                        inp=[det_tensor, iou_threshold, device_id],
                        Tout=tf.int64)

    else:
        y_c, x_c, h, w, theta = tf.unstack(boxes_list, axis=1)
        boxes_list = tf.transpose(tf.stack([x_c, y_c, w, h, theta]))
        det_tensor = tf.concat([boxes_list, tf.expand_dims(scores, axis=1)], axis=1)
        keep = tf.numpy_function(rotate_gpu_nms,
                        inp=[det_tensor, iou_threshold, device_id],
                        Tout=tf.int64)
        keep = tf.reshape(keep, [-1])

    keep = tf.cond(tf.greater(tf.shape(keep)[0], proposal_count),true_fn=lambda: tf.slice(keep, [0], [proposal_count]),
    false_fn=lambda: keep)

    return keep.numpy().tolist()

def get_rcnn_detections(rois, pred_probs, deltas, config):
    """Apply bbox refinement s at stage 2
    Inputs:
        - rois: [num_rois, (y1, x1, y2, x2)]
        - pred_probs: [num_rois, num_classes]
        - deltas: [num_rois, num_classes, (dy, dx, log(dh), log(dw))]
        - config: configuration object
    Outputs:
        - rois: [n_detections, (y1, x1, y2, x2)]
        - probs: [n_detections, 1]
    """
    # we are interested only in the last class
    class_ids = (config.NUM_CLASSES - 1)*np.ones(pred_probs.shape[0]).astype(np.int)
    # Class probability of the last class of each ROI

#    class_scores = pred_probs[np.arange(class_ids.shape[0]), class_ids]
    class_scores = pred_probs[:,1].numpy()
    
    # Class-specific bounding box deltas
#    deltas_specific = deltas[np.arange(deltas.shape[0]), class_ids]
    deltas_specific = deltas[:,1,:]
    
    # Apply bounding box deltas
    # Shape: [boxes, (y1, x1, y2, x2)] in normalized coordinates

    refined_rois = apply_box_deltas(rois, deltas_specific * config.BBOX_STD_DEV)

    # Convert coordiates to image domain
    # height, width = config.IMAGE_SHAPE[:2]
    # window = np.array([0, 0, height, width])
    # refined_rois *= np.array([height, width, height, width, 1.0])
    
    # Clip boxes to image window
    # Bill - should we still clip?
#    refined_rois = clip_to_window(window, refined_rois)    

#    keep = cyutils.cpu_nms(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD)
#    keep = np.array(keep)

    # Bill - New - need to fix?
    #Anirudh 
    #keep = cyutils2.cpu_nms(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD,config.RPN_TEST_PROPOSAL_COUNT) 
    #keep = nms_r5.cpu_nms(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD,config.RPN_TEST_PROPOSAL_COUNT)
    keep = nms_rotate_gpu(refined_rois.numpy(), class_scores.numpy(), config.DETECTION_NMS_THRESHOLD, config.RPN_TEST_PROPOSAL_COUNT)
    #keep = cv2.dnn.NMSBoxes(refined_rois, class_scores, config.DETECTION_NMS_THRESHOLD,config.RPN_TEST_PROPOSAL_COUNT)
    keep = np.array(keep)
        
    # Round and cast to int since we're deadling with pixels now
    # Bill - do we need this
#    refined_rois = np.rint(refined_rois).astype(np.int32)

    # Keep top detections
    roi_count = config.RCNN_MAX_DETECTIONS
    top_ids = np.argsort(class_scores[keep])[::-1][:roi_count]
    keep = keep[top_ids]

    # Arrange output as [N, (y1, x1, y2, x2, class_id, score)]
    # Coordinates are in image domain.
    # result = np.hstack((refined_rois[keep],
                        # class_ids[keep][..., np.newaxis],
                        # class_scores[keep][..., np.newaxis]))

    refined_rois = refined_rois[keep]
    scores = class_scores[keep]

    return refined_rois, scores, keep.astype(np.int32)


#def get_feature_maps(features, idx, config):
#
#    feature_maps = utils.batch_slice([features, idx], lambda x,y: tf.gather(x, y), 
#        batch_size=config.BATCH_SIZE, names=['final_feature_maps'] )
#    return feature_maps



# Convert coordinates from (y,x,h,w,angle)

#def boxCoordinates(box):
#
        # Convert to corner coordinates (y1, x1, y2, x2)
#    boxes = np.concatenate([box_centers - 0.5 * box_sizes,
#                            box_centers + 0.5 * box_sizes], axis=1)

#    # Save new boxes in 
#    boxes = np.concatenate([box_centers,box_sizes],axis=1)
