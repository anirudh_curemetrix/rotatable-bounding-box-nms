import os
import sys
import argparse
import yaml
from IPython import embed


def default_config(yaml_path, model_path, num_classes=3, gt_type='M'):
    config = {}
    # the class names dont really matter, but there should be the correct number of classes
    # the 1st one is always backgroun and the last is alwasy the positive class
    if num_classes==2:
        config['CLASSES'] = ['__background__', 'positive']
    else:
        config['CLASSES'] = ['__background__', 'negative', 'positive']
    config['FRCNN_MODEL_PATH'] = model_path
    config['FINAL_NMS_PRUNE'] = 0.3
    config['MIN_FRCNN_SCORE'] = 0.001
    config['TYPE'] = gt_type
    config['NUM_CLASSES'] = num_classes
    with open(yaml_path, 'wt') as fp:
        fp.write(yaml.dump(config))


def freeze_graph(paths, ckpt_name, output_pb, num_classes=3):

    temp_pb = 'frcnn_resnet101_%dway_graph.pb' % num_classes

    if not os.path.exists(temp_pb):
        cmd = "CUDA_VISIBLE_DEVICES='' python export_inference_graph.py -n %d -o %s" %(num_classes, temp_pb)
        print(cmd)
        os.system(cmd)

    cmd = "CUDA_VISIBLE_DEVICES='' python " + paths['tf_env'] + "python/tools/freeze_graph.py "
    cmd = cmd + '--input_graph=%s ' % temp_pb
    cmd = cmd + '--input_checkpoint=%s ' % ckpt_name
    cmd = cmd + '--output_graph=%s ' % output_pb
    cmd = cmd + '--input_binary=true '
    cmd = cmd + '--output_node_names=rcnn_detections_graph,pred_cls_probs,final_feature_maps'
    print(cmd)
    os.system(cmd)


def apply_FRCNN(modelName, outputPath, gpuNum, gt_type, num_classes, paths):

    # this assumes the Maxwell validation set is avaiable on NFS
    if gt_type=='mass':
        inputPath_pos = os.path.join(paths['maxwell'], 'Mass/')
    else:
        inputPath_pos =  os.path.join(paths['maxwell'], 'Calcs/')

    inputPath_normal =  os.path.join(paths['maxwell'], 'Normal/')

    configPath = '%s_frcnn_temp.yaml' % gt_type

    q_algo_path = paths['q_algo']


    output_pb = './temp_frcnn_resnet101_%s_%dway_gpu%d.pb' % (gt_type, num_classes, gpuNum)
    freeze_graph(paths, modelName, output_pb, num_classes=num_classes)

    default_config(configPath, output_pb, gt_type=gt_type.upper()[0], num_classes=num_classes)

    os.environ['CUDA_VISIBLE_DEVICES']=str(gpuNum)
    cmd = 'python ' + q_algo_path + 'detector.py -i ' + inputPath_pos + ' -o ' +outputPath + ' -c ' +configPath
    print(cmd)
    os.system(cmd)

    cmd = 'python ' + q_algo_path + 'detector.py -i ' + inputPath_normal + ' -o ' +outputPath + ' -c ' +configPath
    print(cmd)
    os.system(cmd)

    cmd =  'python ' + q_algo_path + 'merge_jsons.py -i' + outputPath+'JSON-Outputs/'
    print(cmd)
    os.system(cmd)


if __name__=='__main__':

    gpu_num = 0
    iterNums = [87000]
    num_classes = 3
    gt_type = 'mass'
    exp_path = '/mnt/Array/share/users/shashank/frcnn_logs/3way_sig1_nomaxpool_norm/'
    model_pattern = 'CM-FRCNN.ckpt-'
    output_root = './3way_sig1_nomaxpool_norm_jsons_'
    import yaml
    from IPython import embed

    # load paths to extenal dependencies
    with open('paths_beastxp.yaml') as fp:
        paths = yaml.load(fp)

    for iterNum in iterNums:
        output = output_root +'%d/'%iterNum
        model_name = exp_path + model_pattern + str(iterNum)
        apply_FRCNN(model_name, output, gpu_num, gt_type, num_classes, paths)
