import glob,yaml, json
import os, sys
import numpy as np
import pickle
from IPython import embed
import time
import tensorflow as tf
from StringIO import StringIO
import argparse


def add_value(writer, iterNum, value, tag='Validation pAUC'):
    summary = tf.compat.v1.Summary(value=[tf.compat.v1.Summary.Value(tag=tag, simple_value=value) ] )
    writer.add_summary(summary, iterNum)


if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Tensorboard cleaning (stop tensorboard before running)')
    parser.add_argument('-i', dest='inputJson', help='Path to the JSON file', required=True)
    parser.add_argument('-o', dest='tbPath', help='Path to the Tensorboard event files', required=True)
    args = parser.parse_args()


    with open(args.inputJson) as fp:
        data = json.load(fp)

    keys = data.keys()
    values = np.array(data.values())

    iterNums = np.array([int(x.split('.')[-2].split('-')[-1]) for x in keys])
    idx = np.argsort(iterNums)
    iterNums = iterNums[idx]
    values = values[idx]


    # remove the old event files
    file_list = glob.glob(args.tbPath +'/events*')
    for file_name in file_list:
        print('Removing {0}'.format(file_name))
        os.remove(file_name)

    writer = tf.compat.v1.summary.FileWriter( args.tbPath )

    for n in range(len(values)):
        add_value(writer, iterNums[n], values[n])




