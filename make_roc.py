import matplotlib.pyplot as plt
import sys
sys.path.append('/home/jquinn/Desktop/jquinn/utilities/Python_ROC/')
import roc
import numpy as np

from IPython import embed
from scipy import interpolate

import glob


if __name__=='__main__':

    save = False
    # json root folder

    all_folders = glob.glob('/mnt/Array/share/users/jquinn/jack/FRCNN-NoGT/validation/FRCNN-output-mass-2way-gpu0-48000/JSON-Outputs/')
    folder_list = []
    for folder in all_folders:
        if len(glob.glob(folder +'/*MassCalc.json')) > 0:
            folder_list.append(folder)


    #folder_list = ['./3way_sig1_nomaxpool_norm_jsons_87000/JSON-Outputs/']
    folder_list.append('/nfs/experiments/CMA-2591-Shashank-FRCNN/FRCNN-Mass-3-way-exp3/validation/FRCNN-output-mass-3way-gpu0-95000/JSON-Outputs/')
    print(folder_list)

    #roc type: mass or calcs
    roc_type = 'mass'  

    # score field:  frcnnScore, cnnScore, fcnScore, combinedScoreRaw, or qvalue
    sf = 'frcnnScore'

    # roc_level: image or patient
    roc_level = 'image'  

    # group field for either patient level or image level
    gf = 'patientId' if roc_level=='patient' else 'sop_instance_uid'

    # number of boot strap resampling iterations
    nb=0

    roc_maker = roc.ROC(configFile='roc_%s_config.yaml'%roc_type)
    for folder in folder_list:
        roc_maker.make_roc(folder, label=folder, scoreField=sf, groupField=gf, nboot=nb)


    plt.show()

