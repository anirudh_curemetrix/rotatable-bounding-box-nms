import tensorflow as tf
from tensorflow.keras.mixed_precision import experimental as mixed_precision
import pickle
import pdb

# Mixed precision
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_policy(policy)
print('Compute dtype: %s' % policy.compute_dtype)
print('Variable dtype: %s' % policy.variable_dtype)


IMG_HEIGHT=2048
IMG_WIDTH=1600

input_shape = (IMG_HEIGHT, IMG_WIDTH,3)
resnet101 = tf.keras.applications.ResNet101(input_shape=input_shape,
                                            include_top=False, weights='imagenet')

block3 = tf.keras.Model(inputs=resnet101.inputs,
                        outputs=resnet101.get_layer("conv4_block23_out").output)


print("Saving Blocks1-3 ResNet101")
#block3.save("Block3")

# Save weight for Block4
weights = {}
for layer in resnet101.layers:
    if layer.name.split("_")[0]=="conv5":
        findLayer = resnet101.get_layer(layer.name)
        weights[layer.name] = findLayer.get_weights()

print("Saving Block4 Weights")
with open("Block4_Weights.pkl","wb") as fp:        
    pickle.dump(weights,fp)



