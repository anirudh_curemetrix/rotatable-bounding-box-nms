import tensorflow as tf
#import tensorflow.contrib.slim as slim
#from tensorflow.contrib.slim.nets import resnet_utils, resnet_v1
#from tensorflow.contrib.slim.python.slim.nets.resnet_v1 import resnet_v1_block
# Bill

import tf_slim as slim
from tf_slim.nets import resnet_utils, resnet_v1
from tf_slim.nets.resnet_v1 import resnet_v1_block


def resnet_arg_scope(is_training=True,
                     batch_norm_decay=0.997,
                     batch_norm_epsilon=1e-5,
                     batch_norm_scale=True):
    batch_norm_params = {
        'is_training': False,
        'decay': batch_norm_decay,
        'epsilon': batch_norm_epsilon,
        'scale': batch_norm_scale,
        'trainable': False,
        'updates_collections': tf.compat.v1.GraphKeys.UPDATE_OPS
        }

    with slim.arg_scope([slim.conv2d],
                      weights_regularizer=tf.keras.regularizers.l2(0.5 * (0.0001)),
                      weights_initializer=tf.compat.v1.keras.initializers.VarianceScaling(scale=2.0),
                      trainable=is_training,
                      activation_fn=tf.nn.relu,
                      normalizer_fn=slim.batch_norm,
                      normalizer_params=batch_norm_params):
        with slim.arg_scope([slim.batch_norm], **batch_norm_params) as arg_sc:
            return arg_sc


def build_base(net):
    with tf.compat.v1.variable_scope('resnet_v1_101'):
      net = resnet_utils.conv2d_same(net, 64, 7, stride=2, scope='conv1')
      net = tf.pad(tensor=net, paddings=[[0, 0], [1, 1], [1, 1], [0, 0]])
      net = slim.max_pool2d(net, [3, 3], stride=2, padding='VALID', scope='pool1')

    return net

def resnet101(net, config, is_training, reuse=None):

    assert (0 <= config.RESNET_FIXED_BLOCKS <= 3)
    blocks = [resnet_v1_block('block1', base_depth=64, num_units=3, stride=2),
              resnet_v1_block('block2', base_depth=128, num_units=4, stride=2),
              # use stride 1 for the last conv4 layer
              resnet_v1_block('block3', base_depth=256, num_units=23, stride=1)]
              # resnet_v1_block('block4', base_depth=512, num_units=3, stride=1)]

    with slim.arg_scope(resnet_arg_scope(is_training=False)):
        net_conv = build_base(net)

    if config.RESNET_FIXED_BLOCKS > 0:
        with slim.arg_scope(resnet_arg_scope(is_training=False)):
            net_conv, _ = resnet_v1.resnet_v1(net_conv,
                                       blocks[0:config.RESNET_FIXED_BLOCKS],
                                       global_pool=False,
                                       include_root_block=False,
                                       reuse=reuse,
                                       scope='resnet_v1_101')
    if config.RESNET_FIXED_BLOCKS < 3:
        with slim.arg_scope(resnet_arg_scope(is_training=is_training)):
            net_conv, _ = resnet_v1.resnet_v1(net_conv,
                                   blocks[config.RESNET_FIXED_BLOCKS:],
                                   global_pool=False,
                                   include_root_block=False,
                                   reuse=reuse,
                                   scope='resnet_v1_101')

    return net_conv


def resnet_block4(net, is_training, reuse=None):
    blocks = [resnet_v1_block('block4', base_depth=512, num_units=3, stride=1)]

    with slim.arg_scope(resnet_arg_scope(is_training=is_training)):
        fc7, _ = resnet_v1.resnet_v1(net,
                                   blocks[-1:],
                                   global_pool=False,
                                   include_root_block=False,
                                   reuse=reuse,
                                   scope='resnet_v1_101')
      # average pooling done by reduce_mean
    fc7 = tf.reduce_mean(input_tensor=fc7, axis=[1, 2])
    return fc7

