from __future__ import division

import numpy as np
import tensorflow as tf
import utils
import cv2
from math import ceil
import time 

#from iou import intersection_area

import pdb

def forward_convert(coordinate):
    """
    :param coordinate: format [y_c, x_c, h, w, theta] theta in radians 
    :return: format [x1, y1, x2, y2, x3, y3, x4, y4]
    """
    boxes = []
    for rect in coordinate:
        box = cv2.boxPoints(((rect[1], rect[0]), (rect[3], rect[2]), 57.29577951308232*rect[4]))
        box = np.reshape(box, [-1, ])
        boxes.append([box[0], box[1], box[2], box[3], box[4], box[5], box[6], box[7]])
    
    return np.array(boxes, dtype=np.float32)

def get_horizen_minAreaRectangle(boxs):

    rpn_proposals_boxes_convert = tf.numpy_function(forward_convert,
                                            inp=[boxs],
                                            Tout=tf.float32)
    
    rpn_proposals_boxes_convert = tf.reshape(rpn_proposals_boxes_convert, [-1, 8])

    boxes_shape = tf.shape(rpn_proposals_boxes_convert)
    x_list = tf.strided_slice(rpn_proposals_boxes_convert, begin=[0, 0], end=[boxes_shape[0], boxes_shape[1]],
                                strides=[1, 2])
    y_list = tf.strided_slice(rpn_proposals_boxes_convert, begin=[0, 1], end=[boxes_shape[0], boxes_shape[1]],
                                strides=[1, 2])

    y_max = tf.reduce_max(y_list, axis=1)
    y_min = tf.reduce_min(y_list, axis=1)
    x_max = tf.reduce_max(x_list, axis=1)
    x_min = tf.reduce_min(x_list, axis=1)

    return tf.transpose(tf.stack([x_min, y_min, x_max, y_max], axis=0))

def roi_pooling(feature_maps, rois, img_shape):
    
    img_h, img_w = tf.cast(img_shape[0], tf.float32), tf.cast(img_shape[1], tf.float32)
    N = tf.shape(rois)[0]
    x1, y1, x2, y2 = tf.unstack(rois, axis=1)

    normalized_x1 = x1 / img_w
    normalized_x2 = x2 / img_w
    normalized_y1 = y1 / img_h
    normalized_y2 = y2 / img_h

    normalized_rois = tf.transpose( tf.stack([normalized_y1, normalized_x1, normalized_y2, normalized_x2]), name='get_normalized_rois')

    normalized_rois = tf.stop_gradient(normalized_rois)
    cropped_roi_features = tf.image.crop_and_resize(feature_maps, normalized_rois,
                                                        box_indices=tf.zeros(shape=[N, ],
                                                                            dtype=tf.int32),
                                                        crop_size=[14,14], #Hardcoded for now 
                                                        name='CROP_AND_RESIZE')

    maxpool = tf.keras.layers.MaxPool2D([2, 2], padding='same')
    pooled = maxpool(cropped_roi_features)

    return pooled

class ROIAlign(tf.keras.Model):
    def __init__(self,config):
        super(ROIAlign,self).__init__()
        """Implements ROI Pooling
        
        Inputs:
        - boxes: [batch, num_boxes, (y1, x1, y2, x2)] in normalized
        coordinates. Possibly padded with zeros if not enough
        boxes to fill the array.
        - Feature map: feature map from backbone
                    [batch, height, width, channels]

        Output:
        Pooled regions in the shape: [batch, num_boxes, height, width, channels].
        The width and height are those specific in the pool_shape in the config
        """
        self.config = config
        
        #if self.config.USE_ROI_MAXPOOL:
        #   self.maxpool = tf.keras.layers.MaxPool2D([2, 2], padding='same')
        self.maxpool = tf.keras.layers.MaxPool2D([2, 2], padding='same')
    
    # def RROI_Align_Perspective(feature_map, boxes):
    #     return pooled 
#    @tf.function        
    def __call__(self, boxes, feature_map):
            
        # Crop boxes [batch, num_boxes, (y1, x1, y2, x2, theta)] in normalized coords

        # Feature Maps. List of feature maps from different level of the
        # feature pyramid. Each is [batch, height, width, channels]

        # Crop and Resize
        # Result: [batch * num_boxes, pool_height, pool_width, channels]
        box_indices = tf.where(tf.ones_like(boxes[:,:,0], dtype=tf.bool))
        box_indices = tf.cast(box_indices[:,0], tf.int32)

        boxes_shape = tf.shape(input=boxes)
        #boxes = boxes * np.array([[self.config.IMG_HEIGHT, self.config.IMG_WIDTH, self.config.IMG_HEIGHT, self.config.IMG_WIDTH, 1]])
        boxes = tf.reshape(boxes, (-1, 5))
        
        boxes = tf.stop_gradient(boxes)
        box_indices = tf.stop_gradient(box_indices)

        fheight = feature_map.shape[1]
        fwidth = feature_map.shape[2]
        #pdb.set_trace()
        if self.config.USE_ROI_MAXPOOL:

            feat_shape = (self.config.POOL_SIZE, self.config.POOL_SIZE)
            img_shape  = (self.config.IMG_HEIGHT, self.config.IMG_WIDTH,3)
            rois = get_horizen_minAreaRectangle(boxes)
            pooled_features = roi_pooling(feature_maps=feature_map, rois=rois, img_shape=img_shape)
            ch = pooled_features.get_shape()[-1]
            pooled_features = tf.reshape(pooled_features, (boxes_shape[0], boxes_shape[1], feat_shape[0], feat_shape[1], ch))

            return pooled_features
        else:
            feat_shape = (2*self.config.POOL_SIZE, 2*self.config.POOL_SIZE)
            # NEW - need to fix - ROI pooling with angle

            # Return boxes (y,x,h,w,angle)
            depth = feature_map.shape[-1]
            pooled = np.zeros([len(box_indices),feat_shape[0],feat_shape[1],depth])
                
            for i,box in enumerate(boxes):
                height = np.int0(fheight*box[2])
                width = np.int0(fwidth*box[3])
                rect = cv2.boxPoints(((fwidth*box[1],fheight*box[0]),(width,height),180.0*box[4]/np.pi))
                rect = np.int0(rect)
                src_pts = rect.astype("float32")
                #print("Height {0}, Width {1}, fwidth {2}, fheight {3}, box[2] {4}".format(height, width, fwidth, fheight, box[2]))
                # dst_pts = np.array([[0, height-1],[0, 0],[width-1, 0],
                #             [width-1, height-1]], dtype="float32")
                dst_pts = np.array([[0,0], 
                                    [width, 0], 
                                    [0, height],
                                    [width, height]], dtype="float32")
                #print("src_pts {}".format(src_pts))
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                chunk = 512
                for j in range(0,depth,chunk):
                    pooled[i,:,:,j:j+chunk-1]=cv2.warpPerspective(feature_map.numpy()[0,:,:,j:j+chunk-1], M, feat_shape)
                #for j in range(depth):
                    #pooled[i,:,:,j] = cv2.warpPerspective(feature_map.numpy()[0,:,:,j], M, feat_shape)

            
            pooled =tf.convert_to_tensor(pooled)
            #pooled = self.maxpool(pooled)
#            pooled = tf.image.crop_and_resize(feature_map, boxes[:,0:4], box_indices, feat_shape,method="bilinear")
            
        ch = pooled.get_shape()[-1]
        feat_shape = (2*self.config.POOL_SIZE, 2*self.config.POOL_SIZE)
        #pdb.set_trace()
        # JCQ changed this to use the original shape of box 
        # it shouldn't have worked before but apparently TF
        # optimized out this operation. 
        pooled = tf.reshape(pooled, (boxes_shape[0], boxes_shape[1], feat_shape[0], feat_shape[1], ch))
        
        # returned shape = [batch, num_rois, w, h, c]
        #pdb.set_trace()
        return pooled
