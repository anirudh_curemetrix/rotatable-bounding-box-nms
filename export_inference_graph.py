import sys, os

import tensorflow as tf
import argparse
from tensorflow.python.platform import gfile


from config import Config
from frcnn_model import FRCNN

#from IPython import embed
import pdb

with tf.Graph().as_default() as graph:

    parser = argparse.ArgumentParser(description='Export FRCNN inference graph')
    parser.add_argument('-n', dest='num_classes', help='Number of Classes', required=False, default=3, type=int)
    parser.add_argument('-o', dest='output_pb', help='Name of PB to output', required=False, default='frcnn_resnet101.pb')
    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = ''

    config = Config()
    config.NUM_CLASSES = int(args.num_classes)

    model = FRCNN(config=config)
    model.build_inference_graph()
    
    for op in graph.get_operations():
        print(op.name)

    graph_def = graph.as_graph_def()
    tf.io.write_graph(graph_def, './', args.output_pb,as_text=False)

#    with gfile.GFile(args.output_pb, 'wb') as f:
#      f.write(graph_def.SerializeToString())

    print('TF Graph written to %s' % args.output_pb)
