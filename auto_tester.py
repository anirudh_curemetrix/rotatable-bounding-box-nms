import glob,yaml, json
import os, sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pickle
from IPython import embed
import time
import tensorflow as tf

# load paths to extenal dependencies
with open('paths_beastxp.yaml') as fp:
    paths = yaml.load(fp)

sys.path.insert(0, paths['util_repo']+'Python_ROC/')
import roc3 as roc
import apply_FRCNN
#from StringIO import StringIO
from io import StringIO
import argparse

# refresh interval in seconds
refreshInterval = 20


class AutoTester():
    def __init__(self, checkpointPath, minEvalIntervalSec=600, pattern='*.ckpt.index', 
                    gpu_num=0, tensorboardPath=None, gt_type='mass' , num_classes=3):
        print('Init AutoTest for %s' % checkpointPath)
        self.minEvalIntervalSec = minEvalIntervalSec
        self.path = checkpointPath
        self.lastTime = 0
        self.pattern = pattern
        self.gt_type = gt_type
        self.gpu_num = gpu_num
        self.num_classes = num_classes

        try:
        # use the log to keep track of which checkpoints were already processed or skipped
            with open(checkpointPath +'/auto_eval_log.json','rt') as fp:
                self.log = json.load(fp)
        except:
            self.log = {}

        self.shortName = 'validation' # checkpointPath.split('/')[-2]
        self.tb_val_path = os.path.join(tensorboardPath, self.shortName)
        if tensorboardPath is not None:
            # clean out the TB 
            #if os.path.exists(self.tb_val_path):
            #    if len(tf.gfile.Glob(self.tb_val_path+'/*')):
            #        [os.remove(file) for file in tf.gfile.Glob(self.tb_val_path+'/*')]

            # create a new tensorboard subdirectory to avoid interfering with training sessions
            tf.compat.v1.disable_eager_execution()
            self.writer = tf.compat.v1.summary.FileWriter( self.tb_val_path )
        else:
            self.writer = None


    def refresh(self):
        #try:
        fileList = np.array(glob.glob(os.path.join(self.path,self.pattern)), dtype=object)
        #print('%d files found' % len(fileList))
        modTimes = np.array( [ os.path.getmtime(x) for x in fileList] )
        idx = np.argsort(modTimes)
        modTimes = modTimes[idx]
        fileList = fileList[idx]
        for i in range(modTimes.shape[0]):
            if fileList[i] not in self.log:
                if modTimes[i]-self.lastTime > self.minEvalIntervalSec:
                    pauc = self.evaluate(fileList[i].replace('.index', ''))
                    self.lastTime = modTimes[i]
                    self.log[fileList[i]] = pauc
                    with open(self.path +'/auto_eval_log.json','wt') as fp:
                        json.dump(self.log, fp, indent=1, separators=(',', ': '))
                    break  # stop after processing one checkpoint to give the other 'threads' as chance
                else:
                    print('Skipping %s' % fileList[i])
                    self.log[fileList[i]] = None

        #except:
        #    print("There was a problem but let's ignore it")

    def evaluate(self, checkpoint):
        makePlot = True
        print('Evaluate: ' + checkpoint)
        # extract the iteration number from the check name -- this may have to be changed
        iterNum = int(checkpoint.split('-')[-1])

        print('Processing %s: %d' % (checkpoint, iterNum) )
        jsonPath = self.tb_val_path + '/FRCNN-output-%s-%dway-gpu%d-%d/' % (self.gt_type, self.num_classes, self.gpu_num, iterNum)

        # call the evaluation function here-- it should produce JSONs for the entire validation set using the checkpoint model
        apply_FRCNN.apply_FRCNN(checkpoint, jsonPath, self.gpu_num, self.gt_type, self.num_classes, paths)

        roc_maker = roc.ROC(configFile='roc_%s_config.yaml'%self.gt_type)
        roc_maker.make_roc(jsonPath+'JSON-Outputs/' , scoreField='frcnnScore',  makePlot=makePlot)
        pauc = roc_maker.pauc

        print('pAUC for %s : %f' % (checkpoint, pauc) )

        if self.writer is not None:
            # write the pauc as a scalar summary
            summary = tf.compat.v1.Summary(value=[tf.compat.v1.Summary.Value(tag='Validation pAUC', simple_value=pauc) ] )
            self.writer.add_summary(summary, iterNum)

            # write the ROC figure as an image summary
            if makePlot:
                s = StringIO()
                roc_maker.fig.savefig(s, format='png', dpi=50)
                img_size = roc_maker.fig.get_size_inches()*50
                img_size = np.round(img_size).astype(np.int)
                img_sum = tf.compat.v1.Summary.Image(encoded_image_string=s.getvalue(), height=img_size[0], width=img_size[1])
                summary_img = tf.compat.v1.Summary(value= [tf.compat.v1.Summary.Value(tag='Validation ROC', image=img_sum)] )
                self.writer.add_summary(summary_img, iterNum)



        time.sleep(refreshInterval)
        return pauc




if __name__=='__main__':



    parser = argparse.ArgumentParser(description='Auto Tester for FRCNN')
    parser.add_argument('-i', dest='inputRoot', help='Path to the NFS experiment folder', required=True)
    # example: -i /nfs/experiments/CMA-2591-Shashank-FRCNN/FRCNN-Mass-3-way/
    parser.add_argument('-g', dest='gpu_num', help='GPU index', required=True, type=int)
    parser.add_argument('-t', dest='gt_type', help='mass or calcs', required=False, default='mass')
    parser.add_argument('-n', dest='num_classes', help='number of classes', required=False, default=3, type=int)
    parser.add_argument('-m', dest='model_pattern', help='Model name pattern', default='CM-FRCNN.ckpt-*.index')
    args = parser.parse_args()

    tb_path = args.inputRoot
    model_path = os.path.join(args.inputRoot, 'Models')
    model_paths = [model_path]
    #model_paths = ['/nfs/experiments/CMA-2591-Shashank-FRCNN/FRCNN-Mass-3-way-exp3/Models/',
    #               '/mnt/Array/share/users/jquinn/jack/FRCNN-NoGT/Models/' ]

    auto_testers = []

    for p in model_paths:
        auto_testers.append(AutoTester(p, pattern=args.model_pattern, tensorboardPath=tb_path, 
                                       gpu_num=args.gpu_num, gt_type=args.gt_type, num_classes=args.num_classes))

    while True:
        # cycle through each auto tester and give it a chance to process one checkpoint
        for at in auto_testers:
            at.refresh()


