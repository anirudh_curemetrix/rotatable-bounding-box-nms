import tensorflow as tf
import numpy as np
from config import Config
from cmdataset import CMDataset
from frcnn_model import FRCNN
import logging
logging.basicConfig(level=logging.INFO)
import argparse,pickle
import os,gc
import pdb
from tensorflow.keras.mixed_precision import experimental as mixed_precision

parser = argparse.ArgumentParser(description='FRCNN training code')
parser.add_argument('-i',  '--exp_path', dest='exp_path', help='Path to the folder containing the experiment files', required=True)
args = parser.parse_args()

config = Config(exp_dir=args.exp_path)
config.display()

# GPU config
#tfconfig = tf.compat.v1.ConfigProto(allow_soft_placement=True)
#tfconfig.gpu_options.allow_growth = True
#tfconfig.gpu_options.per_process_gpu_memory_fraction = 3

#gpus = tf.config.experimental.list_physical_devices('GPU')
#for gpu in gpus:
#    tf.config.experimental.set_memory_growth(gpu, True)
#    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
#    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

# Mixed precision
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_policy(policy)
print('Compute dtype: %s' % policy.compute_dtype)
print('Variable dtype: %s' % policy.variable_dtype)

# Assume a batch size of 1 for now
assert config.BATCH_SIZE==1

# Random seeds
np.random.seed(config.SEED)
tf.random.set_seed(config.SEED)

# Define Dataset
dataset = CMDataset(config.CLASSES)
dataset.load_data(config.DATASET, config)
dataset.build_tf_dataset(config)

# Setup directory and writer for Tensorboard summaries
if not os.path.exists(config.TENSORBOARD_DIR):
    os.makedirs(config.TENSORBOARD_DIR)

#Creating tensorboard writer 
logging.info('Tensorboard summaries and checkpoints will be saved in {}'
                       .format(config.TENSORBOARD_DIR))
writer = tf.summary.create_file_writer(config.TENSORBOARD_DIR)

# Define Model
model = FRCNN(config=config, writer=writer,training=True)

# Directory for Model Weights
modelDir = args.exp_path+"/Models/"
if not os.path.exists(modelDir):
    os.mkdir(modelDir)

# =========  Training  ===============

lr = tf.keras.optimizers.schedules.ExponentialDecay(config.LEARNING_RATE,
                                                    config.LR_DECAY_STEP,
                                                    config.LR_DECAY_RATE,
                                                    staircase=True)                                                    
    
optimizer = tf.keras.optimizers.SGD(learning_rate=lr,
                                    momentum=config.MOMENTUM,
                                    nesterov=False)
#                                    nesterov=True) # New17,19

# Need this for mixed precision to keep loss scaled
#optimizer = mixed_precision.LossScaleOptimizer(optimizer, loss_scale='dynamic')
optimizer = mixed_precision.LossScaleOptimizer(optimizer, loss_scale=1024)   # New17
#optimizer = mixed_precision.LossScaleOptimizer(optimizer, loss_scale=32)  # New19

start = 1501
logging.info('Starting training at itr: {}'.format(start))

model(dataset,iteration=0,training=True)
print("--- Loading Weights ---")
#pdb.set_trace()
#model.load_weights("/nfs/experiments/CMA-3D-cmNN/Port/Test1J/Models/Weights20000.h5")
#model.load_weights("/nfs/experiments/CMA-3D-cmNN/Port/Downsample/NativeB16/Models/Weights45000")
model.load_weights("/nfs/experiments/CMA-3D-cmNN/Port/Rotate/Models/Weights1500")

# ---- Set Weights for Block 4 from ResNet101 ----
#input_shape = (config.IMG_HEIGHT, config.IMG_WIDTH,3)
#resnet101 = tf.keras.applications.ResNet101(input_shape=input_shape,
#                                            include_top=False, weights='imagenet')

if False:    
    print(" Loading ImageNet Weights for Block4")
    with open("Block4_Weights.pkl","rb") as fp:
        weights = pickle.load(fp)

    for layer in model.submodules:
        if layer.name.split("_")[0]=="conv5":
            layer.set_weights(weights[layer.name])

#del weights

#----------------------------------------
#tf.profiler.experimental.start(config.TENSORBOARD_DIR)
for itr in range(start,config.NUM_ITRS):
    
    with tf.GradientTape() as tape:

        rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss, rcnn_bbox_loss = model(dataset, iteration=itr, training=True)
        
        logging.info('Itr: {}  \nrpn_cls_loss: {:.6f} rpn_bbox_loss: {:.6f}'
                     ' rcnn_cls_loss: {:.6f} rcnn_bbox_loss: {:.6f}'
                     .format(itr, rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss, rcnn_bbox_loss))

        regularization_loss = tf.math.add_n(model.losses)
        total_loss = rpn_cls_loss + rpn_bbox_loss + rcnn_cls_loss + rcnn_bbox_loss + regularization_loss
        scaled_loss = optimizer.get_scaled_loss(total_loss)
        
    scaled_gradients = tape.gradient(scaled_loss, model.trainable_variables)
    gradients = optimizer.get_unscaled_gradients(scaled_gradients)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))

    logging.info('Total Loss = {:0.6f}'.format(total_loss))
        
    if (itr%config.SUMM_STEP == 0 and itr>0):
        model.save_weights(modelDir+"Weights"+str(itr),save_format="tf")
        gc.collect()
        
   #if itr%config.SUMM_STEP == 0:
    with writer.as_default():
        tf.summary.scalar('rpn_class_loss', rpn_cls_loss,step=itr)
        tf.summary.scalar('rpn_bbox_loss', rpn_bbox_loss,step=itr)
        tf.summary.scalar('rcnn_class_loss', rcnn_cls_loss,step=itr)
        tf.summary.scalar('rcnn_bbox_loss', rcnn_bbox_loss,step=itr)
        tf.summary.scalar('Total Loss', total_loss,step=itr)

#tf.profiler.experimental.stop()
