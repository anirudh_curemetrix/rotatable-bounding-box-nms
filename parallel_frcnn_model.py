from __future__ import division

import os
import utils
import logging
logging.basicConfig(level=logging.INFO)
import numpy as np
import tensorflow as tf

from tensorflow.contrib import slim
from feature_extractor import resnet101
from modules import rpn, ROIAlign
from modules import rcnn_head, rcnn_detections_graph
from modules import generate_anchors, get_proposals
from modules import rpn_targets_graph, rcnn_targets_graph
from losses import rpn_class_loss, rpn_bbox_loss
from losses import rcnn_class_loss, rcnn_bbox_loss


class FRCNN():

    def __init__(self, config):
        self.config = config

        height, width = self.config.IMAGE_SHAPE[:2]
        self.box_norm_coords = np.array([[height, width, height, width]])

    def build_parallel_training_graph(self, sess):

        with tf.device('/cpu:0'):
            self.sess = sess
            self.global_step = tf.Variable(0, name='global_step', trainable=False)
            lr = tf.compat.v1.train.exponential_decay(self.config.LEARNING_RATE,
                                        self.global_step,
                                        self.config.LR_DECAY_STEP,
                                        self.config.LR_DECAY_RATE,
                                        staircase=True)
            optimizer = tf.compat.v1.train.MomentumOptimizer(
                            learning_rate=lr,
                            momentum=self.config.MOMENTUM)
            update_ops = tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.UPDATE_OPS)

            # inputs to the network
            self.imgs = tf.compat.v1.placeholder(tf.float32,
                                      shape=[self.config.EFFECTIVE_BATCH_SIZE,
                                             self.config.IMG_HEIGHT,
                                             self.config.IMG_WIDTH,
                                             3],
                                      name='images')
            # absolute coordinates
            self.gt_bboxes = tf.compat.v1.placeholder(tf.float32,
                                            shape=[self.config.EFFECTIVE_BATCH_SIZE,
                                                   self.config.MAX_GT_INSTANCES,
                                                   4],
                                            name='gt_bboxes')
            self.gt_cls = tf.compat.v1.placeholder(tf.float32,
                                         shape=[self.config.EFFECTIVE_BATCH_SIZE,
                                                self.config.MAX_GT_INSTANCES],
                                         name='gt_cls')

            tower_grads = []
            imgs = tf.split(self.imgs, self.config.NUM_GPUS)
            gt_bboxes = tf.split(self.gt_bboxes, self.config.NUM_GPUS)
            gt_cls = tf.split(self.gt_cls, self.config.NUM_GPUS)
            with tf.compat.v1.variable_scope(tf.compat.v1.get_variable_scope()):
                for i in range(self.config.NUM_GPUS):
                    with tf.device('/gpu:%d'%i):
                        with tf.compat.v1.name_scope('tower_%d'%i) as scope:
                            rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss,\
                            rcnn_bbox_loss, total_loss = self.build_training_graph(
                                                    imgs[i], gt_bboxes[i], gt_cls[i])

                            tf.compat.v1.get_variable_scope().reuse_variables()

                            grads = optimizer.compute_gradients(total_loss)
                            tower_grads.append(grads)

            self.saver = tf.compat.v1.train.Saver(max_to_keep=self.config.MAX_CKPTS,
                                        keep_checkpoint_every_n_hours=2)
            self.rpn_cls_loss = rpn_cls_loss
            self.rpn_bbox_loss = rpn_bbox_loss
            self.rcnn_cls_loss = rcnn_cls_loss
            self.rcnn_bbox_loss = rcnn_bbox_loss
            self._create_summ_ops()

            grads = self.average_gradients(tower_grads)
            with tf.control_dependencies(update_ops):
                self.train_op = optimizer.apply_gradients(grads,
                                                global_step=self.global_step)


# Source: https://github.com/tensorflow/models/blob/master/tutorials/ \
#        image/cifar10/cifar10_multi_gpu_train.py"""
    def average_gradients(self, tower_grads):
        """Calculate the average gradient for each shared variable across all towers.
        Note that this function provides a synchronization point across all towers.
        Args:
        tower_grads: List of lists of (gradient, variable) tuples. The outer list
          is over individual gradients. The inner list is over the gradient
          calculation for each tower.
        Returns:
         List of pairs of (gradient, variable) where the gradient has been averaged
         across all towers.
        """
        average_grads = []
        for grad_and_vars in zip(*tower_grads):
            # Note that each grad_and_vars looks like the following:
            #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
            grads = []
            for g, _ in grad_and_vars:
                # Add 0 dimension to the gradients to represent the tower.
                expanded_g = tf.expand_dims(g, 0)

                # Appennd on a 'tower' dimension which we will average over below.
                grads.append(expanded_g)

            # Average over the 'tower' dimension.
            grad = tf.concat(axis=0, values=grads)
            grad = tf.reduce_mean(input_tensor=grad, axis=0)

            # Keep in mind that the Variables are redundant because they are shared
            # across towers. So .. we will just return the first tower's pointer to
            # the Variable.
            v = grad_and_vars[0][1]
            grad_and_var = (grad, v)
            average_grads.append(grad_and_var)
        return average_grads


    def build_training_graph(self, imgs, gt_bboxes, gt_cls):


        # extract feature maps
        C4 = resnet101(imgs, self.config, is_training=True)

        # Generate Anchors(absolute coordinates)
        self.anchors = generate_anchors(self.config.RPN_ANCHOR_SCALES,
                                        self.config.RPN_ANCHOR_RATIOS,
                                        self.config.FEATURE_SHAPE,
                                        self.config.FEATURE_STRIDE,
                                        self.config.RPN_ANCHOR_STRIDE)
        logging.info('Total number of anchors: {}'.format(self.anchors.shape[0]))

        # rpn predictions
        num_anchors_per_loc = len(self.config.RPN_ANCHOR_SCALES)*\
                                    len(self.config.RPN_ANCHOR_RATIOS)

        with slim.arg_scope([slim.conv2d, slim.fully_connected],
                weights_regularizer=tf.keras.regularizers.l2(0.5 * (self.config.WEIGHT_DECAY)),
                biases_regularizer=tf.compat.v1.no_regularizer,
                biases_initializer=tf.compat.v1.constant_initializer(0.0)):

            rpn_cls, rpn_bbox_deltas = rpn(C4, anchor_per_loc=num_anchors_per_loc,
                                anchor_stride=self.config.RPN_ANCHOR_STRIDE)

        ############## RPN targets and losses #################
        rpn_cls_gt, rpn_bbox_deltas_gt = rpn_targets_graph(self.anchors,
                                                    gt_cls,
                                                    gt_bboxes,
                                                    self.config)

        # losses are added to loss collections
        rpn_cls_loss = rpn_class_loss(rpn_cls, rpn_cls_gt)
        rpn_bboxes_loss = rpn_bbox_loss(rpn_bbox_deltas, rpn_cls_gt,
                                            rpn_bbox_deltas_gt, self.config)
        #######################################################

        if self.config.DEBUG:
            # display positive anchors
            cls_targets = tf.reshape(rpn_cls_gt, (-1,))
            active = tf.compat.v1.where(tf.equal(cls_targets, 1))

            pos_anchors = tf.reshape(self.anchors, (-1, 4))
            pos_anchors = tf.gather_nd(pos_anchors, active)

            boxes = tf.expand_dims(pos_anchors, axis=0)

            utils.image_with_bbox_summ(imgs, boxes,'pos_anchors',
                                    'mass', self.config)


        # select a subset of ROIs, apply refinement and nms
        # proposal boxes are in normalized coordinates
        self.proposed_rois = get_proposals(rpn_cls,
                                      rpn_bbox_deltas,
                                      self.anchors,
                                      self.config,
                                      mode='training')


        ################ RCNN targets ##############
        # select training rois for RCNN and label them
        gt_bboxes_normalized = self._to_normalized_boxes(gt_bboxes)
        rois, roi_cls_gt, roi_bbox_delta_gt = rcnn_targets_graph(
                                                    self.proposed_rois,
                                                    gt_cls,
                                                    gt_bboxes_normalized,
                                                    self.config)

        # display top rpn proposals
        disp_rois = self.proposed_rois[:,:10,:]
        disp_rois = self._from_normalized_boxes(disp_rois)
        utils.image_with_bbox_summ(imgs, disp_rois,'top_rpn_rois',
                                'mass', self.config)



        # shape [batch_size, num_proposals, w, h, c]
        pooled_rois = ROIAlign(rois, C4, self.config)

        # here batch_size and num_proposals are collapsed into one dimension
        with slim.arg_scope([slim.conv2d, slim.fully_connected],
                weights_regularizer=tf.keras.regularizers.l2(0.5 * (self.config.WEIGHT_DECAY)),
                biases_regularizer=tf.compat.v1.no_regularizer,
                biases_initializer=tf.compat.v1.constant_initializer(0.0)):

            rcnn_cls_logits, rcnn_bbox_deltas = rcnn_head(pooled_rois, self.config)


        # collapse target's dimensions too
        roi_cls_gt = tf.cast(tf.reshape(roi_cls_gt, (-1,)), tf.int32)
        roi_bbox_delta_gt = tf.reshape(roi_bbox_delta_gt, (-1, 4))


        if  self.config.DEBUG:
            # display RCNN training mass rois
            active = tf.compat.v1.where(tf.equal(roi_cls_gt, 2))

            pos_rois = tf.reshape(rois, (-1, 4))
            pos_rois = tf.gather_nd(pos_rois, active)
            pos_rois = tf.expand_dims(pos_rois, axis=0)
            pos_boxes = self._from_normalized_boxes(pos_rois)

            utils.image_with_bbox_summ(imgs, pos_boxes,'xrcnn_mass_rois',
                                    'mass', self.config)

            active = tf.compat.v1.where(tf.equal(roi_cls_gt, 1))

            pos_rois = tf.reshape(rois, (-1, 4))
            pos_rois = tf.gather_nd(pos_rois, active)
            pos_rois = tf.expand_dims(pos_rois, axis=0)
            pos_boxes = self._from_normalized_boxes(pos_rois)

            utils.image_with_bbox_summ(imgs, pos_boxes,'xrcnn_mass_normal_rois',
                                    'mass_normal', self.config)

        if self.config.OHEM:
            rcnn_cls_losses = rcnn_class_loss(rcnn_cls_logits, roi_cls_gt,
                                              self.config)
            rcnn_bbox_losses = rcnn_bbox_loss(rcnn_bbox_deltas, roi_cls_gt,
                                             roi_bbox_delta_gt, self.config)

            self.rcnn_cls_loss = tf.reduce_mean(input_tensor=rcnn_cls_losses)
            self.rcnn_bbox_loss = tf.reduce_mean(input_tensor=rcnn_bbox_losses)

            self.rcnn_loss = ohem_loss(rois, rcnn_cls_losses, rcnn_bbox_losses,
                                            self.config)
        else:
            self.rcnn_cls_loss = rcnn_class_loss(rcnn_cls_logits, roi_cls_gt,
                                             self.config)
            self.rcnn_bbox_loss = rcnn_bbox_loss(rcnn_bbox_deltas, roi_cls_gt,
                                             roi_bbox_delta_gt, self.config)


        ######################################################

        # display rcnn detections
        pred_cls_probs = tf.nn.softmax(rcnn_cls_logits, axis=-1)
        pred_cls_probs = tf.reshape(pred_cls_probs, (self.config.BATCH_SIZE,
            self.config.NUM_RCNN_TRAIN_ROIS, self.config.NUM_CLASSES))
        pred_bboxes = tf.reshape(rcnn_bbox_deltas, (self.config.BATCH_SIZE,
            self.config.NUM_RCNN_TRAIN_ROIS, self.config.NUM_CLASSES, 4))

        self.final_rois, self.final_scores = rcnn_detections_graph(rois,
                                    pred_cls_probs, pred_bboxes, self.config)

        top_rois = self.final_rois[:,:5,:]
        top_scores = self.final_scores[:,:5]
        utils.image_with_bbox_summ_with_score(imgs, top_rois, top_scores,
                                    'xtop_rcnn_rois', 'mass', self.config)

        #################### Total losses ##############################

        total_loss = tf.compat.v1.losses.get_total_loss(
                    add_regularization_losses=self.config.USE_REGULARIZATION)


        return rpn_cls_loss, rpn_bboxes_loss, rcnn_cls_loss, rcnn_bboxes_loss, total_loss



    def train(self, data_generator):

        with tf.device('/cpu:0'):
            self.initialize_model()
            start = self.global_step.eval(session=self.sess)
            logging.info('Starting training at itr: {}'.format(start))

            for itr in range(start, self.config.NUM_ITRS):
                imgs, bboxes, cls_ids = data_generator.next()
                if self.config.OHEM:
                    rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss, rcnn_bbox_loss, \
                    rcnn_loss, _ = self.sess.run(
                                [self.rpn_cls_loss, self.rpn_bbox_loss,
                                self.rcnn_cls_loss, self.rcnn_bbox_loss, self.rcnn_loss,
                                self.train_op],
                                feed_dict={self.imgs: imgs,
                                self.gt_bboxes: bboxes,
                                self.gt_cls: cls_ids})
                    logging.info('Itr: {}  \nrpn_cls_loss: {:.6f} rpn_bbox_loss: {:.6f}'
                                        ' rcnn_cls_loss: {:.6f} rcnn_bbox_loss: {:.6f}'
                                        ' rcnn_loss: {:.6f}'
                        .format(itr, rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss,
                            rcnn_bbox_loss, rcnn_loss))
                else:
                    rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss, rcnn_bbox_loss, _ = self.sess.run(
                                [self.rpn_cls_loss, self.rpn_bbox_loss,
                                    self.rcnn_cls_loss, self.rcnn_bbox_loss, self.train_op],
                                feed_dict={self.imgs: imgs,
                                self.gt_bboxes: bboxes,
                                self.gt_cls: cls_ids})
                    logging.info('Itr: {}  \nrpn_cls_loss: {:.6f} rpn_bbox_loss: {:.6f}'
                                        ' rcnn_cls_loss: {:.6f} rcnn_bbox_loss: {:.6f}'
                        .format(itr, rpn_cls_loss, rpn_bbox_loss, rcnn_cls_loss,
                            rcnn_bbox_loss))

                if itr%self.config.SUMM_STEP == 0:
                    summ = self.sess.run(self.summ_op,
                            feed_dict={self.imgs: imgs,
                                       self.gt_bboxes: bboxes,
                                       self.gt_cls: cls_ids})
                    self.writer.add_summary(summ, itr)
                    self._save_model(itr)



    def initialize_model(self):
        self.sess.run(tf.compat.v1.global_variables_initializer())
        if self.config.RESTORE:
            logging.info('Restoring Model...')
# Bill            
            tf.train.load_checkpoint(self.config.MODEL_DIR)
#            ckpt = tf.train.get_checkpoint_state(self.config.MODEL_DIR)
#            tf.contrib.framework.assign_from_checkpoint_fn(
#                    ckpt.model_checkpoint_path,
#                    slim.get_model_variables()+[self.global_step])(self.sess)
        else:
            logging.info('Initializing resnet101 by imagenet weights...')
# Bill                        
            tf.train.load_checkpoint(self.config.IMAGENET_WEIGHTS)
#            tf.contrib.framework.assign_from_checkpoint_fn(
#                        self.config.IMAGENET_WEIGHTS,
#                        slim.get_model_variables('resnet_v1_101'))(self.sess)

    def _save_model(self, count):
        if not os.path.exists(self.config.MODEL_DIR):
            os.makedirs(self.config.MODEL_DIR)

        logging.info('Saving model...')
        self.saver.save(self.sess, self.config.MODEL_DIR+'/{}.ckpt'
                                    .format(self.config.MODEL_NAME),
                        global_step=int(count))

    def load_model(self, sess, ckpt=None):
        """Loads trained weights for inference
        Inputs:
            - sess: tf session
            - ckpt: checkpoint to be loaded, if None last checkpoint from
                    model directory is loaded"""

        logging.info('Loading Model...')
        saver = tf.compat.v1.train.Saver()
        if ckpt is None:
            ckpt = tf.train.get_checkpoint_state(self.config.MODEL_DIR)
            ckpt = ckpt.model_checkpoint_path
        saver.restore(sess, ckpt)

    def _create_summ_ops(self):
        if not os.path.exists(self.config.TENSORBOARD_DIR):
            os.makedirs(self.config.TENSORBOARD_DIR)

        logging.info('Tensorboard summaries and checkpoints will be saved in {}'
                       .format(self.config.TENSORBOARD_DIR))

        self.writer = tf.compat.v1.summary.FileWriter(self.config.TENSORBOARD_DIR,
                                            self.sess.graph)
        if self.config.IMG_SUMMARY:
            height, width = self.config.IMAGE_SHAPE[:2]
            # shape = tf.shape(self.imgs)[1:3]
            # imgs = tf.image.resize_nearest_neighbor(self.imgs, shape//4)
            utils.image_with_bbox_summ(self.imgs, self.gt_bboxes,'gt_bbox',
                                    'mass', self.config)

        tf.compat.v1.summary.scalar('rpn_class_loss', self.rpn_cls_loss)
        tf.compat.v1.summary.scalar('rpn_bbox_loss', self.rpn_bbox_loss)
        tf.compat.v1.summary.scalar('rcnn_class_loss', self.rcnn_cls_loss)
        tf.compat.v1.summary.scalar('rcnn_bbox_loss', self.rcnn_bbox_loss)

        # Create summaries to visualize weights
        for var in tf.compat.v1.trainable_variables():
            tf.compat.v1.summary.histogram(var.name[:-2], var)
        # Summarize all gradients
        # for grad, var in grads:
            # tf.summary.histogram(var.name + '/gradient', grad)

        self.summ_op = tf.compat.v1.summary.merge_all()


    def build_inference_graph(self):
        # inputs to the network
        self.imgs = tf.compat.v1.placeholder(tf.float32,
                                  shape=[self.config.BATCH_SIZE,
                                         self.config.IMG_HEIGHT,
                                         self.config.IMG_WIDTH,
                                         3],
                                  name='images')

        # extract feature maps
        C4 = resnet101(self.imgs, self.config, False)

        # Generate Anchors
        self.anchors = generate_anchors(self.config.RPN_ANCHOR_SCALES,
                                        self.config.RPN_ANCHOR_RATIOS,
                                        self.config.FEATURE_SHAPE,
                                        self.config.FEATURE_STRIDE,
                                        self.config.RPN_ANCHOR_STRIDE)
        logging.info('Total number of anchors: {}'.format(self.anchors.shape[0]))

        # rpn predictions
        num_anchors_per_loc = len(self.config.RPN_ANCHOR_SCALES)*\
                                    len(self.config.RPN_ANCHOR_RATIOS)

        rpn_cls, rpn_bbox_deltas = rpn(C4, anchor_per_loc=num_anchors_per_loc,
                                anchor_stride=self.config.RPN_ANCHOR_STRIDE)

        #######################################################

        # select a subset of ROIs, apply refinement and nms
        # proposal boxes are in normalized coordinates
        proposed_rois = get_proposals(rpn_cls,
                                      rpn_bbox_deltas,
                                      self.anchors,
                                      self.config,
                                      mode='inference')

        # shape [batch_size, num_proposals, w, h, c]
        pooled_rois = ROIAlign(proposed_rois, C4, self.config)

        # here batch_size and num_proposals are collapsed into one dimension
        rcnn_cls_logits, rcnn_bbox_deltas = rcnn_head(pooled_rois, self.config,
                                            is_training=False)

        pred_cls_probs = tf.nn.softmax(rcnn_cls_logits, axis=-1)
        pred_cls_probs = tf.reshape(pred_cls_probs, (self.config.BATCH_SIZE,
            self.config.RPN_TEST_PROPOSAL_COUNT, self.config.NUM_CLASSES))
        pred_bboxes = tf.reshape(rcnn_bbox_deltas, (self.config.BATCH_SIZE,
            self.config.RPN_TEST_PROPOSAL_COUNT, self.config.NUM_CLASSES, 4))

        # shape [batch_size, num_rois, 4] and [batch_size, num_rois, 1]
        # returns rois of last class only
        self.final_rois, self.final_scores = rcnn_detections_graph(proposed_rois,
                                    pred_cls_probs, pred_bboxes, self.config)

    def predict(self, sess, images):
        """Predicts bounding boxes of the last calss for a batch of images
        Inputs:
            - images: numpy array of shape [batch_size, W, H, 3]
        Outputs:
            - rois: numpy array of shape [batch_size, num_rois, 4]
            - scores: numpy array of shape [batch_size, num_rois, 1]
        """
        rois, scores = sess.run([self.final_rois, self.final_scores],
                            feed_dict={self.imgs: images})
        return rois, scores


    def _to_normalized_boxes(self, boxes):
        """Converts boxes to normalized coordinates
        Inputs:
            - boxes: [batch_size, num_rois, 4]
        Outputs:
            - boxes: [batch_size, num_rois, 4]
        """
        return boxes/self.box_norm_coords

    def _from_normalized_boxes(self, boxes):
        """Converts boxes to absolute coordinates
        Inputs:
            - boxes: [batch_size, num_rois, 4]
        Outputs:
            - boxes: [batch_size, num_rois, 4]
        """
        return boxes*self.box_norm_coords
