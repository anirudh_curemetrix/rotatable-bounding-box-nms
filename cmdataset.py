
import os,sys
import logging
logging.basicConfig(level=logging.INFO)
import xmltodict
import numpy as np
import skimage.io as io
import tensorflow as tf
from data_augmentation import DataAugmentor


import pdb

class CMDataset():
    """
    Dataset class

    Attributes:
    images_info: list of all the images with infos
    image_ids: np array of image ids
    classes: list of all the classes in the dataset
    """

    def __init__(self, classes):

        # a list of all the training images
        # - maps imageid to path, shape, bboxes
        self.images_info = []
        self.classes = ['BG']
        self.classes.extend(classes)

    def _add_image(self, image_id, path, objs, width, height):
        self.images_info.append({'image_id': image_id,
                                'path': path,
                                'objs': objs,
                                'width': width,
                                'height': height})

#    @tf.function
    def load_data(self, dataset_dir, config):

        logging.info('Parsing data...')
        #split_dir = os.path.join(dataset_dir, 'ImageSets', 'Main')
        #annotation_dir = os.path.join(dataset_dir, 'Annotations')
        #image_dir = os.path.join(dataset_dir, "PNGImages")

        # dataset_dir should point to the root NFS experiment folder
        # which contains the train.txt file

        self.dataset_dir = dataset_dir
        self.config = config
        # Load images data
        dataset_txt = os.path.join(dataset_dir,"train.txt")

        for ith, line in enumerate(open(dataset_txt)):

            line = line.strip()
            png_path = os.path.join(config.DATA_ROOT, line + '.png')
            xml_path = png_path.replace('images/PNGs', config.PNG_TO_XML_REPLACE)
            xml_path = xml_path.replace('.png', '.xml')

            if not os.path.exists(png_path):
                print(png_path)
            if not os.path.exists(xml_path):
                print(xml_path)

            assert os.path.exists(png_path)
            assert os.path.exists(xml_path)

            with open(xml_path) as f:
                anno = xmltodict.parse(f.read())
                if 'object' not in anno['annotation']:
                    objs = []   # JCQ change- allow images with no GT
                else:
                    objs = anno['annotation']['object']
                if not isinstance(objs, list):
                    objs = [objs]
                self._add_image(
                    image_id=ith,
                    path=png_path,
                    objs=objs,
                    width=anno['annotation']["size"]['width'],
                    height=anno['annotation']["size"]['height'])

        self.num_images = len(self.images_info)
        self.image_ids = np.arange(self.num_images)
        logging.info('{} images found'.format(self.num_images))
        logging.info('Classes: {}'.format(self.classes))

        if config.ONLINE_DATA_AUGMENTATION:
            self.augmentor = DataAugmentor(config.IMG_HEIGHT, config.IMG_WIDTH,
                                config_yaml=os.path.join(dataset_dir, 'data_augmentation.yaml') )
        else:
            self.augmentor = None



    def build_tf_dataset(self, config):
        image_list = [x['path'] for x in self.images_info]
        self.num_images = len(image_list)

        # pack all the bboxes and class ids into matrices
        bboxes  = np.zeros((self.num_images, config.MAX_GT_INSTANCES, 5), dtype=np.float32)
        cls_ids = np.zeros((self.num_images, config.MAX_GT_INSTANCES), dtype=np.float32)

        for i_image, info in enumerate(self.images_info):
            i_gt = 0
            for obj in info['objs']:
                if obj['name'] in self.classes and i_gt < config.MAX_GT_INSTANCES :
                    bb = obj['bndbox']
                    cls_ids[i_image, i_gt] = self.classes.index(obj['name'])
#                    bb_array = np.array( [bb['ymin'], bb['xmin'], bb['ymax'], bb['xmax'], bb['angle'] ], dtype=np.float32)
#                    bboxes[i_image,  i_gt, :] =  bb_array
                      
                    cx = (float(bb['xmax'])+float(bb['xmin']))/2.0
                    dx = (float(bb['xmax'])-float(bb['xmin']))
                    cy = (float(bb['ymax'])+float(bb['ymin']))/2.0
                    dy = (float(bb['ymax'])-float(bb['ymin']))
                    bboxes[i_image,  i_gt, :] =  np.array( [cy,cx,dy,dx,float(bb['angle'])] )

                    # subtract one to convert to 0-based coordinates
                    bboxes[i_image,  i_gt, 0:4] = bboxes[i_image,  i_gt, 0:4]  - 1.0
                    i_gt += 1 

        tf_dataset = tf.data.Dataset.from_tensor_slices((image_list, bboxes, cls_ids)) \
                                    .shuffle(self.num_images).repeat()

        tf_dataset = tf_dataset.map(self.image_reader)

        
        self.iterator = iter(tf_dataset)

        self.tf_dataset = tf_dataset


    def image_reader(self, img_path, bboxes, cls_ids):
        
        with tf.device('/cpu'):
            tf.print(img_path, output_stream=sys.stdout)
            img_file = tf.io.read_file(img_path)
            img = tf.image.decode_image(img_file, channels=3)

            if self.config.ONLINE_DATA_AUGMENTATION:
                img = tf.cast(img, tf.float64)
                img, bboxes = tf.py_function(self.augmentor.doAugmentation, [img, bboxes], [tf.float64, tf.float32])

            img = tf.cast(img, tf.float32)

            if self.config.HORIZONTAL_FLIPPING:
                img, bboxes = tf.py_function(self.maybe_flip, [img, bboxes], [tf.float32, tf.float32])

            if self.config.VERTICAL_FLIPPING:
                img, bboxes = tf.py_function(self.maybe_vflip, [img, bboxes], [tf.float32, tf.float32])

            img = tf.keras.applications.resnet.preprocess_input(img)
            
        return img, bboxes, cls_ids

    def maybe_flip(self, img, bboxes):
        """ 
        flip image and GT left/right with 50% probability 
        """
        if np.random.randint(0,2):
            img = np.fliplr(img.numpy())
            bboxes = bboxes.numpy()
            tmp_xmax = bboxes[:,3].copy()
            # number of non-placeholder GTs
            numGT = sum([np.any(bboxes[i,:]) for i in range(bboxes.shape[0])])
            if numGT > 0:
                bboxes[:numGT,3] = self.config.IMG_WIDTH - bboxes[:numGT,1]
                bboxes[:numGT,1] = self.config.IMG_WIDTH - tmp_xmax[:numGT]
        return img, bboxes

    def maybe_vflip(self, img, bboxes):
        """ 
        flip image and GT Top/Bottom with 50% probability 
        """
        if np.random.randint(0,2):
            img = np.flipud(img.numpy())
            bboxes = bboxes.numpy()
            tmp_ymax = bboxes[:,2].copy()
            # number of non-placeholder GTs
            numGT = sum([np.any(bboxes[i,:]) for i in range(bboxes.shape[0])])
            if numGT > 0:
                bboxes[:numGT,2] = self.config.IMG_HEIGHT - bboxes[:numGT,0]
                bboxes[:numGT,0] = self.config.IMG_HEIGHT - tmp_ymax[:numGT]
        return img, bboxes
