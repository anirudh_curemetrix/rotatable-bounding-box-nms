"""
Faster R-CNN
Configurations class.

"""

import math
import numpy as np
import os
import logging
import yaml 

class Config():
    DEBUG = False
    SEED = 42

    DATA_ROOT = '/nfs'
    MODEL_NAME = 'CM-FRCNN'
#    MODEL_DIR = './outputs/'
    MODEL_DIR = '/nfs/experiments/CMA-3D-cmNN/Focal-Loss/Exp25-focal20-RGB-V2/Models/'
    TENSORBOARD_DIR = './tensorboard/'
    IMG_SUMMARY = False  # there may be a problem if this is true when there are images with no GT
    SUMM_STEP = 1000 # also the stepsize for checkpoints
    MAX_CKPTS = 100

    NUM_ITRS = 120000
    RESTORE = False
    IMAGENET_WEIGHTS = '/nfs/imagenet_weights/resnet_v1_101.ckpt'
    # Learning rate and momentum
    LEARNING_RATE = 0.001
    MOMENTUM = 0.9
    LR_DECAY_STEP = 50000
    LR_DECAY_RATE = 0.1

    # Weight decay regularization(l2 regularization only)
    USE_REGULARIZATION = True
    WEIGHT_DECAY = 0.0001

    # replace 'images/PNGs' in the image names with this string to locate the XMLs
    PNG_TO_XML_REPLACE = 'GT/XMLs'

    PARALLEL = False
    # Changing the number of GPUs changes the batch size too. Thus, learning rate
    # also needs to be adjusted. A linear scaling with the number of GPUs works
    # fine https://arxiv.org/abs/1706.02677
    NUM_GPUS = 1

    BATCH_SIZE = 1 #batch_size on single GPU
    EFFECTIVE_BATCH_SIZE = BATCH_SIZE*NUM_GPUS
    DATASET = '/home/jquinn/Desktop/jquinn/jack/CMA-1994-mass-3way-FRCNN/tf-faster-rcnn/data/VOCdevkit2007/VOC2007/'

    NUM_CLASSES = 1 + 2 # +1 for background
    CLASSES = ['mass_normal', 'mass'] # only non-background classes
    # no resize is done in the done
    # make sure the image sizes are correct
#    IMG_HEIGHT = 1024
#    IMG_WIDTH = 800
    IMG_HEIGHT = 2048
    IMG_WIDTH = 1600
    
    # Image mean (RGB)
    MEAN_PIXEL = np.array([127.5, 127.5, 127.5])
#    MEAN_PIXEL = np.array([123.68, 116.28, 103.53])

    # Maximum number of ground truth instances to use in one image
    MAX_GT_INSTANCES = 10

    # Data augmentation
    ONLINE_DATA_AUGMENTATION = False
    NUM_DA_THREADS = 4
    HORIZONTAL_FLIPPING = False
    VERTICAL_FLIPPING = False
    
    # The strides of block3 of resnet 101.
    RESNET_FIXED_BLOCKS = 0
    FEATURE_STRIDE = 16

    ######################## RPN #########################

    # Length of square anchor side in pixels
    RPN_ANCHOR_SCALES = [128, 256, 512]

    # Ratios of anchors at each cell (width/height)
    # A value of 1 represents a square anchor, and 0.5 is a wide anchor
    RPN_ANCHOR_RATIOS = [0.5, 1, 2]

    # Anchor stride
    # If 1 then anchors are created for each cell in the backbone feature map.
    # If 2, then anchors are created for every other cell, and so on.
    RPN_ANCHOR_STRIDE = 1

    # How many anchors per image to use for RPN training
    RPN_TRAIN_ANCHORS_PER_IMAGE = 256
    RPN_FG_RATIO = 0.5 # Not implemented yet(hard coded to 0.5 in the code)

    RPN_TRAIN_PRE_NMS_COUNT = 12000 # selected using the rpn score
    RPN_TRAIN_PROPOSAL_COUNT = 2000

    RPN_NMS_THRESHOLD = 0.7

    RPN_TEST_PRE_NMS_COUNT = 6000
    RPN_TEST_PROPOSAL_COUNT = 300

    # Thresholds for matching anchors to GT Boxes
    # If an anchor overlaps a GT box with IoU >= RPN_POS_THRESHOLD then it's positive.
    # If an anchor overlaps a GT box with IoU <= RPN_NEG_THRESHOLD then it's negative.
    RPN_POS_THRESHOLD = 0.7
    RPN_NEG_THRESHOLD = 0.3

    # Bounding box refinement standard deviation for RPN
    # if all are 1's, it corresponds to no normalization
    RPN_BBOX_STD_DEV = np.array([0.1, 0.1, 0.2, 0.2, 1.0])

    #######################################################

    ######################### RCNN ########################
    # Pooled ROIs
    POOL_SIZE = 7
    USE_ROI_MAXPOOL = True

    RCNN_POS_THRESHOLD = 0.5
    RCNN_NEG_THRESHOLD = 0.5

    NUM_RCNN_TRAIN_ROIS = 256 # if OHEM is True, this value can be in thousands,
                               # otherwise choose a small value eg. 256
    RCNN_TRAIN_FG_RATIO = 0.5 # for ohem this value is moot
    OHEM = False
    OHEM_NMS_COUNT = 256
    OHEM_NMS_THRESHOLD = 0.7
    OHEM_CLS_WEIGHT = 1.0
    OHEM_BBOX_WEIGHT = 1.0

    # Bounding box refinement standard deviation for final detections.
    BBOX_STD_DEV = np.array([0.1, 0.1, 0.2, 0.2, 1.0])

    # Max number of final detections
    RCNN_MAX_DETECTIONS = 300

    # Non-maximum suppression threshold for detection (test)
    DETECTION_NMS_THRESHOLD = 0.3

    #######################################################


    def __init__(self, exp_dir=None):
        """Set values of computed attributes."""
        
        if exp_dir:
            yaml_path = os.path.join(exp_dir, 'config.yaml')
            # optionally override the defaults using the yaml config file
            if os.path.exists(yaml_path):
                logging.info('Overriding defaults using %s' % yaml_path)
                with open(yaml_path) as fp:
                    new_config = yaml.load(fp)
                for k,v in new_config.items():
                    if hasattr(self, k):
                        setattr(self, k, v)

        # override some paths to conform to NFS convention
        if exp_dir:
            self.DATASET = exp_dir
            self.TENSORBOARD_DIR = exp_dir
            self.MODEL_DIR = os.path.join(exp_dir, 'Models')

        # Effective batch size

        # Input image size
        self.IMAGE_SHAPE = np.array(
            [self.IMG_HEIGHT, self.IMG_WIDTH, 3])

        # Compute backbone size from input image size
        self.FEATURE_SHAPE = np.array(
            [int(math.ceil(self.IMAGE_SHAPE[0] / self.FEATURE_STRIDE)),
              int(math.ceil(self.IMAGE_SHAPE[1] / self.FEATURE_STRIDE))])

    def display(self):
        """Display Configuration."""
        logging.info("\nFaster RCNN configuration:")
        for a in dir(self):
            if not a.startswith("__") and not callable(getattr(self, a)):
                logging.info("\t{:30} {}".format(a, getattr(self, a)))
