# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

import numpy as np

def nms(dets, thresh):
    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]
    scores = dets[:, 4]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]

    return keep


"""
nms_FZ matches the matlab nms_FZ function. It is slightly different than the nms above
because for the overlap condition it uses |intersection(Bi, Bi)| / |Bj| where Bi is the current
box and Bj is the other box. 
The other nms function above uses |intersection(Bi,Bj)|/|union(Bi,Bj)|
"""

def nms_FZ(dets, thresh):
    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]
    scores = dets[:, 4]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / areas[order[1:]]

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]

    return np.array(keep)


"""
Starting with the highest scoring box as the current box, compare the current box 
to all other boxes with smaller scores. If any of the other boxes completly surrond the current box, 
remove the other box (remove the one with the smaller score). 
"""

def secondaryPrune(dets):

    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]
    scores = dets[:, 4]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    # sort in decreasing score order
    order = np.argsort(scores)[::-1]
    Nboxes = dets.shape[0]

    lkeep = np.ones(Nboxes, dtype=np.bool)
    for i in range(Nboxes-1):  # loop over current box
        for j in range(i+1, Nboxes): # loop over other boxes
            if lkeep[j] and x1[i] > x1[j] and y1[i] > y1[j] and x2[i] < x2[j] and y2[i] < y2[j]:
                lkeep[j] = False

    return order[lkeep]

