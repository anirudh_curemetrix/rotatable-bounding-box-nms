GPU_ID=$1
EXP_PATH=$2
GIT_REPO=$(basename `git rev-parse --show-toplevel`)
GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
GIT_COMMIT=$(git rev-parse HEAD)

#LOG=${EXP_PATH}"logs-`date +'%Y-%m-%d_%H-%M-%S'`.txt"
#exec &> >(tee -a "$LOG")

export CUDA_VISIBLE_DEVICES=${GPU_ID}
#export TF_FORCE_GPU_ALLOW_GROWTH="true"

#export TF_CPP_MIN_LOG_LEVEL='2'

echo Logging Output to "$LOG"
echo Using GPU: "$CUDA_VISIBLE_DEVICES"
echo Git Repo: "$GIT_REPO"
echo Git Branch: "$GIT_BRANCH"
echo Git Commit: "$GIT_COMMIT"

python train.py --exp_path=${EXP_PATH}
