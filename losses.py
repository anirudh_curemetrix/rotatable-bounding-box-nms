from __future__ import division

import utils
import numpy as np
import tensorflow as tf
import sys
import pdb
losses = tf.keras.losses

def rpn_class_loss(preds, targets):
    """Loss for RPN.

    targets: [batch, num_rois]. Integer class IDs. Uses zero
        padding to fill in the array.
    preds: [batch, num_rois, 2] logits
    """

    # convert target to 0/1 from -1/1
    anchor_targets = tf.cast(tf.equal(targets, 1), tf.int32)

    active = tf.where(tf.not_equal(targets, 0))
    preds = tf.gather_nd(preds, active)
    anchor_targets = tf.gather_nd(anchor_targets, active)
    
    cce = losses.SparseCategoricalCrossentropy(from_logits=True, reduction='auto')

    loss = cce(y_true=anchor_targets, y_pred=preds)
    #loss = tf.compat.v1.losses.sparse_softmax_cross_entropy(labels=anchor_targets,logits=preds)
    #pdb.set_trace()
    return loss 

def smooth_L1(preds, targets, sigma):
    diff = preds - targets
    abs_diff = tf.abs(diff)
    h =  tf.keras.losses.Huber(delta= sigma, reduction=tf.keras.losses.Reduction.SUM)
    loss = h(targets, preds)

    return loss

def smooth_l1loss(preds, targets, sigma, reduce=True):
    sigma_2 = sigma**2
    #pdb.set_trace()
    #if isinstance(preds, np.float64):
    #targets = tf.cast(targets, np.float32, name=None)
    #print("Preds :{}".format(preds))
    #print("Targets :{}".format(targets))
    diff = preds - targets
    abs_diff = tf.abs(diff)
    abs_diff_lt_1 = tf.less(abs_diff, 1/sigma_2)
    anchorwise_smooth_l1norm  = tf.reduce_sum(input_tensor=tf.where(abs_diff_lt_1,
                                (0.5*sigma_2)*tf.math.square(abs_diff),
                                abs_diff - (0.5/sigma_2)),
                                axis=1)
    if reduce:
        return tf.reduce_sum(input_tensor=anchorwise_smooth_l1norm)
    else:
        return anchorwise_smooth_l1norm

def rpn_bbox_loss(bbox_preds, cls_targets, bbox_targets, config):

    """Return the RPN bounding box loss.

    bbox_preds: [batch, num_anchors, (dy, dx, log(dh), log(dw), angle)].
    cls_targets: [batch, num_anchors]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    bbox_targets: [batch, positive_anchors, (dy, dx, log(dh), log(dw))]
    """

    cls_targets_ = tf.reshape(cls_targets, (-1,))
    active = tf.where(tf.equal(cls_targets_, 1))
    #pdb.set_trace()
    bbox_preds = tf.reshape(bbox_preds, (-1, 5))
    bbox_preds = tf.gather_nd(bbox_preds, active)

    pos_anchors_in_batch = tf.reduce_sum(input_tensor=tf.cast(tf.equal(cls_targets, 1),
                                                              tf.int32), axis=1)
                                                              
    pos_bbox_targets = [bbox_targets[i, :pos_anchors_in_batch[i]] for i in
                        range(config.BATCH_SIZE)]
    #pdb.set_trace()
    pos_bbox_targets = tf.concat(pos_bbox_targets, axis=0)
    print(pos_bbox_targets)

    loss = smooth_l1loss(bbox_preds, pos_bbox_targets, sigma=1.0)
    #loss = smooth_L1(bbox_preds, pos_bbox_targets, sigma=1.0)
    loss /= config.RPN_TRAIN_ANCHORS_PER_IMAGE
    print(loss)
    #pdb.set_trace()
    #tf.losses.add_loss(loss)
    
    return loss

def rcnn_class_loss(preds, targets, config):
    """Loss for the classifier head of RCNN.

    targets: [batch*num_rois]. Integer class IDs. Uses zero
        padding to fill in the array.
    preds: [batch*num_rois, num_classes] logits

    Returns:
        loss [batch*num_rois]
    """

    #cce = losses.SparseCategoricalCrossentropy(from_logits=True)
    #loss = cce(y_true=targets,y_pred=preds)   
    loss = tf.compat.v1.losses.sparse_softmax_cross_entropy(labels=targets,
                                                      logits=preds) 

    return loss

def rcnn_bbox_loss(bbox_preds, cls_targets, bbox_targets, config):
    """Return the stage2 bounding box loss.

    bbox_preds: [batch*num_rois, num_classes, (dy, dx, log(dh), log(dw))].
    cls_targets: [batch*num_rois]
    bbox_targets: [batch*nums_rois, (dy, dx, log(dh), log(dw))]

    Returns:
        loss: [batch*num_rois]
    """

    shape = tf.shape(input=cls_targets)
    right_cls_indx = tf.stack([tf.range(shape[0]),
                               tf.cast(tf.squeeze(cls_targets), tf.int32)],
                              axis=1)

    bbox_preds = tf.gather_nd(bbox_preds, right_cls_indx)
    bbox_preds = tf.squeeze(bbox_preds)

    active = tf.cast(tf.not_equal(cls_targets, 0), tf.float32)

    loss = smooth_l1loss(bbox_preds, bbox_targets, sigma=1.0, reduce=False)
    # make bg loss zero

    loss *= active
    if not config.OHEM:
        loss = tf.reduce_sum(input_tensor=loss)/config.NUM_RCNN_TRAIN_ROIS

    return loss

def ohem_loss(rois, class_losses, bbox_losses, config):
    """Returns loss after OHEM
    Inputs:
        - rois: [batch_size*num_rois, (y1,x1,y2,x2)] in normalized coords
        - class_losses: [batch_size*num_rois]
        - bbox_losses: [batch_size*num_rois]
        - config: configuration object"""

    rois = tf.reshape(rois, (-1, 5))
    total_loss = config.OHEM_CLS_WEIGHT*class_losses + \
                 config.OHEM_BBOX_WEIGHT*bbox_losses

    indices = tf.image.non_max_suppression(
        rois, tf.squeeze(total_loss), config.OHEM_NMS_COUNT,
        config.OHEM_NMS_THRESHOLD, name="ohem_non_max_suppression")

    selected = tf.gather(total_loss, indices)
    selected_loss = tf.reduce_mean(input_tensor=selected)
    
    return selected_loss




