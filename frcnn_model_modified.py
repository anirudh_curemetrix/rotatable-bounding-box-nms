from __future__ import division

from backbones import resnet
import utils
#import logging
#logging.basicConfig(level=logging.INFO)
import numpy as np
import tensorflow as tf
import cmdataset
import pdb

from modules import rpn, ROIAlign
from modules import rcnn_head, get_rcnn_detections
from modules import generate_anchors, get_proposals
from modules import rpn_targets_graph, rcnn_targets_graph
from backbones import resnet

from losses import rpn_class_loss, rpn_bbox_loss
from losses import rcnn_class_loss, rcnn_bbox_loss, ohem_loss
#import tensorflow.keras.regularizers as regularizers

class FRCNN(tf.keras.Model):
    
    def __init__(self, config,writer,training=True, iteration=0):
        super(FRCNN, self).__init__()
        self.config = config
        self.writer = writer
        self.iteration = iteration

        height, width = self.config.IMAGE_SHAPE[:2]
        self.box_norm_coords = np.array([[height, width, height, width, 1]])

        # Generate Anchors(absolute coordinates)
        self.anchors = generate_anchors(self.config.RPN_ANCHOR_SCALES,
                                        self.config.RPN_ANCHOR_RATIOS,
                                        self.config.FEATURE_SHAPE,
                                        self.config.FEATURE_STRIDE,
                                        self.config.RPN_ANCHOR_STRIDE)
        
        # Use first 3 blocks of ResNet101 from tf.keras.applications        
        input_shape = (self.config.IMG_HEIGHT, self.config.IMG_WIDTH,3)
        resnet101 = tf.keras.applications.ResNet101(input_shape=input_shape,
                                                    include_top=False, weights='imagenet')

        self.block3 = tf.keras.Model(inputs=resnet101.inputs,
                                outputs=resnet101.get_layer("conv4_block23_out").output)

        del resnet101  # Free up memory

#        self.block3 = tf.keras.models.load_model("Block3")
        
        FREEZE = False
        if FREEZE:
            print("Freezing ResNet Blocks")
            self.block3.trainable=False            
        else:
            # Turn on regularization for Convolution layers
            l2 = tf.keras.regularizers.l2(config.WEIGHT_DECAY)
            base = ["input_1","conv1_pad","conv1_conv","conv1_bn","conv1_relu","pool1_pad","pool1_pool"]
            for layer in self.block3.layers:            
                print(layer.name)
                # Always keep base fixed
                if layer.name in base:
                    layer.trainable = False
                else:
                    layer.trainable = training                
                    if (training and hasattr(layer,'kernel_regularizer')):
                        print(layer.name," --> qdding L2 regularizer")
                        self.add_loss(lambda layer=layer: l2(layer.kernel))
                                
        #   Manually create ResNet from layers            
#        self.block3 = resnet.ResNet(depth=101,name="ResNet101")

        # Define other trainable modules        
        self.rpn = rpn(self.config)        
        self.ROIAlign = ROIAlign(self.config)
        self.rcnn_head = rcnn_head(self.config)

    def __call__(self, inputs, iteration, training=True):

        if training:
            next_element =  inputs.iterator.get_next()

            self.imgs =      tf.expand_dims(next_element[0], axis=0)
            self.gt_bboxes = tf.expand_dims(next_element[1], axis=0)
            self.gt_cls =    tf.expand_dims(next_element[2], axis=0)

            self.imgs.set_shape([self.config.BATCH_SIZE,
                                 self.config.IMG_HEIGHT,
                                 self.config.IMG_WIDTH,
                                 3])
            self.gt_bboxes.set_shape([self.config.BATCH_SIZE,
                                      self.config.MAX_GT_INSTANCES,
                                      5])

            self.gt_cls.set_shape([self.config.BATCH_SIZE,
                                   self.config.MAX_GT_INSTANCES])            
        else:
            self.imgs = inputs
                    
        # extract feature maps
        C4 = self.block3(self.imgs)
        C4 = tf.keras.layers.Activation('linear',dtype='float32')(C4)
        #C4 = tf.keras.layers.Activation('relu',dtype='float32')(C4)
        
        # rpn predictions
        #print("Ground-truth boxes: {}".format(self.gt_bboxes))

        rpn_cls, rpn_bbox_deltas = self.rpn(C4)       
        print("RPN BOX DELTAS: {}".format(rpn_bbox_deltas))
        print("RPN CLS: {}".format(rpn_cls))
        # select a subset of ROIs, apply refinement and nms
        # proposal boxes are in normalized coordinates
        #pdb.set_trace()
        self.proposed_rois = get_proposals(rpn_cls,
                                      rpn_bbox_deltas,
                                      self.anchors,
                                      self.config,
                                      training)
        #
        #=====================  TRAINING ========================
        if training:
            ############## RPN targets and losses #################
            rpn_cls_gt, rpn_bbox_deltas_gt = rpn_targets_graph(self.anchors,
                                                               self.gt_cls,
                                                               self.gt_bboxes,
                                                               self.config)
            
            print("RPN BOX DELTAS GT :{}".format(rpn_bbox_deltas_gt))
            print("RPN CLS GT: {}".format(rpn_cls_gt))

            # losses are added to loss collections
            self.rpn_cls_loss = rpn_class_loss(rpn_cls, rpn_cls_gt)
            self.rpn_bbox_loss = rpn_bbox_loss(rpn_bbox_deltas, rpn_cls_gt,rpn_bbox_deltas_gt, self.config)

            if self.config.DEBUG:
                # display positive anchors
                cls_targets = tf.reshape(rpn_cls_gt, (-1,))
                active = tf.where(tf.equal(cls_targets, 1))            
                pos_anchors = tf.reshape(self.anchors, (-1, 5))
                pos_anchors = tf.gather_nd(pos_anchors, active)
                boxes = tf.expand_dims(pos_anchors, axis=0)
                #print("Positive Anchors {} ".format(active))
                utils.image_with_bbox_summ(self.imgs, boxes,'pos_anchors',
                                           'mass', self.config, self.writer, iteration)
                        
            ################ RCNN targets ##############
            # select training rois for RCNN and label them
            #pdb.set_trace()
            gt_bboxes_normalized = self._to_normalized_boxes(self.gt_bboxes)

            rois, roi_cls_gt, roi_bbox_delta_gt = rcnn_targets_graph(
                                                    self.proposed_rois,
                                                    self.gt_cls,
                                                    gt_bboxes_normalized,
                                                    self.config)            
            # shape [batch_size, num_proposals, w, h, c]

            pooled_rois = self.ROIAlign(rois, C4)

            # here batch_size and num_proposals are collapsed into one dimension
            rcnn_cls_logits, rcnn_bbox_deltas  = self.rcnn_head(pooled_rois, training)

            if self.config.IMG_SUMMARY:
                # display top rpn proposals
                disp_rois = self.proposed_rois[:,:10,:]
                disp_rois = self._from_normalized_boxes(disp_rois)
                #print("Top RPN Proposals: {} ".format(disp_rois))
                utils.image_with_bbox_summ(self.imgs, disp_rois,'top_rpn_rois',
                                           'mass', self.config, self.writer, iteration)

            # collapse target's dimensions too    
            roi_cls_gt = tf.cast(tf.reshape(roi_cls_gt, (-1,)), tf.int32)
            # Need to fix
            roi_bbox_delta_gt = tf.reshape(roi_bbox_delta_gt, (-1, 5))
            
            if  self.config.DEBUG:
                # display RCNN training mass rois
                active = tf.where(tf.equal(roi_cls_gt, 2))
                pos_rois = tf.reshape(rois, (-1, 5))
                pos_rois = tf.gather_nd(pos_rois, active)
                pos_rois = tf.expand_dims(pos_rois, axis=0)
                pos_boxes = self._from_normalized_boxes(pos_rois)
                #print("RCNN Training mass ROIs: {}".format(pos_boxes))
                utils.image_with_bbox_summ(self.imgs, pos_boxes,'xrcnn_mass_rois',
                                           'mass', self.config, self.writer, iteration)
                active = tf.where(tf.equal(roi_cls_gt, 1))
                pos_rois = tf.reshape(rois, (-1, 5))
                pos_rois = tf.gather_nd(pos_rois, active)
                pos_rois = tf.expand_dims(pos_rois, axis=0)
                pos_boxes = self._from_normalized_boxes(pos_rois)
                #print("RCNN Training mass normal ROIs: {}".format(pos_boxes))
                utils.image_with_bbox_summ(self.imgs, pos_boxes,'xrcnn_mass_normal_rois',
                                           'mass_normal', self.config, self.writer, iteration)

            if self.config.OHEM:
                rcnn_cls_losses = rcnn_class_loss(rcnn_cls_logits, roi_cls_gt,
                                                  self.config)
                rcnn_bbox_losses = rcnn_bbox_loss(rcnn_bbox_deltas, roi_cls_gt,
                                                  roi_bbox_delta_gt, self.config)

                self.rcnn_cls_loss = tf.reduce_mean(input_tensor=rcnn_cls_losses)
                self.rcnn_bbox_loss = tf.reduce_mean(input_tensor=rcnn_bbox_losses)

                self.rcnn_loss = ohem_loss(rois, rcnn_cls_losses, rcnn_bbox_losses,
                                           self.config)
            else:

                self.rcnn_cls_loss = rcnn_class_loss(rcnn_cls_logits, roi_cls_gt,
                                                     self.config)
                self.rcnn_bbox_loss = rcnn_bbox_loss(rcnn_bbox_deltas, roi_cls_gt,
                                                     roi_bbox_delta_gt, self.config)
                
            #################### Total losses ##############################

            return self.rpn_cls_loss, self.rpn_bbox_loss, self.rcnn_cls_loss, self.rcnn_bbox_loss

        else:
            
            #=====================  INFERENCE ========================

            pooled_rois = self.ROIAlign(self.proposed_rois, C4)
            
            rcnn_cls_logits, rcnn_bbox_deltas  = self.rcnn_head(pooled_rois,training)                    

            pred_cls_probs = tf.keras.layers.Activation("softmax",dtype="float32",name="cls_probs")(rcnn_cls_logits)
            
            # shape [batch_size, num_rois, 4] and [batch_size, num_rois] for scores and idx
            # returns rois of last class only

            proposed_rois = self.proposed_rois[0,:,:]
            final_rois, final_scores, final_idx = get_rcnn_detections(proposed_rois, pred_cls_probs, rcnn_bbox_deltas, self.config)
            
        return final_rois, final_scores
                                

    def _to_normalized_boxes(self, boxes):
        """Converts boxes to normalized coordinates
        Inputs:
            - boxes: [batch_size, num_rois, 5]
        Outputs:
            - boxes: [batch_size, num_rois, 5]
        """
        return boxes/self.box_norm_coords

    def _from_normalized_boxes(self, boxes):
        """Converts boxes to absolute coordinates
        Inputs:
            - boxes: [batch_size, num_rois, 5]
        Outputs:
            - boxes: [batch_size, num_rois, 5]
        """
        return boxes*self.box_norm_coords


